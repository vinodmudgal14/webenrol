<?php
session_start();
session_name();
include('admin/conf/conf.php');
include('admin/conf/fucts.php');
$compurl = $_COOKIE["compurl"];

$premiumAmount = @$empDetails[0]['empPremium'] ? $empDetails[0]['empPremium'] : 0;
if ($_SESSION['empId'] == '')
{
    $url = 'Location: company_' . $compurl;
    session_destroy();
    $_SESSION["empId"] = '';
    $_SESSION["companyId"] = '';
    $_SESSION["companyName"] = '';
    $_SESSION["compurl"] = '';
    $_SESSION["urlStatus"] = '';
    $_SESSION["urlExpiryDate"] = '';
    setcookie("compurl", "", time() - 90000);
    header($url);
    exit;
}
$companyWellness = chkCompanyWellness(@$_SESSION['companyId'], time());
$m = date('m', time());
$Y = date('Y', time());
$d = date('d', time());
$startDay = mktime(0, 0, 0, $m, $d, $Y);
//if (($_SESSION["urlStatus"] != 'active') || ($_SESSION["urlExpiryDate"] < $startDay))
if (($_SESSION["urlStatus"] != 'open') || ($_SESSION["enrolEndDate"] < $startDay))
{
    $url = "Location: my_policy_preview.php";
    header($url);
    exit;
}
else
{
    $empDetails             = companyEmpDetails($_SESSION['companyId'], $_SESSION['empId']);
    //$empFamilyDetails     = getEmpFamilyList($_SESSION['companyId'], $_SESSION['empId']);   
    $empFamilyDetails       = getEmpFamily($_SESSION['companyId'], $_SESSION['empId']);    
    $empFamilyList          = getEmpFamilyList($_SESSION['companyId'], $_SESSION['GENDER']);
    $allMembersListArr      = getAllMembersList();
    $allMembersList         = array();
    foreach($allMembersListArr as $memArr)
    {
        $allMembersList[$memArr['id']] = $memArr['member'];
    }
    $gradeDetails           = gradeList($_SESSION['companyId']);
    //$suminsuredlist         = sumInsuredMapList($_SESSION['companyId']);  
    $suminsuredlist         = gradeSumInsuredMapList($_SESSION['companyId'], $empDetails[0]['empGrade']);
    
    foreach($suminsuredlist as $siList)
    {
        if($siList['id'] == $empDetails[0]['sumInsuredId'])
        {
            @$sumInsuredAmount = $siList['sumInsured'];
            @$premiumAmount = $siList['gradePremium'];
        }
    }
    //$df = getGradeSIMapData($gradeId,$sumInsuredId,$companyId);
    $topupSumInsuredMapList = topUpSumInsuredMapList($_SESSION['companyId']);
    $topupsuminsuredList    = chkTopUpList($_SESSION['companyId'],$empDetails[0]['empGrade']);//topUpSumInsuredMapList($_SESSION['companyId']);
    $companydetails         = companydetails($_SESSION['companyId']);
    $empFamilyCount         = count($empFamilyDetails);    
    if($companydetails[0]['defaultTheme'] != 1)
    {    
        $menuColor              = @$companydetails[0]['menuColor'] ? "style=color:#".$companydetails[0]['menuColor']. ";" : "";
        $menuActiveArrowColor   = @$companydetails[0]['menuColor'] ? "style=border-top-color:#".$companydetails[0]['menuColor']. ";" : "";
        $menuArrowColor         = @$companydetails[0]['menuColor'] ? "style=border-top-color:#".$companydetails[0]['menuBgColor']. ";" : "";
        $menuBgColor            = @$companydetails[0]['menuBgColor'] ? "style=background:#".$companydetails[0]['menuBgColor']. ";" : "style=background:#b7db82";
        $headingColor           = @$companydetails[0]['headingColor'] ? "color:#".$companydetails[0]['headingColor']. ";" : "";
        $headingBgColor         = @$companydetails[0]['headingBgColor'] ? "background:#".$companydetails[0]['headingBgColor']. ";" : "";
        $buttonColor            = @$companydetails[0]['buttonColor'] ? "color:#".$companydetails[0]['buttonColor'].";" : "";
        $buttonBgColor          = @$companydetails[0]['buttonBgColor'] ? "background:#".$companydetails[0]['buttonBgColor'] . ";" : "";

        $menuColorsFromDb       = "style=".$menuColor.$menuBgColor;
        $headingColorsFromDb    = "style=".$headingColor.$headingBgColor;  
        $buttonStyle            = "style=". $buttonColor .$buttonBgColor;
    }
    else
    {
        $menuColor              = "";
        $menuActiveArrowColor   = "";
        $menuArrowColor         = "";
        $menuBgColor            = "style=background:#b7db82";
        $headingColor           = "";
        $headingBgColor         = "";
        $buttonColor            = "";
        $buttonBgColor          = "";
        $menuColorsFromDb       = "";
        $headingColorsFromDb    = "";  
        $buttonStyle            = "";
    }
    if($companydetails[0]['proRata'])
    {
        $totalPolicyPeriod = ($companydetails[0]['policyEndDate'] - $companydetails[0]['policyStartDate'])/(60*60*24) + 1;
        $availPolicyPeriod = ($companydetails[0]['policyEndDate'] - $empDetails[0]['enrolStartDate'])/(60*60*24) + 1;
        if($availPolicyPeriod > $totalPolicyPeriod)
        {
            $availPolicyPeriod = $totalPolicyPeriod;
        }
        
        $multiplier = $availPolicyPeriod/$totalPolicyPeriod;
        if(!@$_SESSION['multiplier'])
        {
            $_SESSION['multiplier'] = ($multiplier>0)? $multiplier : 1;
        }
        $premiumAmount = round($premiumAmount * $_SESSION['multiplier'], 2);
    }  
    if ($companydetails[0]['insuranceType'] == 'floater')
    {
        $fixedpremiumamount = $companydetails[0]['fixedPremiumAmount'];
    }
    else
    {
        if ($companydetails[0]['policyType'] == 'fixed')
        {
            $fixedpremiumamount = $companydetails[0]['fixedPremiumAmount'];
        }
        else
        {
            $fixedpremiumamount = '';
        }
    }
    ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>Religare</title>
            <!--<link href="css/style.css" rel="stylesheet" type="text/css" />-->
            <link rel="stylesheet" href="css/style.css"/>
            <link rel="stylesheet" href="css/style_color.css"/>
            <script src="js/jquery.min.js"></script>
            <script type="text/javascript" src="js/select_menu.js"></script>
            <script type="text/javascript" src="admin/js/ui.datepicker.js"></script>
            <link href="admin/css/ui.datepicker.css" rel="stylesheet" type="text/css">
                <script type="text/javascript">
                    $(document).ready(function ()
                    {
                        $("#dob").datepicker({
                            onSelect: function (date) {
                                //alert('hi');
                                selectagelinkedemppremium();
                            },
                            maxDate: '-1d',
                            yearRange: '1900:<?php echo date('Y'); ?>',
                            dateFormat: "yy-mm-dd"
                        });
                        $(".fdob").datepicker({
                            onSelect: function (date) {
                                //alert('hi');
                                var currentid = $(this).attr('id');
                                var getid = currentid.substring(4);
                                seletdob(getid);
                            },
                            maxDate: '-1d',
                            yearRange: '1900:<?php echo date('Y'); ?>',
                            dateFormat: "yy-mm-dd"
                        });
                    });
                    function selectagelinkedemppremium() {
                        var dob = $("#dob").val();
                        var empGrade = $("#empGrade").val();
                        var policyType = $("#policyType").val();
                        var offerSumInsured = $("#offerSumInsured").val();
                        getEmpTopupSIPremium();
                        if (policyType == 'ageLinked') {
                            $.ajax({
                                type: "POST",
                                url: "agelinkedpremium.php",
                                data: "gradeId=" + empGrade + "&dob=" + dob + "&offerSumInsured=" + offerSumInsured,
                                success: function (msg) {
                                    $(".empgraderes").html(msg);
                                    getEmpSIPremium();
                                    getEmpTopupSIPremium();
                                    if (offerSumInsured == 'grade') {
                                        $(".basesipremium").val('');
                                        $(".fdob").val('');
                                    }
                                }
                            });
                        }
                    }
                    function getSIPremium(companyId, htmlid) {
                        var sival = $("#siId" + htmlid).val();
                        var fdob = $("#fdob" + htmlid).val();
                //alert(fdob);
                        var policyType = $("#policyType").val();
                //alert(policyType);
                        if (policyType == 'ageLinked') {
                            if (fdob != '') {
                                $.ajax({
                                    type: "POST",
                                    url: "getSumInsuredMap.php",
                                    data: "sival=" + sival + "&companyId=" + companyId + "&htmlid=" + htmlid + "&fdob=" + fdob,
                                    success: function (msg) {
                                        $("#suminsured_result" + htmlid).html(msg);
                                    }
                                });
                            } else {
                                alert("Please enter date of birth.");
                            }
                        } else {

                            $.ajax({
                                type: "POST",
                                url: "getFixedSumInsuredMap.php",
                                data: "sival=" + sival + "&companyId=" + companyId + "&htmlid=" + htmlid + "&fdob=" + fdob,
                                success: function (msg) {
                                    $("#suminsured_result" + htmlid).html(msg);
                                }
                            });

                        }
                    }
                    function getEmpSIPremium_11Sept2015() {
                        var sival = $("#empSIId").val();
                        var empSIIdHid = $("#empSIIdHid").val();
                //alert(sival);	alert(empSIIdHid);
                        if (typeof sival === 'undefined') {
                            var sival = empSIIdHid;
                        } else {
                            var sival = sival;
                        }
                        var dob = $("#dob").val();
                        var policyType = $("#policyType").val();
                        var companyId = $("#companyId").val();
                //alert(fdob);
                        if (policyType == 'ageLinked') {
                            if (dob != '') {
                                $.ajax({
                                    type: "POST",
                                    url: "getEmpSumInsuredMap.php",
                                    data: "sival=" + sival + "&companyId=" + companyId + "&dob=" + dob,
                                    success: function (msg) {
                                        $("#empsi_result").html(msg);
                                    }
                                });
                            } else {
                                alert("Please enter date of birth.");
                            }
                        }
                        if (policyType == 'fixed') {
                            $.ajax({
                                type: "POST",
                                url: "getEmpFixedSumInsuredMap.php",
                                data: "sival=" + sival + "&companyId=" + companyId + "&dob=" + dob,
                                success: function (msg) {
                                    $("#empsi_result").html(msg);
                                }
                            });
                        }
                    }
                    //Added\Modified on 11Sept2015 by Shakti.
                    function getEmpSIPremium() {
                        var sival       = $("#empSIId").val();
                        var dob         = $("#dob").val();
                        var policyType  = $("#policyType").val();
                        var companyId   = $("#companyId").val();
                        var gradeId  = $("#empGrade").val();
                        if (policyType == 'ageLinked') {
                            if (dob != '') {
                                $.ajax({
                                    type: "POST",
                                    url: "getEmpSumInsuredMap.php",
                                    data: "sival=" + sival + "&companyId=" + companyId + "&dob=" + dob  + '&gradeId=' + gradeId,
                                    success: function (msg) {
                                        $("#empsi_result").html(msg);
                                    }
                                });
                            } else {
                                alert("Please enter date of birth.");
                            }
                        }
                        if (policyType == 'fixed') {
                            $.ajax({
                                type: "POST",
                                url: "getEmpFixedSumInsuredMap.php",
                                data: "sival=" + sival + "&companyId=" + companyId + "&dob=" + dob + '&gradeId=' + gradeId,
                                success: function (msg) {
                                    $("#empgradepremium").html(msg);                                    
                                    $("#empPremium").val(msg);
                                }
                            });
                        }
                    }
                    function getEmpGradePremium(companyId) {
                        var gradeId = $("#empGrade").val();
                        var dob = $("#dob").val();
                        var insuranceType = $("#insuranceType").val();
                        var policyType = $("#policyType").val();
                        if (policyType == 'ageLinked') {
                            if (dob == '') {
                                alert("Please enter date of birth.");
                                return false;
                            }
                            $.ajax({
                                type: "POST",
                                url: "getEmpGradePremium.php",
                                data: "gradeId=" + gradeId + "&companyId=" + companyId + "&insuranceType=" + insuranceType + "&policyType=" + policyType + "&dob=" + dob,
                                success: function (msg) {
                                    //alert(msg);
                                    var arr = msg.split('_');
                                    $("#empgraderesult").html(arr[0]);
                                    //$("#empSIId").val(arr[1]);
                                    //$("#empgradepremium").html(arr[2]);
                                    //$("#empPremium").val(arr[2]);
                                    //$(".empgraderes").html(msg);
                                    $(".gradesires").html(arr[0]);
                                    $(".basesipremium").val('');
                                    $(".fdob").val('');
                                }
                            });
                        }
                        if (policyType == 'fixed') {
                            $.ajax({
                                type: "POST",
                                url: "getEmpGradeFixedPremium.php",
                                data: "gradeId=" + gradeId + "&companyId=" + companyId + "&insuranceType=" + insuranceType + "&policyType=" + policyType + "&dob=" + dob,
                                success: function (msg) {
                                    var arr = msg.split('_');
                                    //alert(msg);			
                                    $("#empgraderesult").html(arr[0]);
                                    //$("#empSIId").val(arr[1]);
                                    //$("#empgradepremium").html(arr[2]);
                                    //$("#empPremium").val(arr[2]);
                                    //$(".gradesires").html(arr[0]);
                                    $(".basesipremium").val(arr[0]);
                                    var empPremium = $("#empPremium").val();
                                    $(".basesipremium").val(empPremium);
                                    //$(".fdob").val('');	
                                    /*var empTopupSIId = $("#empTopupSIId").val();
                                     if(empTopupSIId!='') {
                                     getEmpTopupSIPremium();	
                                     }*/
                                }
                            });
                        }
                        //getTopupSIMapped(gradeId, policyType, companyId); commented on 17Sept 2015.
                    }
                    
                    //added on 11sept 2015 - Shakti
                    function getEmpSIMapped(companyId)
                    {
                        var gradeId = $("#empGrade").val();                        
                        var policyType = $("#policyType").val();                        
                        if (gradeId != '') {
                            $.ajax({
                                type: "POST",
                                url: "getEmpSIMapped.php",
                                data: "gradeId=" + gradeId + "&companyId=" + companyId,
                                beforeSend: function(){
                                    $("#empgradepremium").html('0');
                                    $("#empPremium").val(0);
                                    $("#emptopuppremiumresult").html('0');
                                },
                                success: function (msg) {                                    
                                    $("#empSIId").html(msg);
                                    getTopupSIMapped(gradeId, policyType, companyId);
                                }
                            });
                        }
                        
                    }
                    function getTopupSIMapped(gradeId, policyType, companyId) {
                        if (gradeId != '') {
                            $.ajax({
                                type: "POST",
                                url: "getTopupSIMapped.php",
                                data: "gradeId=" + gradeId + "&companyId=" + companyId + "&policyType=" + policyType,
                                success: function (msg) {
                                    $("#topupresult").html(msg);
                                    $("#empTopupSIId").html(msg); // added on 17 Sept 2015.                                    
                                }
                            });
                        }
                    }
                    function getTopupSIPremium(companyId, htmlid) {
                        var topupsival = $("#topSIId" + htmlid).val();
                        var fdob = $("#fdob" + htmlid).val();
                        if (fdob != '') {
                //alert(sival);
                            $.ajax({
                                type: "POST",
                                url: "getTopupSumInsuredMap.php",
                                data: "sival=" + topupsival + "&companyId=" + companyId + "&htmlid=" + htmlid + "&fdob=" + fdob,
                                success: function (msg) {
                                    $("#topupsuminsured_result" + htmlid).html(msg);
                                }
                            });
                        } else {
                            alert("Please enter date of birth");
                        }
                    }
                    function getEmpTopupSIPremium() {
                        var empTopupSIId = $("#empTopupSIId").val();
                        var companyId = $("#companyId").val();
                        var policyType = $("#policyType").val();
                        var empGrade = $("#empGrade").val();
                        var dob = $("#dob").val();
                        if (policyType == 'fixed') {
                            if (empGrade == '') {
                                alert('Please select grade first');
                                return false;
                            }
                        }
                        $.ajax({
                            type: "POST",
                            url: "getEmpTopupSumInsuredMap.php",
                            data: "sival=" + empTopupSIId + "&companyId=" + companyId + "&dob=" + dob + "&policyType=" + policyType + "&empGrade=" + empGrade,
                            success: function (msg) {
                                $("#emptopuppremiumresult").html(msg);
                                
                            }
                        });
                    }
                    function getFamilyHtml(id) {
                        var idval = parseInt(id) + 1;
                        var firstName = $("#firstName" + parseInt(id)).val();
                        if (firstName != '') {
                            $(".res" + idval).show();
                            //alert(idval);
                            $("#htmlreslut" + id).html('<b onclick=removehtml(' + id + ',0)><img src="images/minus_white.png" /></b>');
                            /*$.ajax({
                             type: "POST",
                             url: "getFamilyDetailsHtml.php",
                             data: "id="+id,
                             success: function(msg){
                             $(".res"+idval).html(msg);			
                             }
                             });*/
                        } else {
                            alert("Please enter first name");
                            $("#firstName" + parseInt(id)).focus();
                        }
                    }
                    function removehtml(id, familyid) {
                        var r = confirm('Are you sure you want to remove.');
                        if (r == true) {
                            if (familyid != '') {
                                $.ajax({
                                    type: "POST",
                                    url: "removeFamilyMember.php",
                                    data: "familyid=" + familyid,
                                    success: function (msg) {
                                        $(".res" + id).remove();
                                    }
                                });
                            } else {
                                $(".res" + id).remove();
                            }
                        }
                    }
                    function seletdob(id) {
                        var fdob = $("#fdob" + id).val();
                        var companyId = $("#companyId").val();
                        var policyType = $("#policyType").val();
                        var offerSumInsured = $("#offerSumInsured").val();
                        if (offerSumInsured == 'age') {
                            var siId = $("#siId" + id).val();
                            //if(siId=='') { alert("Please select a sum insured"); return false;	}
                            var dataval = "sival=" + siId + "&fdob=" + fdob + "&htmlid=" + id + "&offerSumInsured=" + offerSumInsured;
                        } else {
                            var gradeid = $("#empGrade").val();
                //if(gradeid=='') { alert("Please select a grade"); return false;	}
                            var dataval = "gradeId=" + gradeid + "&fdob=" + fdob + "&htmlid=" + id + "&offerSumInsured=" + offerSumInsured;
                        }
                        if (policyType == 'ageLinked') {
                            $.ajax({
                                type: "POST",
                                url: "getEmpDOBPremium.php",
                                data: dataval,
                                success: function (msg) {
                                    $("#suminsured_result" + id).html(msg);
                                    getTopupSIPremium(companyId, id);
                                }
                            });
                        }
                    }
                </script>
        </head>

        <body>
        <!--<div class="top_container"><img src="images/religare_logo.jpg" alt="Religare" class="fl" />-->
            <div class="top_header">
                <div class="login_container"><img src="<?php echo $companydetails[0]['logoFile'] ? "imagetest/" . $companydetails[0]['logoFile'] : "images/login_logo.jpg"; ?>" border="0"><a class="logout" href="signout.php">Log Out</a></div>
            </div>

            <div class="mid_nav">
                <ul class="mainnav" <?= $menuBgColor;?>>
                    <li><a class="active" href="my_policy.php"> <span class="arrow-down" <?= $menuActiveArrowColor;?>>&nbsp;</span> <span class="activetext" <?= $menuColor;?>>My&nbsp;Details</span> </a></li>
                    <li><a href="enquiry.php"> <span class="arrow-down" <?= $menuArrowColor;?>>&nbsp;</span> <span class="activetext" <?= $menuColor;?>>Change Request</span> </a></li>
            <?php if (count($companyWellness) > 0)
            {
                ?>
                        <li><a href="wellness.php">Wellness</a></li>
    <?php } ?>
                </ul>
            </div>

            <div class="page_nav"><a href="my_policy.php">Home</a> &raquo; My Details</div> 

    <?php if (@$_REQUEST['msg'] == 'success')
    { ?>
                <div style="color: red;padding: 10px;text-align: center;">Employee details has been updated successfully.</div>
    <?php } ?>
            <form name="detailform" id="detailform" method="post" action="updateDetails.php">
                <div class="mid_container">
                    <div class="mid_shadow">
                        <div class="mid_shadow_head" <?= $headingColorsFromDb;?>>Company details</div>                        
                        <input type="hidden" name="companyId" id="companyId" value="<?= $_SESSION['companyId']; ?>" />
                        <input type="hidden" name="empId" id="empId" value="<?= $_SESSION['empId']; ?>" />
                        <input type="hidden" name="empFamilyCount" id="empFamilyCount" value="<?= $empFamilyCount; ?>" />
                        <input type="hidden" name="insuranceType" id="insuranceType" value="<?= $companydetails[0]['insuranceType']; ?>" />
                        <input type="hidden" name="dependent" id="dependent" value="<?= $companydetails[0]['dependent']; ?>" />
                        <input type="hidden" name="policyType" id="policyType" value="<?= $companydetails[0]['policyType']; ?>" />
                        <input type="hidden" name="offerSumInsured" id="offerSumInsured" value="<?= $companydetails[0]['offerSumInsured']; ?>" />
                        <div class="mid_content">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td height="40" align="left"><table width="450" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="150">Company Name</td>
                                                <td width="10">:</td>
                                                <td width="290"><?= $_SESSION['companyName']; ?></td>
                                            </tr>
                                        </table></td>
                                    <td class="gray_devider" align="right"><table width="400" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="193" align="left">Employee No.</td>
                                                <td width="11" align="left">:</td>
                                                <td width="196" align="left"><?= $empDetails[0]['empNo']; ?></td>
                                            </tr>
                                        </table></td>
                                </tr>
                            </table>
                        </div>
                        <div class="cl"></div>
                    </div>

                    <div class="mid_container">
                        <div class="mid_shadow">
                        <div class="mid_shadow_head"  <?= $headingColorsFromDb;?>>Personal Details</div>
                            <div class="mid_content">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td height="40" align="left"><table width="450" border="0" cellspacing="0" cellpadding="0">        
                                                <tr>
                                                    <td width="145" height="40">Salutation</td>
                                                    <td width="10">:</td>
                                                    <td width="301"><select id="empSalutation" name="empSalutation" class="input">
                                                            <option value="MR" <?php if ($empDetails[0]['empSalutation'] == 'MR')
                                                            {
                                                                echo 'selected';
                                                            }
                                                            else
                                                            {

                                                            } ?>>MR</option>
                                                            <option value="MS" <?php if ($empDetails[0]['empSalutation'] == 'MS')
                                                            {
                                                                echo 'selected';
                                                            }
                                                            else
                                                            {

                                                            } ?>>MS</option>      
                                                        </select></td>
                                                </tr>

                                                <tr>
                                                    <td width="145">First Name</td>
                                                    <td width="10">:</td>
                                                    <td width="301"><input type="text" value="<?= $empDetails[0]['empFirstName']; ?>" name="empFirstName" id="empFirstName" style="width:200px;" class="input"></td>
                                                </tr>
                                                <tr>
                                                    <td width="145" height="40">Last name</td>
                                                    <td width="10">:</td>
                                                    <td width="301"><input type="text" class="input" value="<?= $empDetails[0]['empLastName']; ?>" name="empLastName" id="empLastName" style="width:200px;"></td>
                                                </tr>
                                                <tr>
                                                    <td width="145">Phone</td>
                                                    <td width="10">:</td>
                                                    <td width="301"><input type="text" value="<?= $empDetails[0]['empPhone']; ?>"  class="input" name="empPhone" id="empPhone" style="width:200px;"></td>
                                                </tr>
                                                <tr>
                                                    <td width="145" height="40">Email ID</td>
                                                    <td width="10">:</td>
                                                    <td width="301"><input type="text" value="<?= $empDetails[0]['empEmail']; ?>"  class="input" name="empEmail" id="empEmail" style="width:200px;"></td>
                                                </tr>                
                                            </table></td>

                                        <td class="gray_devider" align="right"><table width="400" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="187" height="40" align="left">Grade</td>
                                                    <td width="13" align="left">:</td>
                                                    <td width="200" align="left">
                                                        <select id="empGrade" name="empGrade" class="input" <?php if ($companydetails[0]['offerSumInsured'] == 'grade')
                                                        { ?> onchange="getEmpSIMapped('<?= $_SESSION['companyId']; ?>')"; <?php } ?>>
                                                            <option value="">Select</option>
    <?php
    $s = 0;
    while ($s < count($gradeDetails))
    {
        ?>
                                                                <option value="<?php echo $gradeDetails[$s]['id']; ?>" <?php if ($gradeDetails[$s]['id'] == $empDetails[0]['empGrade'])
        {
            echo 'selected';
        }
        else
        {
            
        } ?>><?php echo $gradeDetails[$s]['grade']; ?></option>
        <?php $s++;
    } ?>				                        
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr style="display: none">
                                                    <td width="187" align="left">Gender</td>
                                                    <td width="13" align="left">:</td>
                                                    <td width="200" align="left"><select id="gender" name="gender" class="input">
                                                            <option value="M" <?php if ($empDetails[0]['gender'] == 'M')
    {
        echo 'selected';
    }
    else
    {
        
    } ?>>Male</option>
                                                            <option value="F" <?php if ($empDetails[0]['gender'] == 'F')
    {
        echo 'selected';
    }
    else
    {
        
    } ?>>Female</option>                            
                                                        </select> 
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="187" height="40" align="left">Date of Birth</td>
                                                    <td width="13" align="left">:</td>
                                                    <td width="200" align="left"><input type="text" value="<?= date('Y-m-d', $empDetails[0]['dob']); ?>" name="dob" id="dob" style="width:200px;"  class="input"></td>
                                                </tr>
                                                <tr>
                                                    <td width="187" align="left">Nominee Name</td>
                                                    <td width="13" align="left">:</td>
                                                    <td width="200" align="left"><input type="text" value="<?= $empDetails[0]['nomineeName']; ?>" name="nomineeName" id="nomineeName" style="width:200px;" class="input"></td>
                                                </tr>
                                                <tr>
                                                    <td width="187" height="40" align="left">Nominee Relationship</td>
                                                    <td width="13" align="left">:</td>
                                                    <td width="200" align="left"><input type="text" value="<?= $empDetails[0]['relationNomEmp']; ?>" name="relationNomEmp" id="relationNomEmp" style="width:200px;" class="input"></td>
                                                </tr>              

                                            </table></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="cl"></div>    
                        </div>

                        <div class="mid_shadow">
                        <div class="mid_shadow_head" <?= $headingColorsFromDb;?>>Policy details</div>
                            <div class="mid_content">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td height="40" align="left"><table width="450" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="150">Insurance Type</td>
                                                    <td width="10">:</td>
                                                    <td width="290"><?= ucfirst($companydetails[0]['insuranceType']); ?></td>
                                                </tr>
                                                <tr>
                                                    <td width="150" height="40">Sum Insured</td>
                                                    <td width="10">:</td>
                                                    <!--<td width="290"><span id="empgraderesult"><select class="input" name="empSIId" id="empSIId" onchange="getEmpSIPremium()">-->
                                                    <td width="290"><select class="input" name="empSIId" id="empSIId" onchange="getEmpSIPremium()"<?php //$companydetails[0]['policyType']?>>
                                                                <option value="">Select</option>
    <?php
    $s = 0;
    while ($s < count($suminsuredlist))
    {
        ?>
                                                                    <option value="<?php echo $suminsuredlist[$s]['id']; ?>" <?php if ($suminsuredlist[$s]['id'] == $empDetails[0]['sumInsuredId'])
        {
            echo 'selected=selected';
        }
        else
        {
            
        } ?>><?php echo $suminsuredlist[$s]['sumInsured']; ?></option>
        <?php $s++;
    } ?>                   
                                                            </select>
                                                    </td>
                                                </tr>                

                                                <tr>
                                                    <td width="150">Topup Sum Insured</td>
                                                    <td width="10">:</td>
                                                    <td width="290"><select id="empTopupSIId" name="empTopupSIId" class="input" onchange="getEmpTopupSIPremium()">
                                                            <option value="">Select</option>
                                        <?php
                                        $s = 0;
                                        while ($s < count($topupsuminsuredList))
                                        {
                                            ?>
                                            <option value="<?php echo $topupsuminsuredList[$s]['id']; ?>" <?php if ($topupsuminsuredList[$s]['id'] == $empDetails[0]['empTopupSIId'])
                                            {
                                                $selectedTopSI = $topupsuminsuredList[$s]['sumInsured'];
                                                echo 'selected';
                                            } ?>><?php echo $topupsuminsuredList[$s]['sumInsured']; ?></option>
                                            <?php $s++;
                                        } ?>    				                            
                                                        </select>

                                                    </td>
                                                </tr>
                                            </table></td>
                                        <td class="gray_devider" align="right"><table width="400" border="0" cellpadding="0" cellspacing="0">  
                                                <tr>
                                                    <td width="150" align="left">Policy Type</td>
                                                    <td width="10" align="left">:</td>
                                                    <td width="290" align="left"><?php if ($companydetails[0]['policyType'] == 'ageLinked')
    {
        echo 'Age Linked';
    }
    else
    {
        echo 'Fixed';
    } ?></td>
                                                </tr>
                                                <tr>
                                                    <td width="150" height="40" align="left">Premium</td>
                                                    <td width="10" align="left">:</td>
                                                    <td width="290" align="left"><span id="empgradepremium"><?php echo @$premiumAmount;//echo $empDetails[0]['empPremium'];?></span>
                                                        <input type="hidden" id="empPremium" name="empPremium" class="empPremium" value="<?php echo @$premiumAmount;//echo $empDetails[0]['empPremium']; ?>"></td>                                                        
                                                </tr>
                                                <tr>
                                                    <td width="150" align="left">Topup Premium</td>
                                                    <td width="10" align="left">:</td>
                                                    <td width="290" align="left"><span id="emptopuppremiumresult"><?php echo $empDetails[0]['empTopupPremium']; ?>
                                                            <input type="hidden" id="empTopupPremium" name="empTopupPremium" class="emptopuppremium" value="<?php echo $empDetails[0]['empTopupPremium']; ?>">
                                                        </span></td>
                                                </tr>
                                            </table></td>
                                    </tr>
                                </table>  
                            </div>
                            <div class="cl"></div>
                        </div>

                        <div class="mid_shadow">
                        <div class="mid_shadow_head" <?= $headingColorsFromDb;?>>Family Details</div>

    <?php if (@$companydetails[0]['dependent'] == 'allowed')
    { ?>
                                <div class="sub_container">    
                                    <table width="965" border="0" cellspacing="0" cellpadding="0">
                                        <tr>      
                                            <td width="95" class="green_txt_center gray_devider1" style="line-height:32px;">Relation</td>
                                            <td width="95" class="green_txt_left gray_devider1" style="line-height:32px;">Title</td>
                                            <td width="95" class="green_txt_center gray_devider1" style="line-height:32px;">First Name</td>
                                            <td width="95" class="green_txt_center gray_devider1" style="line-height:32px;">Last Name</td>
                                            <td width="95" class="green_txt_center" style="line-height:32px;">Date Of Birth</td>
                                        <?php
                                        if (@$companydetails[0]['insuranceType'] != 'floater'){ ?>
                                            <td width="95" class="green_txt_center" style="line-height:32px;">Base SI</td>
                                            <td width="95" class="green_txt_center" style="line-height:12px;">Base SI Premium</td>
                                            <?php if (@$companydetails[0]['offerTopUp'] == 'yes'){ ?>
                                                <td width="95" class="green_txt_center" style="line-height:12px;">Top-up SI</td>
                                                <td width="95" class="green_txt_center" style="line-height:12px;">Top-up SI Premium</td>
                                            <?php }
                                        }
                                        ?>
                                            <td width="95" class="green_txt_center" style="line-height:12px;">Action</td>
                                        </tr>
                                            <?php for ($z = 1; $z <= 10; $z++)
                                            {
                                                ?>
                                                <tr class="res<?= $z; ?>" <?php if (($z > 1) && ($z > $empFamilyCount + 1))
                                            {
                                                ?>style="display:none;" <?php } ?>>
                                                        <td class="gray_devider1 general_txt_left" style="padding-left:5px;"> 
                                                            <select  class="input" style="width:75px;" name="relationship<?= $z; ?>" id="relationship<?= $z; ?>">
                                                                <?php foreach ($empFamilyList as $memberId => $member)
                                                                {
                                                                    ?>
                                                                    <option value="<?= lcfirst($allMembersList[$member['memberId']]); ?>" 
                                                                    <?php
                                                                            if (@$empFamilyDetails[$z - 1]['relation'] == lcfirst($allMembersList[$member['memberId']]))
                                                                            {
                                                                                echo 'selected';
                                                                            }
                                                                            ?>><?= $allMembersList[$member['memberId']]; ?></option>     
            <?php } ?>
                                                            </select>

                                                        </td>
                                                        <td class="gray_devider1 general_txt_left" style="padding-left:5px;">

                                                            <select  class="input" name="title<?= $z; ?>" id="title<?= $z; ?>">
                                                                <option value="MR" <?php
                                                                if (@$empFamilyDetails[$z - 1]['salutation'] == 'MR')
                                                                {
                                                                    echo 'selected';
                                                                }
                                                                else
                                                                {
                                                                    
                                                                }
                                                                ?>>MR</option>
                                                                <option value="MS" <?php
                                                                        if (@$empFamilyDetails[$z - 1]['salutation'] == 'MS')
                                                                        {
                                                                            echo 'selected';
                                                                        }
                                                                        else
                                                                        {
                                                                            
                                                                        }
                                                                        ?>>MS</option>				                            
                                                            </select>

                                                        </td>
                                                        <td class="gray_devider1 general_txt_center">

                                                            <input class="input" type="text" style="width:67px;"  id="firstName<?= $z; ?>" name="firstName<?= $z; ?>" value="<?= @$empFamilyDetails[$z - 1]['firstName'] ? $empFamilyDetails[$z - 1]['firstName'] : ""; ?>">

                                                        </td>
                                                        <td class="gray_devider1 general_txt_center">

                                                            <input class="input" type="text" style="width:67px;"  id="lastName<?= $z; ?>" name="lastName<?= $z; ?>" value="<?= @$empFamilyDetails[$z - 1]['lastName'] ? $empFamilyDetails[$z - 1]['lastName'] : ""; ?>">
                                                        </td>
                                                        <td class="gray_devider1 general_txt_center">
                                                            <input class="input fdob" type="text" style="width:80px;" id="fdob<?= $z; ?>" name="fdob<?= $z; ?>" value="<?= date('Y-m-d', @$empFamilyDetails[$z - 1]['fdob']); ?>" onkeyup="seletdob('<?= $z; ?>')" >
                                                        </td>
                                                                <?php
                                                                        if ($companydetails[0]['insuranceType'] != 'floater')
                                                                        {
                                                                            ?>
                                                            <td class="gray_devider1 general_txt_left" style="padding-left:5px;">
                                                                <span class="gradesires">	
                                                                    <?php echo @$sumInsuredAmount;?>
<!--                                                                    <select name="siId<?= $z; ?>" id="siId<?= $z; ?>" onchange="getSIPremium('<?= $empDetails[0]['companyId']; ?>', '<?= $z; ?>')" <?php
                                                                        if ($companydetails[0]['offerSumInsured'] == 'grade')
                                                                        {
                                                                            //echo 'disabled';
                                                                        }
                                                                        ?>>
                                                                        <option value="">Select</option>
                                                                        <?php
                                                                        
                                                                        $s = 0;
                                                                        //while ($s < count($suminsuredlist))
                                                                        {
                                                                            ?>
                                                                            <option value="////<?php echo $suminsuredlist[$s]['id']; ?>" <?php
                                                                if ($suminsuredlist[$s]['id'] == $empFamilyDetails[$z - 1]['sumInsuredId'])
                                                                {
                                                                    echo 'selected';
                                                                }
                                                                ?>><?php echo $suminsuredlist[$s]['sumInsured']; ?></option>
                                                                                <?php $s++;
                                                                            }
                                                                            ?>                   
                                                                    </select>-->
                                                                </span>
                                                            </td>
                                                            <td class="gray_devider1 general_txt_center">
                                                                <span id="suminsured_result<?= $z; ?>">
                                                                    <!--<input type="text" style="width:67px;" class="basesipremium input" disabled="disabled" value="<?php //echo @$empFamilyDetails[$z - 1]['sumInsuredPremium'];?>">-->
                                                                        <input type="hidden" id="sipremium<?= $z; ?>" name="sipremium<?= $z; ?>" class="-basesipremium" value="<?= @$premiumAmount;//echo @$empFamilyDetails[$z - 1]['sumInsuredPremium']; ?>">		 
                                                                           <?= @$premiumAmount; ?>
                                                                            </span>
                                                            </td>
                                                                                    <?php
                                                                                    if ($companydetails[0]['offerTopUp'] == 'yes')
                                                                                    {
                                                                                        if ($companydetails[0]['policyType'] != 'fixed')
                                                                                        {
                                                                                            ?>
                                                                                    <td class="gray_devider1 general_txt_left" style="padding-left:5px;">

                                                                                        <select  style="width:75px;" name="topSIId<?= $z; ?>" id="topSIId<?= $z; ?>" onchange="getTopupSIPremium('<?= $empDetails[0]['companyId']; ?>', '<?= $z; ?>')" style="min-height:20px !important;" >
                                                                                            <option value="">Select</option>
                                                                                                    <?php
                                                                                                    $s = 0;
                                                                                                    while ($s < count($topupsuminsuredlist))
                                                                                                    {
                                                                                                        ?>
                                                                                                <option value="<?php echo $topupsuminsuredlist[$s]['id']; ?>" <?php
                                                                                                        if ($topupsuminsuredlist[$s]['id'] == $empFamilyDetails[$z - 1]['topSumInsuredId'])
                                                                                                        {
                                                                                                            echo 'selected';
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            
                                                                                                        }
                                                                                                        ?>><?php echo $topupsuminsuredlist[$s]['sumInsured']; ?></option>
                                                                                                        <?php $s++;
                                                                                                    }
                                                                                                    ?>    				                            
                                                                                        </select>

                                                                                    </td>		
                                                                                    <td class="gray_devider1 general_txt_center">
                                                                                        <span id="topupsuminsured_result<?= $z; ?>">
                                                                                            <input type="text1" style="width:67px;"  disabled="disabled" value="<?= @$empFamilyDetails[$z - 1]['topSumInsuredPremium']; ?>">
                                                                                                <input type="hidden" id="empTopupPremium<?= $z; ?>" name="empTopupPremium<?= $z; ?>" value="<?= @$empFamilyDetails[$z - 1]['topSumInsuredPremium']; ?>">
                                                                                                    </span>
                                                                                                    </td>
                                                                                                        <?php } else { ?>
<!--                                                    <td class="gray_devider1 general_txt_left" style="padding-left:5px;" colspan="2">Fixed</td>-->
                                                    <td class="gray_devider1 general_txt_left" style="padding-left:5px;"><?= $selectedTopSI; ?></td>
                                                    <td class="gray_devider1 general_txt_left" style="padding-left:5px;"><?= $empDetails[0]['empTopupPremium']; ?></td>
                                                                                                        <?php
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                                ?>
                                                                                        <td class="gray_devider1 general_txt_center">
                                                                                            <input type="hidden" name="empFamilyId<?= $z ?>" id="empFamilyId<?= $z ?>" value="<?= @$empFamilyDetails[$z - 1]['id']; ?>" />
                                                                                            <span id="htmlreslut<?= $z ?>">
            <?php if ($z > $empFamilyCount)
            {
                ?>
                                                                                                    <b onclick="getFamilyHtml('<?= $z ?>')" style="cursor:pointer;"><img src="images/iphone-plus-sign-icon.jpg" height="20" /></b>
            <?php
            }
            else
            {
                if ($empFamilyDetails[$z - 1]['id'] != '')
                {
                    $familyid = $empFamilyDetails[$z - 1]['id'];
                }
                else
                {
                    $familyid = "0";
                }
                ?>
                                                                                                    <b onclick="removehtml('<?= $z ?>', '<?= $empFamilyDetails[$z - 1]['id']; ?>')" style="cursor:pointer;"><img src="images/minus_white.png" height="20"/></b>
            <?php } ?>
                                                                                            </span></td>
                                                                                        </tr>
                                                                                        <!--<script type="text/javascript">getSIPremium('<?= $_SESSION['companyId']; ?>','<?= $z ?>');</script>-->
        <?php } ?>
                                                                            </table>
                                                                            </div>
    <?php
    }
    else
    {
        echo '<div style="padding:10px;">Dependent are not Allowed</div>';
    }
    ?>

                                                                        <div class="cl"></div>
                                                                        <div style="text-align:center;" class="mid_content">
                                                                            <input type="hidden" name="companyId" id="companyId" value="<?= $_SESSION['companyId']; ?>" />
                                                                            <input type="hidden" name="empId" id="empId" value="<?= $_SESSION['empId']; ?>" />
                                                                            <input type="hidden" name="empFamilyCount" id="empFamilyCount" value="<?= $empFamilyCount; ?>" />
                                                                            <input type="hidden" name="insuranceType" id="insuranceType" value="<?= $companydetails[0]['insuranceType']; ?>" />
                                                                            <input type="hidden" name="dependent" id="dependent" value="<?= $companydetails[0]['dependent']; ?>" />
                                                                            <input type="hidden" name="policyType" id="policyType" value="<?= $companydetails[0]['policyType']; ?>" />
                                                                            <input type="hidden" name="offerSumInsured" id="offerSumInsured" value="<?= $companydetails[0]['offerSumInsured']; ?>" />
                                                                        </div>
                                                                        <div class="update">
                                                                            
    <input type="submit" value="Update" id="button" name="button" class="submit_button fr" <?= $buttonStyle ?>/>
  </div>
  <div class="cl"></div>
                                                                        
                                                                        </div></div>
                                                                        </form>
                                                                        <div class="footer_container">
                                                                            <div class="disclaimer_nav"> Insurance is the subject matter of solicitation | IRDA Registration No. 148<br>
                                                                                    Copyrights 2012, All right reserved by Religare Health Insurance Company Ltd.<br>
                                                                                        <span>This site is best viewed on Internet Explorer 7/8 and Fire Fox 3.x</span> </div>
                                                                                        <div class="cl"></div>
                                                                                        </div>


    <?php if ($companydetails[0]['offerSumInsured'] == 'age')
    { ?>
                                                                                            <script type="text/javascript">
                                                                                                getEmpSIPremium();
                                                                                            </script> 
    <?php } if ($companydetails[0]['offerSumInsured'] == 'grade')
    { ?>  
                                                                                            <script type="text/javascript">
                                                                                                getEmpGradePremium('<?= $_SESSION['companyId']; ?>');
                                                                                            </script> 
    <?php
    }
}
?>
                                                                                    </body>
                                                                                    </html>
