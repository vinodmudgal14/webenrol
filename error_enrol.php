<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/style_color.css">
    <style>
        .top_header1{
    border-top: 5px solid #a5a5a5;
    margin: 0;
    padding: 19px 0 10px;
    width: 100%;
        }
     .mid_shadow_text {
    background: #e4e2e3 none repeat scroll 0 0;
    color: #065a2d;
    font: 16px/31px Kozuka Gothic Pro B,Kozuka Gothic Pro,Arial,Helvetica,sans-serif;
    height: 32px;
    margin: auto;
    padding: 0 8px;
    width: 951px;
    </style>
<title>Url Deactivated</title>
</head>
<body>
<div class="top_header1">
  <div class="login_container"><img src="images/login_logo.jpg" border="0"></div>
</div>

<div class="mid_shadow">
  <div class="mid_shadow_text">Url Status</div>
  <div class="mid_content" style="text-align: center;">
      <img src="images/warning.png" border="0" style="text-align: center"></img>
      <p>Something went wrong with your link !</p>
      <p>Please contact with portal administrator's</p>
  </div>
  <div class="cl"></div>
</div>
<div class="footer_container">
  <div class="disclaimer_nav"> Insurance is the subject matter of solicitation | IRDA Registration No. 148<br>
    Copyrights 2016, All right reserved by Religare Health Insurance Company Ltd.<br>
    <span>This site is best viewed on Internet Explorer 7/8 and Fire Fox 3.x</span> </div>
  <div class="cl"></div>
</div>
</body>
</html>
