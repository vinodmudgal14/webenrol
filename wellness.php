<?php 
session_start();
session_name();
include('admin/conf/conf.php');
include('admin/conf/fucts.php'); 
$compurl = $_COOKIE["compurl"];
if($_SESSION['empId']==''){
	$url='Location: company_'.$compurl;
	session_destroy();
	$_SESSION["empId"]='';		
	$_SESSION["companyId"]='';
	$_SESSION["companyName"]='';
	$_SESSION["compurl"]='';
	setcookie("compurl","",time()-90000);
	header($url);
	exit;
}
$companyWellness =  companyWellnessDetails(@$_SESSION['companyId']);
$wellnessArray = array();
for($i=0;$i<count($companyWellness);$i++){
$wellnessArray[]	=	$companyWellness[$i]['wellness'];
	if($companyWellness[$i]['wellness']=='HRA'){ $hraStartDate =  $companyWellness[$i]['startDate'];	$hraEndDate =  $companyWellness[$i]['endDate'];	}
	if($companyWellness[$i]['wellness']=='Doctor on call'){ $callStartDate =  $companyWellness[$i]['startDate']; $callEndDate =  $companyWellness[$i]['endDate'];	}
	if($companyWellness[$i]['wellness']=='Doctor on chat'){ $chatStartDate =  $companyWellness[$i]['startDate']; $chatEndDate =  $companyWellness[$i]['endDate'];	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Religare</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/wellness.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
var win=null;
function NewWindow(mypage,myname,w,h,scroll,pos){
if(pos=="random"){LeftPosition=(screen.width)?Math.floor(Math.random()*(screen.width-w)):100;TopPosition=(screen.height)?Math.floor(Math.random()*((screen.height-h)-75)):100;}
if(pos=="center"){LeftPosition=(screen.width)?(screen.width-w)/2:100;TopPosition=(screen.height)?(screen.height-h)/2:100;}
else if((pos!="center" && pos!="random") || pos==null){LeftPosition=0;TopPosition=20}
settings='width='+w+',height='+h+',top='+TopPosition+',left='+LeftPosition+',scrollbars='+scroll+',location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=yes';
win=window.open(mypage,myname,settings);}
</script>
</head>

<body>
<div class="top_container"><img src="images/religare_logo.jpg" alt="Religare" class="fl" />
<div class="welcome_area">
<a href="signout.php" class="logout">Log Out</a></div>
</div>

<div class="mid_nav">
<ul class="mainnav">
<li><a href="my_policy.php">My&nbsp;Details</a></li>
<li><a href="enquiry.php">Change Request</a></li>
<li><a href="wellness.php" class="selected">Wellness</a></li>
</ul>
</div>

<div class="page_nav">
    <a href="#">Home</a> &raquo; Wellness
</div> 
<div class="mid_container">
<div class="mid_shadow">
<div class="mid_content">
  <div class="comission_area"><!--<img class="fl" src="images/top_964_shadow.jpg">-->
<div class="download_area">
  <div class="wellness_Count">
<table width="100%" cellspacing="0" cellpadding="0" border="0">    
    <tbody><tr>
	<?php 
	if((in_array("HRA",$wellnessArray))&&($hraStartDate<time())&&($hraEndDate>=time())){ 
	?>
      <td valign="top">
      <div class="CountWellness">
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
  <tbody><tr>
    <td valign="top" class="CountWellness_topLeft">&nbsp;</td>
    <td valign="top" class="CountWellness_topMid">Health Risk Assessment </td>
    </tr>
  <tr>
    <td valign="top" class="wellnessBDR_left">&nbsp;</td>
    <td valign="top" class="wellnessBDR_right">
	<form name="hra_form" id="hra_form"  target="_blank" method="post" action="<?php echo @$hraActionUrl;?>">
     <span>Health Risk Assessment</span> is an online questionnaire based application, which empowers you analyze your health status and identify health risks early.      <br>
            <br>
        
            <span>HRA</span> helps in early identification and management of risks, promotion of preventive healthcare, regular follow up and monitoring to ensure effective management of health status.			
			<span style="float:right;padding-top:10px; cursor:pointer;">
			<input type="hidden" name="policy_id" value="" />
			<input type="hidden" name="member_id" value="<?php echo @$_SESSION['empId'];?>" />
			<input type="hidden" name="corporate_id" value="<?php echo @$_SESSION['corporateid'];?>" />
			<input type="hidden" name="enable_hra_form" value="Y" />
			<input type="hidden" name="start_date" value="<?php echo date('Y-m-d',@$hraStartDate);?>" />
			<input type="hidden" name="end_date" value="<?php echo date('Y-m-d',@$hraEndDate);?>" />
			<input type="hidden" name="gender" value="<?php if($_SESSION['GENDER']=='M') { echo 'Male'; } else { echo 'Female'; } ?>" />
			<input type="hidden" name="email" value="<?php echo @$_SESSION['empEmail'];?>" />
			<input type="image" src="images/assessment_btn.png" title="Health Risk Assessment" alt="Health Risk Assessment" />
			</span>
			</form>
			</td>
    </tr>
  <tr>
    <td valign="top" class="CountWellness_BotLeft">&nbsp;</td>
    <td valign="top" class="CountWellness_BotRight">&nbsp;</td>
    </tr>
</tbody></table>
      </div>                </td>
	  <?php }  if((in_array("Doctor on chat",$wellnessArray))&&($chatStartDate<time())&&($chatEndDate>=time())){  ?>
      <td valign="top"><div class="CountWellness">
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
  <tbody><tr>
    <td valign="top" class="CountWellness_topLeft">&nbsp;</td>
    <td valign="top" class="CountWellness_topMid_Doc_on_chat">Doctor on Chat </td>
    </tr>
  <tr>
    <td valign="top" class="wellnessBDR_left">&nbsp;</td>
    <td valign="top" class="wellnessBDR_right">
    Religare Health tele-consult offers a <span>24X7</span> web chat facility to the Customers. Customers can take advice from our Doctors through chat facility from anywhere. 	<br>
<br>


The solution was created with the objective of Improving access to medical/ health advice, Facilitating the most appropriate use of medical/ health facilities etc.<span style="float:right;padding-top:10px; cursor:pointer;">
<a  href="doctor_on_chat.php?policy=<?php echo @$_SESSION['policyId'];?>" onclick="NewWindow(this.href,'mywin','420','290','yes','center');return false" onfocus="this.blur()" ><img src="images/doc_on_chat_btn.png" title="Doctor on Chat" alt="Doctor on Chat" style="cursor:pointer;" /></a>
</span></td>
    </tr>
  <tr>
    <td valign="top" class="CountWellness_BotLeft">&nbsp;</td>
    <td valign="top" class="CountWellness_BotRight">&nbsp;</td>
    </tr>
</tbody></table>

      
      </div></td>
	  <?php }  if((in_array("Doctor on call",$wellnessArray))&&($callStartDate<time())&&($callEndDate>=time())){ ?>
      <td valign="top"><div class="CountWellness">
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
  <tbody><tr>
    <td valign="top" class="CountWellness_topLeft">&nbsp;</td>
    <td valign="top" class="CountWellness_topMid_Doc_on_call">Doctor on Call </td>
    </tr>
  <tr>
    <td valign="top" class="wellnessBDR_left">&nbsp;</td>
    <td valign="top" class="wellnessBDR_right">
      <span>Religare Health </span>tele-consult offers a <span>24X7 </span>voice based tele-triage system, which offers its customers the ability to get a doctor's advice over the phone. <br>
<br>


This service can be availed by calling our Customer care Number XXXXXXX and our Customer care representative will connect the Customer to the <span>Religare</span> Health tele-consult</td>
    </tr>
  <tr>
    <td valign="top" class="CountWellness_BotLeft">&nbsp;</td>
    <td valign="top" class="CountWellness_BotRight">&nbsp;</td>
    </tr>
</tbody></table>

      
      </div></td>
	  <?php }	?>
    </tr>
  </tbody></table>

</div>
  
</div>
<!--<img class="fl" src="images/bottom_964_shadow.jpg">--></div>
</div>

<div class="cl"></div></div>
</div>
<div class="footer_container_in">
<div class="disclaimer_nav"><!--<a href="#">Claim Forms</a>   |   <a href="#">Disclaimer</a>   |   <a href="#">Privacy Statement</a>   |   <a href="#">Terms & Conditions</a>   |   <a href="#">Feedback</a>   |   <a href="#">Sitemap</a>--><br>
      <span class="copyright">Insurance is the subject matter of solicitation | IRDA Registration No. 148<br /> 
Copyrights 2012, All right reserved by Religare Health Insurance Company Ltd.</span><br /> 
  <span class="site_viewed">This site is best viewed on Internet Explorer 7/8 and Fire Fox 3.x</span> </div>     
      
<div class="cl"></div></div>  
</body>
</html>