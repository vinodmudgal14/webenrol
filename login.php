<?php error_reporting(0);
include_once("admin/conf/conf.php");
include_once("admin/conf/fucts.php");

$employeeDetail = sanitize_data(@$_REQUEST['u']);
$urlchk = base64_decode($employeeDetail);
$urlchkexp = explode(":", $urlchk);
if (count($urlchkexp)> 5) {
    $empEmail = $urlchkexp[0];
    $eStartDate = $urlchkexp[4];
    $eEndDate = $urlchkexp[5];
    if(ctype_digit($enrolStartDate)){
		$sDate=$eStartDate;
	}else{
		$sDate=strtotime(date_format(date_create_from_format("d/m/Y",$eStartDate),"Y-m-d"));
	}
    if(ctype_digit($enrolStartDate)){
		$eDate=$eEndDate;
	}else{
		$eDate=strtotime(date_format(date_create_from_format("d/m/Y",$eEndDate),"Y-m-d"));
	}
}
else die("Unknown Request.");
?>
<html>
    <head>
        <title>Religare Health Insurance - Best Health & Medical Insurance Online in India</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Login</title>
        <style>
            table {width: 100%;height: 80%}
            .form_container{height: 60%;/*margin: auto;padding: 3%;*/ }
            .form_container span h4 {display:inline-block;font-size:20px;color:#0F6633;}
            .adiv {
                width: 700px;
                height: 100px;
                /*background-color: red;*/
                position: absolute;
                top:0; bottom: 0; left: 0; right: 0; margin: auto;
            }
            /*.loginmaindiv{margin-left: 125px !important;margin-right: 50px !important;}*/
            .companylogo{width:30%;float:left;}
            /*.cred_div{width:70%;display:inline-block;}*/
            /*.logincsdiv{display:none;width: 600px;position: fixed;right: 280px;top: 80px;background: #EFEFEF none repeat scroll 0% 0%;display: none;border: 1px solid #D8D8D7;z-index: 99999;box-shadow: 0px 4px 4px #333;}*/
            .logincsdiv{height: 418px;width: 600px;top:0; bottom: 0; left: 0; right: 0; margin: auto;display:none; position: fixed; background: #EFEFEF none repeat scroll 0% 0%;display: none;border: 1px solid #D8D8D7;z-index: 99999;box-shadow: 0px 4px 4px #333;}
            
            .logincsdiv1{padding: 20px 25px;position: relative;}
            /*.logincsdiv1{position: relative; top:0; bottom: 0; left: 0; right: 0; margin: auto;}*/
            .removediv{width: 30px;height: 30px;position: absolute;right: 10px;top: 10px;background: url('images/grCross.png') no-repeat scroll center center;cursor: pointer;}
            .logindisplay{width: auto;float: left;position: absolute;top: 20px;height: 47px;}
            .logindisplay ul {width: 100%; float: left; padding: 0px; margin: 0px;list-style: outside none none;}
            .logindisplay ul li {float: left;padding: 0px;margin: 0px;list-style: outside none none;}
            .logindisplay ul li a {float: left;display: block;padding: 12px 10px;border-top: 1px solid #87A469;border-right: 1px solid #87A469;border-left: 1px solid #87A469;-moz-border-top-colors: none;-moz-border-right-colors: none;-moz-border-bottom-colors: none;-moz-border-left-colors: none;border-image: none;border-bottom: 1px solid #E0F0D0 !important;color: #0F0F0F;font: 16px/22px "FS Humana",Tahoma,Helvetica,sans-serif;text-decoration: none;
                                   background: #E0F0D0 none repeat scroll 0% 0%;}
            .loginpopupcontent{width: 100%;float: left;margin-top: 47px;background: #E0F0D0 none repeat scroll 0% 0%;border-top: 1px solid #87A469;}
            .loginpopupcontent1{padding: 15px 22px;}
            .loginpopupcontent1 h3 {padding: 0px;width: 100%;margin: 0px;float: left;color: #0F6633;font: 16px/18px "FS Humana",Tahoma,Helvetica,sans-serif;}
            .loginpopupcontent1 p {padding: 11px 0px;width: 100%;margin: 0px;float: left;color: #0F0F0F;font: 14px/16px "FS Humana",Tahoma,Helvetica,sans-serif;}
            .input_username{width: 80%;float: left;background: #FFF none repeat scroll 0% 0%;border: 1px solid #D7D7D7;margin: 4px 10px 4px 0px;padding: 9px 12px;color: #8A8A8A;font: 14px/18px "FS Humana",Tahoma,Helvetica,sans-serif;}
            .input_password{width: 80%;float: left;background: #FFF none repeat scroll 0% 0%;border: 1px solid #D7D7D7;margin: 4px 10px 4px 0px;padding: 9px 12px;color: #8A8A8A;font: 14px/18px "FS Humana",Tahoma,Helvetica,sans-serif;}
            .submit_button{float: left;color: #FFF;font: 18px/22px "FS Humana",Tahoma,Helvetica,sans-serif;text-decoration: none;border: 1px solid #000;
                           background: #333 url("images/getqArrow.png") no-repeat scroll right center;
                           padding: 7px 35px 7px 10px;}
            .password_error{color:red;font-size:14px;}
            .username_error{color:red;font-size:14px;}

        </style>
        <!--[if lt IE 9]>
        <script src="cpjs/html5.js"></script>
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="css/singlesignin.css">
        <!--<link rel="stylesheet" type="text/css" href="../cpcss/stylesheet.css" media="all">-->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript">
//            (function () {
//                var hm = document.createElement('script');
//                hm.type = 'text/javascript';
//                hm.async = true;
//                hm.src = ('++u-heatmap-it+log-js').replace(/[+]/g, '/').replace(/-/g, '.');
//                var s = document.getElementsByTagName('script')[0];
//                s.parentNode.insertBefore(hm, s);
//            })();
        </script>
    </head>
    <body style="background:<?php echo @$compdetailarr[0]['themeColor'] ? "#" . $compdetailarr[0]['themeColor'] : "rgb(255, 255, 255)"; ?>">
        <?php
        
        $browser = get_browser(null, true);
        $tax_policynumber = sanitize_data_email(@$_POST['tax_policynumber']);
        $code = strtolower(sanitize_data(@$_POST['code']));
        $dob1 = sanitize_data(@$_POST['dob']);
        $dob = date("Y-m-d", strtotime(str_replace("/", "-", $dob1)));
        ?>
        <!--<link rel="stylesheet" type="text/css" href="../cpcss/datepiker.css">-->
        <?php /*
        
        if (($browser['browser'] == 'IE' && $browser['version'] == '7.0') || ($browser['browser'] == 'IE' && $browser['version'] == '6.0')) {
            ?>
            <script type="text/javascript">
                location.href = "<?php echo SITEURL; ?>oldwebsite/<?php echo basename($_SERVER["REQUEST_URI"]); ?>";
            </script>
            <?php
        } */
        ?>
        <div class="screenlock"></div>
        <header>
            <a href="./<?php echo $urlString; ?>" class="logo"></a>
            <div class="cl"></div>
        </header>
        <div class="topBanner" style="color:#ffffff; background-size:100% auto;height:430px;">
            <div class="topBannerIn">
                <div class="loginmaindiv">
                    <div class="cred_div">
                        <div class="form_container adiv" id="form_container">
                            <span><h4>Employee Web Enrollment* : </h4></span>
                            <?php $currTime=time();
                            //echo "$sDate | ".$currTime." | $eDate";
                            /*if($eDate < $currTime){
                                echo '<span style="color:f00">Enrolment has been close.</span>';
                            }
                            elseif($sDate>$currTime){
                                echo '<span style="color:f00">Enrolment has not started yet. Visit again after '.$eStartDate.'</span>';
                                
                            }else*/{
                                echo '<a href="company_'.$_GET["u"].'" class="" role="button">Click to Enrol into Webenrol</a>';
                            }
                            ?>
                            <br>
                            <span style="display: inline-block;margin-top: 12px;"><h4>Employee Portal login** &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</h4></span>
                            <?php /*
							if($eDate>$currTime){
                                echo '<span style="color:f00">Enrolment has not completed yet.</span>';
                            }
                            else*/{
                                echo '<a href="javascript:void(0);" class="login_cs">Click to Login in Employee Portal</a>';
                            }
                            ?>
                            <br><br><br><span style="color:f00">**You will receive login credential after few days. Then you'll be able to login</span>
                        </div>
                    </div>
                </div>
            </div>
            <div id="clickonlogincs" class="logincsdiv">
                <div class="logincsdiv1" style="">
                    <div class="removediv"></div>
                    <div class="logindisplay">
                        <ul>
                            <li><a href="javascript:void(0);" style="">Customer Login</a></li>
                        </ul>
                    </div>
                    <div class="loginpopupcontent">
                        <div class="loginpopupcontent1">
                            <h3>Customer Login</h3>
                            <p>To Login in Customer Portal</p>
                            <div class="" style="padding: 2px 0px;width: 100%;float: left;">
                                <!--<form method="post" action='http://localhost/customerlogin/index.php' name="login_form" autocomplete="off">
								<form method="post" action='https://myuat.religarehealthinsurance.com:9443/index.php' name="login_form" autocomplete="off">
                <form method="post" action='http://localhost/customerlogin/index.php' name="login_form" autocomplete="off">-->
								<form method="post" action='https://my.religarehealthinsurance.com/index.php' name="login_form" autocomplete="off">
                
                                    <p><input type="text" class="input_username" name="username" id="username" value="<?php
                                        if (isset($empEmail) && !empty($empEmail)) {
                                            echo $empEmail;
                                        } else {
                                            echo "";
                                        }
                                        ?>" placeholder="Username"></p>
                                    <span class="username_error"></span>
                                    <p><input type="password" class="input_password" name="password" id="password" placeholder="Password"></p>
                                    <span class="password_error"></span>
                                    <p><input type="hidden" class="span3" name="userType" value="CORPORATE"></p>
                                    <p><input type="hidden" class="span3" name="fake_Password" value="Password*"></p>
                                    <p><button type="submit" onclick="return validatefields();" class="submit_button">Sign in</button>
                                    </p>
                                </form>
                            </div>
                            <div class="cl"></div></div>
                    </div>
                    <div class="cl"></div>
                </div>
            </div>
            
        </div>
           
        <!--<script src="../cpjs/datepiker.js"></script>--> 
        <!--<script src="../cpjs/jquery.colorbox.js"></script>-->        
        <!--<script type="text/javascript" src="../cpjs/script.js"></script>-->
        <script>
//            var DT = jQuery.noConflict();
//            DT(function () {
//                DT('#datepicker').datepicker({
//                    changeMonth: true,
//                    changeYear: true,
//                    yearRange: "-99:-0",
//                    dateFormat: "dd/mm/yy"
//
//                });
//            });
            
            $(function(){
                $(".login_cs").click(function () {
                    $('#clickonlogincs').toggle();
                });
                $(".removediv").click(function () {
                    $('#clickonlogincs').hide();

                });
                $("#username").keyup(function () {
                    if ($("#username").val() != '') {
                        $('.username_error').html('');
                    }
                });
                $("#password").keyup(function () {
                    if ($("#password").val() != '') {
                        $('.password_error').html('');
                    }
                });
            });
            
            function validatefields()
            {
                var username = $("#username").val();
                var password = $("#password").val();
                if ($.trim(username) == '')
                {
                    $('.username_error').html('Please Enter Username');
                    $("#username").focus();
                    return false;
                }
                if ($.trim(password) == '')
                {
                    $('.password_error').html('Please Enter Password');
                    $("#password").focus();
                    return false;
                }
            }
        </script>
    </body>
</html>