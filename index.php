<?php 
include_once("admin/conf/conf.php"); 
include_once("admin/conf/fucts.php"); 
$company		=	sanitize_data(@$_REQUEST['company']);
//print_r($_SESSION);
$compdetailarr = chkcompanyurl($company);
//echo '<pre>';
//print_r($compdetailarr);
//exit;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Religare</title>
<link rel="stylesheet" href="css/style.css"/>
<link rel="stylesheet" href="css/style_color.css"/>
<!--<link href="css/style.css" rel="stylesheet" type="text/css" />-->
<script src="admin/js/jquery.js" type="text/javascript"></script>
<script src="admin/js/jquery.livequery.js" type="text/javascript"></script>
<script type="text/javascript" src="admin/js/ui.datepicker.js"></script>
<link href="admin/css/ui.datepicker.css" rel="stylesheet" type="text/css">
<script language="javascript">
$(document).ready(function() {
	$("#dateOfBirth").datepicker({
		yearRange: '1910:<?php echo date('Y');?>',
		dateFormat: "yy-mm-dd"
		});
});
function isInteger(s)
{   var i;
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}
function isEmpty(s)
{
	var re = /\s/g; //Match any white space including space, tab, form-feed, etc. 
	var s = s.replace(re, "");
	if (s.length == 0) 
	{
		return true;
	}
	else 
	{
		return false;
	}
} 
function validate(){
		var EmailRegex=/^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,6})$/i;;
		var FromEmail = document.loginchk.empEmail.value;
		var dob 	= $("#dateOfBirth").val();
		var empNo 	= $("#empNo").val();
		
		if(isEmpty(document.loginchk.empEmail.value)||( FromEmail!="" && (EmailRegex.test(FromEmail)) != true)||( FromEmail=="" ))
		{
		alert("Please enter valid Email Id (abc@xyz.com)");
		document.getElementById('empEmail').focus();
		return false;
		}			
			
		if(empNo==''){ 
			if(document.loginchk.dateOfBirth.value=="")
			{
			alert("Please enter either employee number or date of birth");
			document.loginchk.dateOfBirth.focus();
			return false;
			}		
		
		} else {	
			if(document.loginchk.empNo.value=="")
			{
			alert("Please enter the employee number");
			document.loginchk.empNo.focus();
			return false;
			}	
		}		
}
</script>
</head>

<body style="background:<?php echo @$compdetailarr[0]['themeColor'] ? "#".$compdetailarr[0]['themeColor'] : "#b7db82"; ?>">
    
<div class="logo_header"><img src="<?php echo @$compdetailarr[0]['logoFile'] ? "imagetest/".$compdetailarr[0]['logoFile'] : "images/login_logo.jpg"; ?>" border="0">
  <div class="cl"></div>
</div>
<?php if(sanitize_data(@$_REQUEST['msg'])=='error'){ ?>
<div style="text-align:center; color:#FF0000; font-weight:bold;">Employee Number/Email-id or Date of birth did not match</div>
<?php } ?>
<div class="login_container">
 <div class="login_innerdiv">
<form name="loginchk" id="loginchk" action="login_check.php" method="post" onsubmit="return validate()">
  <table width="440" cellspacing="0" cellpadding="0" border="0">
      <tr>
        <td width="102" valign="middle" align="left">Company Name</td>
        <td width="49" valign="middle" align="center">:</td>
        <td width="289" valign="middle" align="left"><?php echo sanitize_data($compdetailarr[0]['companyName']);?></td>
      </tr>
      <tr>
        <td width="102" valign="middle" align="left">Email</td>
        <td width="49" valign="middle" align="center">:</td>
        <td width="289" valign="middle" align="left"><input type="text" class="login_input" name="empEmail"></td>
      </tr>
      <tr>
        <td colspan="3" align="center" valign="middle">-------------------------OR----------------------------------------------------------------</td>
      </tr>
      <tr>
        <td width="102" valign="middle" align="left">Employee No</td>
        <td width="49" valign="middle" align="center">:</td>
        <td width="289" valign="middle" align="left"><input type="text" class="login_input" name="empNo"></td>
      </tr>
      <tr>
        <td width="102" valign="middle" align="left">Date of Birth</td>
        <td width="49" valign="middle" align="center">:</td>
        <td width="289" valign="middle" align="left"><input type="text" class="login_input" name="dateOfBirth"></td>
      </tr>
      <?php if($compdetailarr[0]['id']!=''){ ?>
        <tr>
          <td width="102" valign="middle" align="left">&nbsp;</td>
          <td width="49" valign="middle" align="center">&nbsp;
              <input type="hidden" name="cid" id="cid" value="<?=$compdetailarr[0]['id'];?>" />
              <input type="hidden" name="url" id="url" value="<?=$company;?>" />
          </td>
          
          <!--<td height="45" align="left" valign="top"><input type="image" name="button" id="button" value="Submit" src="images/submit_btn.png"/></td>-->
          <td width="289" valign="middle" align="left"><input type="submit" class="submit_button" id="button" name="button" value="Submit"/></td>
          </tr>
		<?php } ?>
    </table>
    <table>
      <tr>
        <td width="58" align="left">Email Id:</td>
        <td width="430" align="left"><a href="mailto:customerfirst@religarehealthinsurance.com" target="_blank">customerfirst@religarehealthinsurance.com</a></td>
      </tr>
      <tr>
        <td width="90" align="left">Toll Free No.:</td>
        <td width="430" align="left">1800-200-4488</td>
      </tr>
    </table>  
  </form>
</div>
<div class="cl"></div></div>

<div class="login_footer_container">Insurance is the subject matter of solicitation | IRDA Registration No. 148<br />
Copyrights 2012, All right reserved by Religare Health Insurance Company Ltd.<br />
<span>This site is best viewed on Internet Explorer 7/8 and Fire Fox 3.x</span></div>
</body>
</html>
