<?php 
session_start();
session_name();
include('admin/conf/conf.php');
include('admin/conf/fucts.php'); 
$compurl = $_COOKIE["compurl"];
if($_SESSION['empId']==''){
	$url='Location: company_'.$compurl;
	session_destroy();
	$_SESSION["empId"]='';		
	$_SESSION["companyId"]='';
	$_SESSION["companyName"]='';
	$_SESSION["compurl"]='';
	setcookie("compurl","",time()-90000);
	header($url);
	exit;
}
    $companyWellness        =  chkCompanyWellness(@$_SESSION['companyId'],time());
    $companydetails         = companydetails($_SESSION['companyId']);
    
    
    if($companydetails[0]['defaultTheme'] != 1)
    {    
        $menuColor              = @$companydetails[0]['menuColor'] ? "style=color:#".$companydetails[0]['menuColor']. ";" : "";
        $menuBgColor            = @$companydetails[0]['menuBgColor'] ? "style=background:#".$companydetails[0]['menuBgColor']. ";" : "style=background:#b7db82";
        $menuActiveArrowColor   = @$companydetails[0]['menuColor'] ? "style=border-top-color:#".$companydetails[0]['menuColor']. ";" : "";
        $menuArrowColor         = @$companydetails[0]['menuColor'] ? "style=border-top-color:#".$companydetails[0]['menuBgColor']. ";" : "";
        $headingColor           = @$companydetails[0]['headingColor'] ? "color:#".$companydetails[0]['headingColor']. ";" : "";
        $headingBgColor         = @$companydetails[0]['headingBgColor'] ? "background:#".$companydetails[0]['headingBgColor']. ";" : "";

        $menuColorsFromDb       = "style=".$menuColor.$menuBgColor;
        $headingColorsFromDb    = "style=".$headingColor.$headingBgColor;   
    }
    else
    {
        $menuColor              = "";
        $menuActiveArrowColor   = "";
        $menuArrowColor         = "";
        $menuBgColor            = "style=background:#b7db82";
        $headingColor           = "";
        $headingBgColor         = "";
        $buttonColor            = "";
        $buttonBgColor          = "";
        $menuColorsFromDb       = "";
        $headingColorsFromDb    = "";  
        $buttonStyle            = "";
    }
    
    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Religare</title>
<!--<link href="css/style.css" rel="stylesheet" type="text/css" />-->
<link rel="stylesheet" href="css/style.css"/>
<link rel="stylesheet" href="css/style_color.css"/>
</head>

<body>

<!--<div class="top_container"><img src="images/religare_logo.jpg" alt="Religare" class="fl" />
<div class="welcome_area">
<a href="signout.php" style="display:none" class="logout">Log Out</a></div>
</div>

<div class="mid_nav">
<ul class="mainnav">-->
<!--  <li><a href="my_policy.php">My&nbsp;Details</a></li>
<li><a href="enquiry.php" class="selected">Change Request</a></li>
<?php 
if(count($companyWellness)>0){
?>
<li><a href="wellness.php">Wellness</a></li>
<?php }	?>
</ul>
</div>  -->
    
    
<div class="top_header">
                <div class="login_container"><img src="<?php echo $companydetails[0]['logoFile'] ? "imagetest/" . $companydetails[0]['logoFile'] : "images/religare_logo.jpg"; ?>" border="0"><a class="logout" href="signout.php" style="display:none">Log Out</a></div>
            </div>
    
<div class="mid_nav">
                <ul class="mainnav" <?= $menuBgColor;?>>
                    <li><a href="my_policy.php"> <span class="arrow-down" <?= $menuArrowColor;?>>&nbsp;</span> <span class="activetext" <?= $menuColor;?>>My&nbsp;Details</span> </a></li>
                    <li><a class="active" href="enquiry.php"> <span class="arrow-down" <?= $menuActiveArrowColor;?>>&nbsp;</span> <span class="activetext" <?= $menuColor;?>>Change Request</span> </a></li>
            <?php if (count($companyWellness) > 0)
            {
                ?>
                        <li><a href="wellness.php">Wellness</a></li>
    <?php } ?>
                </ul>
            </div>    
    


<div class="page_nav">
    <div class="page_nav"><a href="my_policy.php">Home</a> &raquo; Change Request
</div> 
<?php if(@$_REQUEST['msg']=='success'){	?>
<div style="color: red;padding: 10px;text-align: center;">Request has been sent successfully.</div>
<?php } ?>
<div class="mid_container">
<form name="changeRequest" id="changeRequest" method="post" action="writeChangeRequest.php"/>
<div class="mid_shadow">
<div class="mid_shadow_head" <?= $headingColorsFromDb;?>>Enquiry Form</div>

<div class="mid_content">
  <table width="950" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="476" align="left" valign="top"><table width="400" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="107" height="40" align="left" valign="middle" class="general_txt">First Name</td>
          <td width="31" height="40" align="center" valign="middle" class="general_txt">:</td>
          <td width="262" height="40" align="left" valign="middle" class="general_txt"><div class="dropdown_2">
        <input type="text" name="empFirstName" id="empFirstName" class="email_f input" style="width:200px;" />
    	</div></td>
        </tr>
        <tr>
          <td height="40" align="left" valign="middle" class="general_txt">Last name</td>
          <td height="40" align="center" valign="middle" class="general_txt">:</td>
          <td height="40" align="left" valign="middle" class="general_txt"><div class="dropdown_2">
        <input type="text" name="empLastName" id="empLastName" class="email_f input" style="width:200px;" />
    	</div></td>
        </tr>
        
        <tr>
          <td height="40" align="left" valign="middle" class="general_txt">Phone</td>
          <td height="40" align="center" valign="middle" class="general_txt">:</td>
          <td height="40" align="left" valign="middle" class="general_txt"><div class="dropdown_2">
        <input type="text" name="empPhone" id="empPhone" class="email_f input" style="width:200px;" />
    	</div></td>
        </tr>
        <tr>
          <td height="40" align="left" valign="middle" class="general_txt">Email ID</td>
          <td height="40" align="center" valign="middle" class="general_txt">:</td>
          <td height="40" align="left" valign="middle" class="general_txt"><div class="dropdown_2">
        <input type="text" name="empEmail" id="empEmail" class="email_f input" style="width:200px;" />
    	</div></td>
        </tr>
        
        
      </table></td>
      <td width="474" align="right" valign="top" class="gray_devider"><table width="400" border="0" cellspacing="0" cellpadding="0">       
        
        <tr>
          <td width="107" height="40" align="left" valign="middle" class="general_txt">Change Request</td>
          <td width="31" height="40" align="center" valign="middle" class="general_txt">:</td>
          <td width="262" height="40" align="left" valign="middle" class="general_txt"><div class="txtarea_bg">
            <textarea name="crDesc" id="crDesc" cols="45" rows="5" class="txtarea_l input"></textarea>
          </div></td>
        </tr>
        
      </table></td>
    </tr>
  </table>
</div>
<div class="mid_content" style="text-align:center;">
<input type="hidden" name="empId" id="empId" value="<?=$_REQUEST['empId'];?>" />
<input type="hidden" name="companyId" id="companyId" value="<?=$_REQUEST['companyId'];?>" />
<input type="submit" value="Update" id="button" name="button" class="submit_button" />
</div>
<div class="cl"></div>    
</div>
</form></div>
<div class="footer_container">
                                                                            <div class="disclaimer_nav"> Insurance is the subject matter of solicitation | IRDA Registration No. 148<br>
                                                                                    Copyrights 2012, All right reserved by Religare Health Insurance Company Ltd.<br>
                                                                                        <span>This site is best viewed on Internet Explorer 7/8 and Fire Fox 3.x</span> </div>
                                                                                        <div class="cl"></div>
                                                                                        </div>
</body>
</html>
