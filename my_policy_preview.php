<?php 
session_start();
session_name();
include('admin/conf/conf.php');
include('admin/conf/fucts.php'); 
$compurl = $_COOKIE["compurl"];
if($_SESSION['empId']==''){
	$url='Location: company_'.$compurl;
	session_destroy();
	$_SESSION["empId"]='';		
	$_SESSION["companyId"]='';
	$_SESSION["companyName"]='';
	$_SESSION["compurl"]='';
	setcookie("compurl","",time()-90000);
	header($url);
	exit;
}
$companyWellness =  chkCompanyWellness(@$_SESSION['companyId'],time());
$empDetails = companyEmpDetails($_SESSION['companyId'],$_SESSION['empId']);
$empFamilyDetails = getEmpFamily($_SESSION['companyId'],$_SESSION['empId']);
$gradeDetails = gradeList($_SESSION['companyId']);
$suminsuredlist = sumInsuredMapList($_SESSION['companyId']);
$topupsuminsuredlist = topUpSumInsuredMapList($_SESSION['companyId']);
$companydetails = companydetails($_SESSION['companyId']);
if($companydetails[0]['defaultTheme'] != 1)
{    
    $menuColor              = @$companydetails[0]['menuColor'] ? "style=color:#".$companydetails[0]['menuColor']. ";" : "";
    $menuActiveArrowColor   = @$companydetails[0]['menuColor'] ? "style=border-top-color:#".$companydetails[0]['menuColor']. ";" : "";
    $menuArrowColor         = @$companydetails[0]['menuColor'] ? "style=border-top-color:#".$companydetails[0]['menuBgColor']. ";" : "style=background:#b7db82";
    $menuBgColor            = @$companydetails[0]['menuBgColor'] ? "style=background:#".$companydetails[0]['menuBgColor']. ";" : "";
    $headingColor           = @$companydetails[0]['headingColor'] ? "color:#".$companydetails[0]['headingColor']. ";" : "";
    $headingBgColor         = @$companydetails[0]['headingBgColor'] ? "background:#".$companydetails[0]['headingBgColor']. ";" : "";
    $buttonColor            = @$companydetails[0]['buttonColor'] ? "color:#".$companydetails[0]['buttonColor'].";" : "";
    $buttonBgColor          = @$companydetails[0]['buttonBgColor'] ? "background:#".$companydetails[0]['buttonBgColor'] . ";" : "";

    $menuColorsFromDb       = "style=".$menuColor.$menuBgColor;
    $headingColorsFromDb    = "style=".$headingColor.$headingBgColor;   
    $empFamilyCount         = count($empFamilyDetails);    
    $buttonStyle            = "style=". $buttonColor .$buttonBgColor;
}
else
{
    $menuColor              = "";
    $menuActiveArrowColor   = "";
    $menuArrowColor         = "";
    $menuBgColor            = "style=background:#b7db82";
    $headingColor           = "";
    $headingBgColor         = "";
    $buttonColor            = "";
    $buttonBgColor          = "";
    $menuColorsFromDb       = "";
    $headingColorsFromDb    = "";  
    $buttonStyle            = "";
}
//print_r($suminsuredlist);
//echo '<pre>';
$empFamilyCount = count($empFamilyDetails);
//print_r($companydetails);
if($companydetails[0]['insuranceType']=='floater'){
	$fixedpremiumamount = $companydetails[0]['fixedPremiumAmount'];
} else {
	if($companydetails[0]['policyType']=='fixed'){
	$fixedpremiumamount = $companydetails[0]['fixedPremiumAmount'];
	} else{
	$fixedpremiumamount ='';
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Religare</title>
<!--<link href="css/style.css" rel="stylesheet" type="text/css" />-->
<link rel="stylesheet" href="css/style.css"/>
<link rel="stylesheet" href="css/style_color.css"/>
<script src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/select_menu.js"></script>
<script type="text/javascript" src="admin/js/ui.datepicker.js"></script>
<link href="admin/css/ui.datepicker.css" rel="stylesheet" type="text/css">
</head>

<body>
<div class="top_header">
                <div class="login_container"><img src="<?php echo $companydetails[0]['logoFile'] ? "imagetest/" . $companydetails[0]['logoFile'] : "images/religare_logo.jpg"; ?>" border="0"><a class="logout" href="signout.php" style="display:none">Log Out</a></div>
            </div>
    
<div class="mid_nav">
                <ul class="mainnav" <?= $menuBgColor;?>>
                    <li><a class="active" href="my_policy.php"> <span class="arrow-down" <?= $menuActiveArrowColor;?>>&nbsp;</span> <span class="activetext" <?= $menuColor;?>>My&nbsp;Details</span> </a></li>
                    <!--<li><a href="enquiry.php"> <span class="arrow-down" <?= $menuArrowColor;?>>&nbsp;</span> <span class="activetext" <?= $menuColor;?>>Change Request</span> </a></li>-->
            <?php if (count($companyWellness) > 0)
            {
                ?>
                        <li><a href="wellness.php">Wellness</a></li>
    <?php } ?>
                </ul>
            </div>

            <div class="page_nav"><a href="my_policy_preview.php">Home</a> &raquo; My Details</div> 
    
    
    
    
<?php if(@$_REQUEST['msg']!=''){	?>
<div style="color: green;padding: 10px;text-align: center;"><?php echo sanitize_data(@$_REQUEST['msg']);?></div>
<?php } ?>
<div class="mid_container">
<div class="mid_shadow">
<div class="mid_shadow_head" <?= $headingColorsFromDb;?>>Company details</div>
<div class="mid_content">
  <table width="950" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="476" align="left" valign="top"><table width="400" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="107" height="30" align="left" valign="middle" class="general_txt">Company Name</td>
          <td width="31" height="30" align="center" valign="middle" class="general_txt">:</td>
          <td width="262" height="30" align="left" valign="middle" class="general_txt"><?=$_SESSION['companyName'];?></td>
        </tr>       
      </table></td>
      <td width="474" align="right" valign="top" class="gray_devider"><table width="400" border="0" cellspacing="0" cellpadding="0">        
        <tr>
          <td height="30" align="left" valign="middle" class="general_txt">Employee No.</td>
          <td height="30" align="center" valign="middle" class="general_txt">:</td>
          <td height="30" align="left" valign="middle" class="general_txt"><?=$empDetails[0]['empNo'];?></td>
        </tr>
      </table></td>
    </tr>
  </table>
</div>
<div class="cl"></div></div>
<div class="mid_container">
<div class="mid_shadow">
<div class="mid_shadow_head" <?= $headingColorsFromDb;?>>Personal Details</div>

<div class="mid_content">
  <table width="950" cellspacing="0" cellpadding="0" border="0">
    <tbody><tr>
      <td width="476" valign="top" align="left"><table width="400" cellspacing="0" cellpadding="0" border="0">
        <tbody>
		<tr>
          <td width="107" valign="middle" height="40" align="left" class="general_txt">Salutation</td>
          <td width="31" valign="middle" height="40" align="center" class="general_txt">:</td>
          <td width="262" valign="middle" height="40" align="left" class="general_txt"><?php echo $empDetails[0]['empSalutation'];?>
		</td>
        </tr>		
		<tr>
          <td width="107" valign="middle" height="40" align="left" class="general_txt">First Name</td>
          <td width="31" valign="middle" height="40" align="center" class="general_txt">:</td>
          <td width="262" valign="middle" height="40" align="left" class="general_txt"><?=$empDetails[0]['empFirstName'];?>
        
    	</td>
        </tr>
        <tr>
          <td valign="middle" height="40" align="left" class="general_txt">Last name</td>
          <td valign="middle" height="40" align="center" class="general_txt">:</td>
          <td valign="middle" height="40" align="left" class="general_txt"><?=$empDetails[0]['empLastName'];?>
       
    	</td>
        </tr>
        
        <tr>
          <td valign="middle" height="40" align="left" class="general_txt">Phone</td>
          <td valign="middle" height="40" align="center" class="general_txt">:</td>
          <td valign="middle" height="40" align="left" class="general_txt"><?=$empDetails[0]['empPhone'];?>
       
    	</td>
        </tr>
        		     
      </tbody></table></td>
      <td width="474" valign="top" align="right" class="gray_devider"><table width="400" cellspacing="0" cellpadding="0" border="0">
        <tbody>
		<tr>
          <td width="107" valign="middle" height="40" align="left" class="general_txt">Grade</td>
          <td width="31" valign="middle" height="40" align="center" class="general_txt">:</td>
          <td width="262" valign="middle" height="40" align="left" class="general_txt">	<?php echo getName('tbl_grade','id',$empDetails[0]['empGrade'],'grade');?>
							
			  </td>
        </tr>        
       <tr>
          <td width="107" valign="middle" height="40" align="left" class="general_txt">Gender</td>
          <td width="31" valign="middle" height="40" align="center" class="general_txt">:</td>
          <td width="262" valign="middle" height="40" align="left" class="general_txt">
				<?php if($empDetails[0]['gender']=='M'){ echo 'Male'; } else { echo 'Female'; }?></td>
        </tr>
		 <tr>
          <td width="107" valign="middle" height="40" align="left" class="general_txt">Date of Birth</td>
          <td width="31" valign="middle" height="40" align="center" class="general_txt">:</td>
          <td width="262" valign="middle" height="40" align="left" class="general_txt"><?=date('Y-m-d',$empDetails[0]['dob']);?>
    	</td>
        </tr>
	<tr>
          <td valign="middle" height="40" align="left" class="general_txt">Email ID</td>
          <td valign="middle" height="40" align="center" class="general_txt">:</td>
          <td valign="middle" height="40" align="left" class="general_txt"><?=$empDetails[0]['empEmail'];?>
        
    	</td>
        </tr>
	<!--<tr>
          <td width="107" valign="middle" height="40" align="left" class="general_txt">Nominee Name</td>
          <td width="31" valign="middle" height="40" align="center" class="general_txt">:</td>
          <td width="262" valign="middle" height="40" align="left" class="general_txt"><?=$empDetails[0]['nomineeName'];?>
       
    	</td>
        </tr>
		 <tr>
          <td width="107" valign="middle" height="40" align="left" class="general_txt">Nominee Relationship</td>
          <td width="31" valign="middle" height="40" align="center" class="general_txt">:</td>
          <td width="262" valign="middle" height="40" align="left" class="general_txt"><?=ucfirst($empDetails[0]['relationNomEmp']);?>
    	</td>
        </tr> -->

      </tbody></table></td>
    </tr>
  </tbody></table>
</div>
<div class="cl"></div></div>

<div class="mid_shadow">
<div class="mid_shadow_head" <?= $headingColorsFromDb;?>>Policy details</div>

<div class="mid_content">
  <table width="950" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="276" align="left" valign="top"><table width="400" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="107" height="30" align="left" valign="middle" class="general_txt">Insurance Type</td>
          <td width="31" height="30" align="center" valign="middle" class="general_txt">:</td>
          <td width="262" height="30" align="left" valign="middle" class="general_txt"><?=ucfirst($companydetails[0]['insuranceType']);?></td>
        </tr>       
      </table></td>
      <td width="274" align="right" valign="top" class="gray_devider"><table width="400" border="0" cellspacing="0" cellpadding="0">        
        <tr>
          <td height="30" align="left" valign="middle" class="general_txt" width="150">Policy Type</td>
          <td height="30" align="center" valign="middle" class="general_txt" width="50">:</td>
          <td height="30" align="left" valign="middle" class="general_txt" width="200">
		  <?php if($companydetails[0]['policyType']=='ageLinked') { echo 'Age Linked'; } else { echo 'Fixed'; } ?></td>
        </tr>
      </table></td>
    </tr>
	<?php if($companydetails[0]['offerSumInsured']=='grade'){ ?>
	<tr class="empgraderes">
      <td width="276" align="left" valign="top"><table width="400" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="107" height="30" align="left" valign="middle" class="general_txt">Sum Insured</td>
          <td width="31" height="30" align="center" valign="middle" class="general_txt">:</td>
          <td width="262" height="30" align="left" valign="middle" class="general_txt">
              <span id="empgraderesult"><?php //$gradesiid = chkGradeSI($empDetails[0]['empGrade'],$_SESSION['companyId']);  echo getName('tbl_sum_insured','id',$gradesiid,'sumInsured'); echo @$gradesi; ?>
                  <?php $gradesiid = $empDetails[0]['sumInsuredId']; echo getName('tbl_sum_insured','id',$gradesiid,'sumInsured'); echo @$gradesi;?>
              </span></td>
        </tr>       
      </table></td>	  
      <td width="274" align="right" valign="top" class="gray_devider"><table width="400" border="0" cellspacing="0" cellpadding="0">        
        <tr>
          <td height="30" align="left" valign="middle" class="general_txt" width="150">Premium</td>
          <td height="30" align="center" valign="middle" class="general_txt" width="50">:</td>
          <td height="30" align="left" valign="middle" class="general_txt" width="200">
		  <span id="empgradepremium">
		   <?php echo $empDetails[0]['empPremium'];?></span>
		  
		 	</td>
        </tr>		
      </table></td>
    </tr>
	<?php }	else { ?>
	<tr>
      <td width="276" align="left" valign="top"><table width="400" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="107" height="30" align="left" valign="middle" class="general_txt">Sum Insured</td>
          <td width="31" height="30" align="center" valign="middle" class="general_txt">:</td>
          <td width="262" height="30" align="left" valign="middle" class="general_txt"><?php echo getName('tbl_sum_insured','id',$empDetails[0]['sumInsuredId'],'sumInsured'); ?> 		 
		</td>
        </tr>       
      </table></td>	  
      <td width="274" align="right" valign="top" class="gray_devider"><table width="400" border="0" cellspacing="0" cellpadding="0">        
        <tr>
          <td height="30" align="left" valign="middle" class="general_txt"  width="150">Premium</td>
          <td height="30" align="center" valign="middle" class="general_txt"  width="50">:</td>
          <td height="30" align="left" valign="middle" class="general_txt"  width="200">
		  <?php echo $empDetails[0]['empPremium'];?>		 
		 </td>
        </tr>		
      </table></td>
    </tr>
	<?php } if($companydetails[0]['offerTopUp']=='yes'){ ?>	
	<tr>
      <td width="276" align="left" valign="top"><table width="400" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="112" height="30" align="left" valign="middle" class="general_txt">Topup Sum Insured</td>
          <td width="28" height="30" align="center" valign="middle" class="general_txt">:</td>
          <td width="267" height="30" align="left" valign="middle" class="general_txt">
		 <?php echo getName('tbl_topup_sum_insured','id',$empDetails[0]['empTopupSIId'],'sumInsured'); ?></td>
        </tr>       
      </table></td>	  
      <td width="274" align="right" valign="top" class="gray_devider"><table width="400" border="0" cellspacing="0" cellpadding="0">        
        <tr>
          <td height="30" align="left" valign="middle" class="general_txt" width="150">Topup Premium</td>
          <td height="30" align="center" valign="middle" class="general_txt" width="50">:</td>
          <td height="30" align="left" valign="middle" class="general_txt" width="200"><?php echo $empDetails[0]['empTopupPremium'];?></td>
        </tr>		
      </table></td>
    </tr>
	<?php }	?> 
  </table>
</div>
<div class="cl"></div></div>
</div>

<div class="mid_shadow">
<div class="mid_shadow_head" <?= $headingColorsFromDb;?>>Family Details</div>
<?php  if($companydetails[0]['dependent']=='allowed'){ ?>
<div class="sub_container">
  <table width="965" border="0" cellspacing="1" cellpadding="0">
    <tr>      
      <td width="95" class="green_txt_center gray_devider1" style="line-height:32px; font-weight: bold;">Relation</td>
	  <td width="95" class="green_txt_left gray_devider1" style="line-height:32px; font-weight: bold;">Title</td>
      <td width="95" class="green_txt_center gray_devider1" style="line-height:32px; font-weight: bold;">First Name</td>
      <td width="95" class="green_txt_center gray_devider1" style="line-height:32px; font-weight: bold;">Last Name</td>
      <td width="95" class="green_txt_center" style="line-height:32px; font-weight: bold;">Date Of Birth</td>
	  <?php 
	  		 if($companydetails[0]['insuranceType']!='floater'){ 
	  	?>
	  <td width="95" class="green_txt_center" style="line-height:32px;  font-weight: bold;">Base SI</td>
     <td width="95" class="green_txt_center" style="line-height:12px; font-weight: bold;">Base SI Premium</td>
	  <?php 	
	  if($companydetails[0]['offerTopUp']=='yes'){
	  ?>
	  <td width="95" class="green_txt_center" style="line-height:12px; font-weight: bold;">Top-up SI</td>
	  <td width="95" class="green_txt_center" style="line-height:12px; font-weight: bold;">Top-up SI Premium</td>
	  <?php } }	?>
	 <!-- <td width="95" class="green_txt_center" style="line-height:12px;">Action</td>-->
    </tr>
	<?php for($z=1;$z<=10;$z++){ ?>
    <tr class="res<?=$z;?>" <?php if(($z>1)&&($z>$empFamilyCount)){ ?>style="display:none;" <?php }	?>>
      <td class="gray_devider1 general_txt_left" style="padding-left:5px;"> <?php echo @$empFamilyDetails[$z-1]['relation']; ?>
	  	</td>
      <td class="gray_devider1 general_txt_left" style="padding-left:5px;">
	  <?php echo @$empFamilyDetails[$z-1]['salutation']; ?>		
		</td>
      <td class="gray_devider1 general_txt_center">
	  	<?=@$empFamilyDetails[$z-1]['firstName'];?>
       </td>
      <td class="gray_devider1 general_txt_center">
	  <?=@$empFamilyDetails[$z-1]['lastName'];?>
        </td>
      <td class="gray_devider1 general_txt_center">
	  <?=date('d-M-Y',@$empFamilyDetails[$z-1]['fdob']);?>
        
    	</td>
		 <?php
	  		 if($companydetails[0]['insuranceType']!='floater'){ 
			 
	  	?>
		 <td class="gray_devider1 general_txt_left" style="padding-left:5px;">		 
		 <input class="input" style="width:67px;" class="basesipremium" disabled="disabled" value=" <?php echo getName('tbl_sum_insured','id',@$empFamilyDetails[$z-1]['sumInsuredId'],'sumInsured'); ?>">
		</td>
		 <td class="gray_devider1 general_txt_center">
		 <input type="text" class="input" style="width:67px;" class="basesipremium" disabled="disabled" value="<?php echo @$empFamilyDetails[$z-1]['sumInsuredPremium']; ?>">       
    	</td>
		<?php 
	  		if($companydetails[0]['offerTopUp']=='yes'){
	  	?>
		 <td class="gray_devider1 general_txt_left" style="padding-left:5px;">
	  	
	  	<input type="text" class="input" style="width:67px;" class="basesipremium" disabled="disabled" value="<?php echo getName('tbl_topup_sum_insured','id',@$empFamilyDetails[$z-1]['topSumInsuredId'],'sumInsured'); ?>">
		
		</td>		
		 <td class="gray_devider1 general_txt_center">
        <input type="text" class="input" style="width:67px;"  disabled="disabled" value="<?=@$empFamilyDetails[$z-1]['topSumInsuredPremium'];?>">
        <inpuetails
    	</td>
		<?php } 	}	?>
	 <!-- <td class="gray_devider1 general_txt_center">
	  <input type="hidden" name="empFamilyId<?=$z?>" id="empFamilyId<?=$z?>" value="<?=@$empFamilyDetails[$z-1]['id'];?>" />
	  <span id="htmlreslut<?=$z?>">
	  <?php if($z>$empFamilyCount){ ?>
	  <b onclick="getFamilyHtml('<?=$z?>')" style="cursor:pointer;"><img src="images/iphone-plus-sign-icon.jpg" /></b>
	  <?php } else { ?>
	  <b onclick="removehtml('<?=$z?>','<?=@$empFamilyDetails[$z-1]['id'];?>')" style="cursor:pointer;"><img src="images/minus_white.png" /></b>
	  <?php } ?>
	  </span></td>-->
    </tr>
	<!--<script type="text/javascript">getSIPremium('<?=$_SESSION['companyId'];?>','<?=$z?>');</script>-->
	<?php }	?>
  </table>
</div>
<?php }	 else { echo '<div style="padding:10px;">Dependent are not Allowed</div>'; } 
$m = date('m', time());
$Y = date('Y', time());
$d = date('d', time());
$startDay = mktime(0, 0, 0, $m, $d, $Y);
?>
<div>
<?php 
if($_SESSION['enrolEndDate'] > $startDay)
{
?>
<form action="my_policy.php" method="post"><input type="submit" id="myPolicy" value="Update" class="submit_button fr" <?= $buttonStyle;?> /></form>
    <!--<a href="my_policy.php"><input type="button" value="Update" class="submit_button fr" < ?= $buttonStyle;?>/></a>-->
<?php } else { ?>
<form action="my_policy_add.php" method="post"><input type="submit" id="myPolicyAdd" value="Add" class="submit_button fr" <?= $buttonStyle;?> /></form>
<!--<a href="my_policy_add.php"><input type="button" value="Add" class="submit_button fr" < ?= $buttonStyle;?> /></a>-->
<?php } ?>
</div>
<div class="cl"></div>
</div>
<div class="footer_container">
                                                                            <div class="disclaimer_nav"> Insurance is the subject matter of solicitation | IRDA Registration No. 148<br>
                                                                                    Copyrights 2012, All right reserved by Religare Health Insurance Company Ltd.<br>
                                                                                        <span>This site is best viewed on Internet Explorer 7/8 and Fire Fox 3.x</span> </div>
                                                                                        <div class="cl"></div>
                                                                                        </div>
<?php if($companydetails[0]['offerSumInsured']=='age') { ?>
<script type="text/javascript">
//getEmpSIPremium();
</script> 
<?php } if($companydetails[0]['offerSumInsured']=='grade') {  ?>  
<script type="text/javascript">
//getEmpGradePremium('<?=$_SESSION['companyId'];?>');
</script> 
<?php } ?>
</body>
</html>
