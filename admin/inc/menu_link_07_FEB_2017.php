<div id="header">
  <div class="logo"></div>
  <!--logo-->
  <div class="header-right">
    <div class="top-link-bg">
      <div class="left"><img src="<?=_WWWROOT;?>/images/top-link-left-img.jpg" alt="" /></div>
      <!--left-->
      <div class="top-txt">Logged in as <?php echo sanitize_data($_SESSION["username"]);?> ( <span><a href="<?=_WWWROOT;?>/logout.php">Logout</a></span> )</div>
      <!--top txt-->
      <div class="top-links">
        <ul>
          <li><a href="<?=_WWWROOT;?>/account.php">My Account</a></li>
          <li><a href="<?=_WWWROOT;?>/change_password.php">Change Password</a></li>
        </ul>
      </div>
      <!--top links-->
      <div style="clear:both;"></div>
    </div>
    <!--top link bg-->
    <!--<div class="select-theme">
      <div class="left">Select Colour Theme :</div>
      <div class="right">
        <ul class="theme">
          <li><a href="<?php echo $_SERVER["PHP_SELF"]?>?colorname=brown" class="brown"></a></li>
          <li><a href="<?php echo $_SERVER["PHP_SELF"]?>?colorname=green" class="green"></a></li>
          <li><a href="<?php echo $_SERVER["PHP_SELF"]?>?colorname=blue" class="blue"></a></li>
          <li><a href="<?php echo $_SERVER["PHP_SELF"]?>?colorname=pink" class="pink"></a></li>
        </ul>
      </div>
      <!--right-->
    <!--</div>-->
    <!--select theme-->
  </div>
  <!--header right-->
  <div style="clear:both;"></div>
  <div style="clear:both;"></div>
</div>
<div id="naviagtion">
  <div id="ddtopmenubar" class="mattblackmenu">
    <ul>
      <li><a href="<?=_WWWROOT;?>/dashboard.php">&nbsp;Dashboard&nbsp;</a></li>
      <li><a href="#" rel="ddsubmenu1">&nbsp;Masters&nbsp;</a>
			<ul id="ddsubmenu1" class="ddsubmenustyle">
              <li><a href="<?=_WWWROOT;?>/cms/ageGroupList.php">Age Group</a></li>		  
			  <li><a href="<?=_WWWROOT;?>/cms/sumInsuredList.php">Sum Insured</a></li>
			  <li><a href="<?=_WWWROOT;?>/cms/topupSumInsuredList.php">Top-up Sum Insured</a></li>
			</ul>
		</li>
	 <li><a href="#" rel="ddsubmenu1">&nbsp;Companies&nbsp;</a>
			<ul id="ddsubmenu1" class="ddsubmenustyle">
			  <li><a href="<?=_WWWROOT;?>/cms/corporateList.php">List Companies</a></li>			  
			  <li><a href="<?=_WWWROOT;?>/cms/addCorporate.php">Add Company</a></li>			 
			</ul>
		</li>	
                <li><a href="<?=_WWWROOT;?>/cms/mailStatus.php">&nbsp;Employee Upload / Mail Log&nbsp;</a></li>
	 
    </ul>
  </div>
  <script type="text/javascript">
						ddlevelsmenu.setup("ddtopmenubar", "topbar") //ddlevelsmenu.setup("mainmenuid", "topbar|sidebar")
						</script>
    <div style="clear:both;"></div>
</div>