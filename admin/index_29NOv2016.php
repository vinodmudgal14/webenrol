<?php 
ini_set("session.cookie_secure", 0);
//session_start();
//
//$_SESSION["Flag"]		=	0;
//$_SESSION["id"]			=	"";
//$_SESSION["username"]	=	"";
//$_SESSION["firstName"]	=	"";
//$_SESSION["PHPSESSID"]	=	"";
//$_SESSION["color"]		=	"";
include("conf/conf.php");
include("conf/fucts.php");
$msg='';
if($_SERVER["REQUEST_METHOD"] == "POST")
{
$usernamelen	=	strlen($_POST['username']);
$passwordlen	=	strlen(base64_decode($_POST['password']));

$username	=	sanitize_data($_POST['username']);
$password	=	sanitize_data_password(base64_decode($_POST['password']));

$uafterlen	=	strlen($username);
$pafterlen	=	strlen($password);

if(($usernamelen!=$uafterlen)||($passwordlen!=$pafterlen)) {
	header("location: index.php?msg=Please enter valid characters in login details.");
	exit;
}
$sql 		= 	sprintf("SELECT * FROM `tbl_user` WHERE userName='%s' AND password='%s'  AND `status`='Activate' LIMIT 1",
				mysql_real_escape_string(@$username),
				mysql_real_escape_string(@$password));
$result=mysql_query($sql);
$row=mysql_fetch_array($result);
$count=mysql_num_rows($result);
if($count==1){
session_start();
$_SESSION["PHPSESSID"] = session_id();
$_SESSION['id']=$row['id'];
$_SESSION["username"]=$row['userName'];
$_SESSION["firstName"]=$row['firstName'];
$_SESSION['color']='green';
header("location: dashboard.php");
exit;
}	else	{
	header("location: index.php?msg=Your Login Name or Password is invalid.");
	exit;
	}
}
if(@$_REQUEST['msg']!='') {
	$msg	=	sanitize_data(@$_REQUEST['msg']);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>:: Admin Panel ::</title>
<?php
$styleSheet  = (isset($_POST['colorname']))		?  $_POST['colorname']  :   'green' ;
//$styleSheet  = 'green';
//echo $styleSheet;
if ($styleSheet == 'brown'){
	$cssFileNamer  =  'css/brown.css';
}
else if ($styleSheet == 'green'){
	$cssFileNamer  =  'css/green.css';
}
else if ($styleSheet == 'blue'){
	$cssFileNamer  =  'css/blue.css';
}
else if ($styleSheet == 'pink'){
	$cssFileNamer  =  'css/pink.css';
}else{
	$cssFileNamer  =  'css/brown.css';
	}
?>
<link href="<?php echo $cssFileNamer;?>" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script language="javascript" src="js/encrypt.js"></script>
<script type="text/javascript" src="js/ddlevelsmenu.js"></script>
<script language="javaScript">
function  validate()
{
	var username=document.loginForm.username.value;
	if (username == "")
	{
		alert("\nPleaser enter Username.")
		document.loginForm.username.focus();
		return false;
	}
	var password = document.loginForm.password.value;
	if ((password == "")||(password.length < 4))
	{
		alert("\nThe PASSWORD field is either empty or less than 4 chars.\n\nPlease re-enter your Password.")
		document.loginForm.password.focus();
		return false;
	}
	 var encoded_val=base64_encode(password);
	 $('#password').val(encoded_val);
	return true;
}
</script>
</head>
<body <?php echo $cssFileNamer;?>>
	<div id="main">
    	<div id="header">
        	<div class="logo"></div> <!--logo-->
            	<div class="header-right">
                	
                    <div class="top-link-bg" style="background:none;">&nbsp;</div> <!--top link bg-->
                    	<!--<div class="select-theme">
                        	<div class="left">Select Colour Theme :</div>
                            	<div class="right">
                                	<ul class="theme">
                                    	<li><a href="<?php echo $_SERVER["PHP_SELF"]?>?colorname=brown" class="brown"></a></li>
                                        <li><a href="<?php echo $_SERVER["PHP_SELF"]?>?colorname=green" class="green"></a></li>
                                        <li><a href="<?php echo $_SERVER["PHP_SELF"]?>?colorname=blue" class="blue"></a></li>
                                        <li><a href="<?php echo $_SERVER["PHP_SELF"]?>?colorname=pink" class="pink"></a></li>
                                    </ul>
                                </div> <!--right-->
                        </div> <!--select theme-->
                </div> <!--header right-->
                <div style="clear:both;"></div>
                
        <div style="clear:both;"></div>
        </div> <!--header-->
   	<div id="login-area" align="center">
       <form name="loginForm" id="loginForm" action="" method="post" onSubmit="return validate()">
           <table align="center" width="402" cellpadding="0" cellspacing="0" border="0">
            <tr>
              <td height="20"></td>
            </tr>
            <tr>
              <td align="left" class="login-heading">Login Area</td>
            </tr>
			<?php if($msg!='') {	?>
			 <tr>
              <td align="left" ><span style="color:#FF0000; margin-left:30px; padding-top:20px; font-size:14px;"><?php echo sanitize_data(@$msg);?></span></td>
            </tr>
			<?php }	?>
            <tr>
              <td align="left" class="login-text">Enter your login details</td>
            </tr>
            <tr>
               <td align="left" class="field-text">Your Login :</td>
            </tr>
            <tr>
              <td align="left" style="padding:8px 0px 0px 18px;"><input name="username" id="username" type="text" class="login-home-textfield" AUTOCOMPLETE="OFF"/></td>
            </tr>
            <tr>
               <td align="left" class="field-text">Your Password :</td>
            </tr>
            <tr>
              <td align="left" style="padding:8px 0px 0px 18px;"><input name="password" id="password" type="password" class="login-home-textfield" AUTOCOMPLETE="OFF"/></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td style="padding-left:18px;"><input name="submit" type="submit" class="login-btn" value="LOGIN" /></td>
            </tr>
            </table>
           </form>
      </div> <!--login area-->
      	<?php include('inc/ft.php'); ?>

</body>
</html>
