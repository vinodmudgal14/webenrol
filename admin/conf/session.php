<?php
    	//handles session on server
    	ini_set("session.cookie_secure", 0);

	session_start();
    	session_name();
	if ((trim($_SESSION["username"])==""))
	{
		session_unset();
		session_destroy();
		$url="Location:  ../index.php";
		header($url);
		exit;
	}	
?>