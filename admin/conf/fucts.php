﻿<?php
function sanitize_data($input_data) {
	$searchArr=array("document","write","alert","%","$",";","+","#","<",">",")","(","'","\'","<img","src=",".ini","<iframe","java:","window.open","http","!",":boot",".exe",".com",".php",".js",".txt",".css","@","having","sleep0");
	$input_data= str_replace("script","",$input_data);
	$input_data= str_replace("iframe","",$input_data);
	$input_data= str_replace($searchArr,"",$input_data);
	$input_data=trim($input_data);
	return htmlentities(stripslashes($input_data), ENT_QUOTES);
}
function sanitize_data_password($input_data) {
	$searchArr=array("document","write","alert","%","$",";","+","<",">",")","(","'","\'","<img","src=",".ini","<iframe","java:","window.open","http",":boot",".exe",".com",".php",".js",".txt",".css","having","sleep0");
	$input_data= str_replace("script","",$input_data);
	$input_data= str_replace("iframe","",$input_data);
	$input_data= str_replace($searchArr,"",$input_data);
	$input_data=trim($input_data);
	return htmlentities(stripslashes($input_data), ENT_QUOTES);
}
function sanitize_data_email($input_data) {
	$searchArr=array("document","write","alert","%","$",";","+","#","<",">",")","(","'","\'","<img","src=",".ini","<iframe","java:","window.open","http","!",":boot",".exe",".php",".js",".txt",".css","having","sleep0");
	$input_data= str_replace("script","",$input_data);
	$input_data= str_replace("iframe","",$input_data);
	$input_data= str_replace($searchArr,"",$input_data);
	$input_data=trim($input_data);
	return htmlentities(stripslashes($input_data), ENT_QUOTES);
}
function isNumeric($numeric) {
	if(trim($numeric)=='') {	$numeric = '0';	}
	$var = preg_match("/^[0-9]+$/", trim($numeric));
	if($var==1){ return $numeric; } else {
		echo "<script>window.top.location='../index.php?colorname=green&msg=Invalid request'</script>";
		//header('Location: index.php?colorname=green&message=Invalid request');		
	}
}
function isVarChar($varchar) {
	if(trim($varchar)=='') {	$varchar = '0';	}
	$var = preg_match('/^[a-zA-Z]+[a-zA-Z0-9_]+$/',trim($varchar));	
	if($var==1){	return $varchar; 	} else {
		echo "<script>window.top.location='../index.php?colorname=green&msg=Invalid request'</script>";
		//header('Location: index.php?colorname=green&message=Invalid request');		
	}
}
function getName($tblName, $colname, $id ,$getCol){
	$sql ="select * from `$tblName`	 where `$colname` ='$id' ";
	$res=mysql_query($sql);
	$result = mysql_fetch_assoc($res);
	return $result[$getCol];
}
function PageList($table){
 $dataArray=array();
 $sql="select * from ".$table;
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
function locationList(){
 $dataArray=array();
 $sql="select * from tbl_location where `isDeleted` = 'no' ";
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
function industryList(){
 $dataArray=array();
 $sql="select * from tbl_industry where `isDeleted` = 'no' ";
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
function claimRatioList(){
 $dataArray=array();
 $sql="select * from tbl_claim_ratio where `isDeleted` = 'no' ";
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
function familyStructureList(){
 $dataArray=array();
 $sql="select * from tbl_family_structure where `isDeleted` = 'no' ";
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
function ageGroupList(){
 $dataArray=array();
 $sql="select * from tbl_age_group where `isDeleted` = 'no' ";
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
function serviceTaxList(){
 $dataArray=array();
 $sql="select * from tbl_service_tax where `isDeleted` = 'no' ";
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
function corporatesList(){
 $dataArray=array();
  $currentDate = strtotime(date("Y-m-d H:i:s"));
 //$sql="select * from tbl_company where `isDeleted` = 'no' ";
 $sql="select * from tbl_company where policyEndDate > '".$currentDate."' and `isDeleted` = 'no' ";
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
function companydetails($companyId){
 $dataArray=array();
 $sql="select * from tbl_company where `isDeleted` = 'no' AND `id` = '$companyId' ";
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
function companyWellnessDetails($companyId){
 $dataArray=array();
 $sql="select * from `tbl_company_wellness` where `companyId` = '$companyId' ";
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
function chkCompanyWellness($companyId,$date){
 $dataArray=array();
 $sql="select * from `tbl_company_wellness` where `companyId` = '$companyId' AND `startDate` <= '$date' AND `endDate` >= '$date' ";
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
function chkcompanyurl($url){
 $dataArray=array();
 $sql="select * from `tbl_company` where `isDeleted` = 'no' AND `url` = '$url' AND `urlStatus` != 'deactive' ";
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
function companyEmpList($companyId){
 $dataArray=array();
 $sql="select * from `tbl_company_employee` where `isDeleted` = 'no' AND `companyId` = '$companyId' ";
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
function companyEmpDetails($companyId,$empId){
 $dataArray=array();
 $sql="select * from `tbl_company_employee` where `isDeleted` = 'no' AND `companyId` = '$companyId' AND `id` = '$empId' ";
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
function chkGradeSI($gradeId,$companyId){
 $sql="select `sumInsuredId` from `tbl_grade_si_mapping` where `gradeId` = '$gradeId' AND `companyId` = '$companyId' AND `isDeleted` = 'no' ";
 $result = mysql_query($sql);
 $row=mysql_fetch_assoc($result);
 $sumInsuredId = $row['sumInsuredId'];
 return $sumInsuredId;
}
function checkedGradeSI($gradeId,$companyId){
 $sql="select `sumInsuredId` from `tbl_grade_si_mapping` where `gradeId` = '$gradeId' AND `companyId` = '$companyId' AND `isDeleted` = 'no' ";
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}

function chkAgeSI($companyId,$sumInsuredId){
$sql="select `sumInsuredId` from `tbl_age_si` where `companyId` = '$companyId' AND `sumInsuredId` = '$sumInsuredId' AND `isDeleted` = 'no' ";
 $result = mysql_query($sql);
 $row=mysql_fetch_assoc($result);
 $sumInsuredId = $row['sumInsuredId'];
 return $sumInsuredId;
}
function chkTopUp($companyId,$sumInsuredId,$gradeId){
 $sql="select `sumInsuredId` from `tbl_topup_si` where `companyId` = '$companyId' AND `gradeId` = '$gradeId' AND `sumInsuredId` = '$sumInsuredId' AND `isDeleted` = 'no' ";
 $result = mysql_query($sql);
 $row=mysql_fetch_assoc($result);
 $sumInsuredId = $row['sumInsuredId'];
 return $sumInsuredId;
}
//Added on 14 Sept 2015
function chkTopUpList($companyId,$gradeId){
$sql = "SELECT ts.sumInsuredId as id, tsi.sumInsured FROM `tbl_topup_si` AS ts LEFT JOIN tbl_topup_sum_insured AS tsi ON ts.sumInsuredId = tsi.id 
WHERE ts.companyId = '$companyId' AND ts.gradeId = '$gradeId' AND ts.isDeleted = 'no' AND tsi.isDeleted = 'no' order by tsi.sumInsured";
$dataArray=mysqltoarray($sql);

 return $dataArray;
// $result = mysql_query($sql);
// $row=mysql_fetch_assoc($result);
// $sumInsuredId = $row['sumInsuredId'];
// return $sumInsuredId;
}
function chkMembers($companyId, $memberId ,$gender){
$sql="select `id` from `tbl_company_family_map` where `companyId` = '$companyId' AND `gender` = '$gender' AND `memberId` = '$memberId' AND `isDeleted` = 'no' ";
 $result = mysql_query($sql);
 $row=mysql_fetch_assoc($result);
 $sumInsuredId = $row['id'];
 return $sumInsuredId;
}
function sumInsuredList(){
 $dataArray=array();
 $sql="select * from tbl_sum_insured where `isDeleted` = 'no' ";
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
function sumInsuredMapList($companyId){
 $dataArray=array();
 $sql="select a.sumInsuredId as id,b.sumInsured from `tbl_age_si` as a,`tbl_sum_insured` as b where a.`sumInsuredId` = b.`id` AND a.`companyId` = '$companyId' AND b.`isDeleted` = 'no' AND a.`isDeleted` = 'no' GROUP BY a.`sumInsuredId` ";
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
function gradeSumInsuredMapList_10Sept2015($companyId){
 $dataArray=array();
 $sql="select sumInsuredId,siAmount from `tbl_grade_si_mapping` where `isDeleted` = 'no' AND `companyId` = '$companyId' AND `isDeleted` = 'no' GROUP BY `sumInsuredId` ";
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
function gradeSumInsuredMapList($companyId, $gradeId=null){
 $dataArray=array();
 if($gradeId)
 {
    $sql="select a.sumInsuredId as id,b.sumInsured,a.gradePremium from `tbl_grade_si_mapping` as a,`tbl_sum_insured` as b where a.`sumInsuredId` = b.`id` AND a.`companyId` = '$companyId' AND a.`gradeId`='$gradeId' AND b.`isDeleted` = 'no' AND a.`isDeleted` = 'no' GROUP BY b.`sumInsured` order by a.siAmount asc";
 }
 else
 {
     $sql="select sumInsuredId,siAmount,gradeId from `tbl_grade_si_mapping` where `companyId` = '$companyId' AND `isDeleted` = 'no' GROUP BY `sumInsuredId` ";
 }
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
//added on 05 nov 2015. to show age based si on my_policy.
function ageSumInsuredMapList($companyId, $empAgeGroupId){
    $dataArray=array();
 //$sql="SELECT gsp.id as id , si.sumInsured as sumInsured ,gsp.amount as gradePremium FROM tbl_grade_si_premium AS gsp JOIN tbl_sum_insured AS si WHERE gsp.companyId = '$companyId' AND  gsp.sumInsuredId = si.id GROUP BY gsp.sumInsuredId ";
  $sql = "SELECT gsp.sumInsuredId as id, gsp.ageGroupId, si.sumInsured,gsp.amount as gradePremium FROM tbl_grade_si_premium AS gsp, tbl_sum_insured AS si
        WHERE gsp.sumInsuredId = si.id AND gsp.ageGroupId = '$empAgeGroupId' AND gsp.companyId = '$companyId'";
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}


function ageSumInsuredMapList_05nov2015($companyId, $empAgeGroupId){
    $dataArray=array();
 //$sql="SELECT gsp.id as id , si.sumInsured as sumInsured ,gsp.amount as gradePremium FROM tbl_grade_si_premium AS gsp JOIN tbl_sum_insured AS si WHERE gsp.companyId = '$companyId' AND  gsp.sumInsuredId = si.id GROUP BY gsp.sumInsuredId ";
  echo $sql = "SELECT gsp.sumInsuredId as id, si.sumInsured,gsp.amount as gradePremium FROM tbl_grade_si_premium AS gsp, tbl_sum_insured AS si WHERE gsp.sumInsuredId = si.id AND gsp.ageGroupId = '$empAgeGroupId' AND gsp.companyId = '$companyId'";
exit;
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}



function topUpSumInsuredMapList($companyId){
 $dataArray=array();
 $sql="select a.sumInsuredId as id,b.sumInsured from `tbl_topup_si` as a,`tbl_topup_sum_insured` as b where a.`sumInsuredId` = b.`id` AND a.`companyId` = '$companyId' AND b.`isDeleted` = 'no' AND a.`isDeleted` = 'no' GROUP BY a.`sumInsuredId` ";
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
function topUpSIMappedList17Sept2015($companyId,$gradeId){
 $dataArray=array();
 $sql="select a.sumInsuredId as id,b.sumInsured from `tbl_topup_si` as a,`tbl_topup_sum_insured` as b where a.`sumInsuredId` = b.`id` AND a.`companyId` = '$companyId' AND b.`isDeleted` = 'no' AND a.`isDeleted` = 'no' AND a.`gradeId` = '$gradeId' GROUP BY a.`sumInsuredId` ";
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
function topUpSIMappedList($companyId,$gradeId){
 $dataArray=array();
 $sql="select a.sumInsuredId as id,b.sumInsured from `tbl_topup_si` as a,`tbl_topup_sum_insured` as b where a.`sumInsuredId` = b.`id` AND a.`companyId` = '$companyId' AND b.`isDeleted` = 'no' AND a.`isDeleted` = 'no' AND a.`gradeId` = '$gradeId' GROUP BY a.`sumInsuredId` order by b.sumInsured";
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
function topupSumInsuredList(){
 $dataArray=array();
 $sql="select * from tbl_topup_sum_insured where `isDeleted` = 'no' ";
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
function summaryInsuredList($quoteId){
 $dataArray=array();
 $sql="select a.* from tbl_sum_insured as a, tbl_quote_sum_insured as b where a.`isDeleted` = 'no' AND b.quoteId = '$quoteId' AND a.id = b.sumInsuredId GROUP BY b.sumInsuredId";
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
function getSAMappingData($ageGroupId,$sumInsuredId,$companyId){
 $sql="select `amount` from `tbl_si_premium` where `ageGroupId` = '$ageGroupId' AND `sumInsuredId` = '$sumInsuredId' AND `companyId` = '$companyId' ";
 $result = mysql_query($sql);
 $row=mysql_fetch_assoc($result);
 $amount = $row['amount'];
 return $amount;
}
function getGradeSAMappingData($ageGroupId,$sumInsuredId,$companyId){
 $sql="select `amount` from `tbl_grade_si_premium` where `ageGroupId` = '$ageGroupId' AND `sumInsuredId` = '$sumInsuredId' AND `companyId` = '$companyId' ";
 $result = mysql_query($sql);
 $row=mysql_fetch_assoc($result);
 $amount = $row['amount'];
 return $amount;
}
function getGradeSIMapData_11Sept2015($gradeId,$sumInsuredId,$companyId){
 $sql="select `premium` from `tbl_grade_si` where `gradeId` = '$gradeId' AND `sumInsuredId` = '$sumInsuredId' AND `companyId` = '$companyId' AND `isDeleted` = 'no' ";
 $result = mysql_query($sql);
 $row=mysql_fetch_assoc($result);
 $premium = $row['premium'];
 return $premium;
}

function getGradeSIMapData($gradeId,$sumInsuredId,$companyId){
 $sql="select `gradepremium` from `tbl_grade_si_mapping` where `gradeId` = '$gradeId' AND `sumInsuredId` = '$sumInsuredId' AND `companyId` = '$companyId' AND `isDeleted` = 'no' ";
 $result = mysql_query($sql);
 $row=mysql_fetch_assoc($result);
 $premium = $row['gradepremium'];
 return $premium;
}
function getGradeTopupSIMapData($gradeId,$sumInsuredId,$companyId){
 $sql="select `amount` from `tbl_topupsi_grade_premium` where `gradeId` = '$gradeId' AND `sumInsuredId` = '$sumInsuredId' AND `companyId` = '$companyId'";
 $result = mysql_query($sql);
 $row=mysql_fetch_assoc($result);
 $premium = $row['amount'];
 return $premium;
}
function getAgeSIMapData($sumInsuredId,$companyId){
 $sql="select `premium` from `tbl_age_si_premium` where `sumInsuredId` = '$sumInsuredId' AND `companyId` = '$companyId' ";
 $result = mysql_query($sql);
 $row=mysql_fetch_assoc($result);
 $premium = $row['premium'];
 return $premium;
}
function getTopUpSAMappingData($ageGroupId,$sumInsuredId,$companyId){
 $sql="select `amount` from `tbl_topupsi_premium` where `ageGroupId` = '$ageGroupId' AND `sumInsuredId` = '$sumInsuredId' AND `companyId` = '$companyId' ";
 $result = mysql_query($sql);
 $row=mysql_fetch_assoc($result);
 $amount = $row['amount'];
 return $amount;
}
function getQuoteMappingData($quoteId,$ageGroupId,$sumInsuredId){
 $sql="select SUM(memberNo) as totalMember from `tbl_quote_sum_insured` where `ageGroupId` = '$ageGroupId' AND `sumInsuredId` = '$sumInsuredId' AND `quoteId` = '$quoteId' GROUP BY `quoteId` ";
 $result = mysql_query($sql);
 $row=mysql_fetch_assoc($result);
 $totalMember = $row['totalMember'];
 return $totalMember;
}
function getGroupTotal($quoteId,$ageGroupId){
 $sql="select SUM(memberNo) as totalMember from `tbl_quote_sum_insured` where `ageGroupId` = '$ageGroupId' AND `quoteId` = '$quoteId' GROUP BY `quoteId` ";
 $result = mysql_query($sql);
 $row=mysql_fetch_assoc($result);
 $totalMember = $row['totalMember'];
 return $totalMember;
}
function getSumInsuredTotal($quoteId,$sumInsuredId){
 $sql="select SUM(memberNo) as totalMember from `tbl_quote_sum_insured` where `sumInsuredId` = '$sumInsuredId' AND `quoteId` = '$quoteId' GROUP BY `quoteId` ";
 $result = mysql_query($sql);
 $row=mysql_fetch_assoc($result);
 $totalMember = $row['totalMember'];
 return $totalMember;
}
function quoteReport($sql_filter){
 $dataArray=array();
 $sql="select *  from `tbl_quote` WHERE `companyId` > 0 ".$sql_filter." ORDER BY `id` DESC ";
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
function getSumInsuredPremium($companyId,$sumInsuredId,$empAgeGroupId){
 $sql="select `id`,`amount` from `tbl_si_premium` where `companyId` = '$companyId' AND `sumInsuredId` = '$sumInsuredId' AND `ageGroupId` = '$empAgeGroupId' ";
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
//added on 05 nov 2015. to fetch age based si in my_policy.
function getAgeSumInsuredPremium($companyId,$sumInsuredId,$empAgeGroupId){
 $sql="select `id`,`amount` from `tbl_grade_si_premium` where `companyId` = '$companyId' AND `sumInsuredId` = '$sumInsuredId' AND `ageGroupId` = '$empAgeGroupId' ";
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
function getFixedSumInsuredPremium($companyId,$sumInsuredId){
 $sql="select `id`,`premium` from `tbl_age_si_premium` where `companyId` = '$companyId' AND `sumInsuredId` = '$sumInsuredId' ";
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
function getEmpGradePremium($companyId,$gradeId){
 $sql="select * from `tbl_grade_si_mapping` where `companyId` = '$companyId' AND `gradeId` = '$gradeId' AND `isDeleted` = 'no'  ";
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
function getTopupSumInsuredPremium($companyId,$sumInsuredId,$empAgeGroupId){
 $sql="select `id`,`amount` from `tbl_topupsi_premium` where `companyId` = '$companyId' AND `sumInsuredId` = '$sumInsuredId' AND `ageGroupId` = '$empAgeGroupId' ";
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
function getEmpFamily($companyId,$empId){
 $sql="select * from `tbl_employee_family` where `companyId` = '$companyId' AND `empId` = '$empId' AND `isDeleted` = 'no' ";
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
function getEmpFamilyList($companyId,$gender){
 $dataArray=array();
 $sql="select memberId from `tbl_company_family_map` where `isDeleted` = 'no' AND `companyId` = '$companyId' AND `gender` = '$gender' ";
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
function getAllMembersList(){
 $dataArray=array();
 $sql="select id, member from `tbl_family_member` where `isDeleted` = 'no'";
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}

function quoteSummary($quoteId){
 $sql="select * from `tbl_quote` where `id` = '$quoteId' LIMIT 1";
 $result = mysql_query($sql);
 $row=mysql_fetch_assoc($result);
 return $row;
}
function chkGradeSIMapData($gradeId,$sumInsuredId,$companyId){
 $sql="select `id` from `tbl_grade_si_mapping` where `gradeId` = '$gradeId' AND `sumInsuredId` = '$sumInsuredId' AND `companyId` = '$companyId' AND `isDeleted` = 'no' ";
 $result = mysql_query($sql);
 $row=mysql_fetch_assoc($result);
 $id = $row['id'];
 return $id;
}
function chkGradeTopupSIMapData($gradeId,$sumInsuredId,$companyId){
 $sql="select `id` from `tbl_topup_si` where `gradeId` = '$gradeId' AND `sumInsuredId` = '$sumInsuredId' AND `companyId` = '$companyId' AND `isDeleted` = 'no' ";
 $result = mysql_query($sql);
 $row=mysql_fetch_assoc($result);
 $id = $row['id'];
 return $id;
}
function getQuoteData($table,$selectColumn1,$selectColumn2,$conditionColumn,$condition){
 $sql="select ".$selectColumn1." as val1, ".$selectColumn2." as val2 from ".$table." where `".$conditionColumn."` = '$condition' AND `isDeleted` = 'no' ";
 $result = mysql_query($sql);
 $sqlresult=array();
		while($row = mysql_fetch_assoc($result))
		{
			$sqlresult[0] = $row['val1'];
			$sqlresult[1] = $row['val2'];
		}
		return $sqlresult;
}
function getClaimRatioDetails($claimRatioId){
	 $sql="select * from tbl_claim_ratio WHERE `isDeleted` = 'no' AND `id` = '$claimRatioId' ";
	 $result = mysql_query($sql);
	 $sqlresult=array();
	 $row = mysql_fetch_assoc($result);
	 $sqlresult['claimRatioMin'] = $row['claimRatioMin'];
	 $sqlresult['claimRatioMax'] = $row['claimRatioMax'];
	 $sqlresult['discount'] = $row['discount'];
	 $sqlresult['loading'] = $row['loading'];
	return $sqlresult;
}
function mediaTypeList($table){
 $dataArray=array();
 $sql="select *  from `tbl_ad_banner`  WHERE `id`!='' AND `isDeleted` = 'no' ORDER BY `id` DESC ";
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
function videoTypeList($table){
 $dataArray=array();
 $sql="select *  from `tbl_media`  WHERE `mediaId`!='' AND `title`='Video'";
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
function gradeList($companyId){
 $dataArray=array();
 $sql="select *  from `tbl_grade`  WHERE `companyId`='$companyId' AND `isDeleted` = 'no' ORDER BY `id` ASC ";
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
function get_record_data($table_name, $primary_key, $primary_value,$field)
	{
		$query = "select `$field` from `$table_name` where `$primary_key`='$primary_value' limit 1";
		$result= mysql_query($query);
		if ($result)
		{
			$row=mysql_fetch_row($result);
			$field_value = stripslashes($row[0]);
		}
		else
		{
			echo "Problem  in function";
		}
		if($field_value=='0')
		{
			$field_value="";
		}
		return str_replace("<p>&nbsp;</p>","",$field_value);
	}
function listdisplaydropdownvaluesproduct($dropdownname,$dropdownid,$tablename,$columnname,$field_value)
{  	
	$function_query= "Select  `$dropdownid`,`$columnname` from `$tablename` order by staticId ASC";
	$result = mysql_query($function_query);
		echo "<select id=\"" . $dropdownname . "\" class=\"txtfield\" style=\"width:300px\"    name=\"" . $dropdownname . "\">";
	while ($row = mysql_fetch_array($result, MYSQL_BOTH)) 
	{
	  if ($row[0] == $field_value)
		{
			echo "<option value=\"" . $row[0] . "\" SELECTED >" . getCategoryRoot($row[0]) .  "</option>";
		}
		else
		{
			echo "<option value=\"" . $row[0] . "\">" . getCategoryRoot($row[0]) . "</option>";
		}
	}		
	echo "</select>";
}


function getMyslFormatDate($check_in)
{
	if($check_in!=''){
	$dd=substr($check_in,0,2);
	$mm=substr($check_in,3,2);
	$yy=substr($check_in,6,4);
	return $yy.'-'.$mm.'-'.$dd;
	}else{
	return $check_in;
	}
}
function getIndianFormatDate($check_in)
{
	if($check_in!="")
	{
	$dd=substr($check_in,8,2);
	$mm=substr($check_in,5,2);
	$yy=substr($check_in,0,4);
	return $dd.'-'.$mm.'-'.$yy;
	}
	else
	{
	  return "";
	}
}
function mysqltoarray($sql)
	{
		//echo $sql;
		$result = mysql_query($sql);
		if(!$result)	{		die("cannot execute query $sql".mysql_error());
		}
		$i= 0;
		$sqlresult=array();
		while($row = mysql_fetch_assoc($result))
		{
			foreach($row as $key => $value )
			{
				$sqlresult[$i][$key] = $value;
			}
	 		$i++;
		}
		return $sqlresult;
	}
function forConditionalDrop($dropdownname,$dropdownid,$tablename,$columnname,$field_value,$width,$condition1,$value1,$condion2,$value2,$drop_val='')
{
	if($drop_val =='') { $drop_val  ="-- Select --";}else {$drop_val   =$drop_val  ; }
	$function_query= "Select  `$dropdownid`,`$columnname` from `$tablename` ";
	if(trim($condition1) !='')
	{
		$function_query.= " where `$condition1` ='$value1'";
	}
	if(trim($condion2) !='')
	{
		$function_query.= " and `$condion2` !='$value2'";
	}
	 $function_query.= "order by `$columnname` ASC";
	$result = mysql_query($function_query);
	//echo $function_query;
	//echo $field_value;
	if (!$result)
	{
	   die('Invalid formation of select query in listdisplaydropdownvalues(): ' . mysql_error());
	}
	echo "<select id=\"" . $dropdownname . "\"  style=\"width:".$width."px\" name=\"" . $dropdownname . "\" class='login-textfield' >";
	echo "<option  value=\"\">" . $drop_val. "</option>";
	while ($row = mysql_fetch_array($result, MYSQL_BOTH))
	{
	  if ($row[0] == $field_value)
		{
			echo "<option value=\"" . $row[0] . "\" SELECTED >" . $row[1] .  "</option>";
		}
		else
		{
			echo "<option  value=\"" . $row[0] . "\">" . $row[1] . "</option>";
		}
	}
	echo "</select>";
}
function touploadItem($filedName ,$target_path,$filename)
{
	 $target_path = $target_path.$filename;
	  if(move_uploaded_file($_FILES[$filedName]['tmp_name'], $target_path))
		{
			chmod($target_path,0777);
			//echo "The file ". basename( $_FILES['file']['name']). " has been uploaded";
			//exit;

			//$upload_item=basename($prefix.$_FILES[$filedName]['name']);
		}
		return $filename;
}
function toHAndleSpace($string)
{
   $string = ereg_replace("[ \t\n\r]+", " ", $string);
   $text = str_replace(" ","_", $string);
	return $text;
}
function getCategoryRoot($staticId)
{
	if($staticId<1)
	{
		//echo "ashish";
	}
	else
	{
		 $sql="select `staticId`,parentId,staticTitle from `tbl_static_pages` where `staticId`='$staticId'";
		$Result=mysql_query($sql);
		//echo "<br>";
		$row=mysql_fetch_array($Result);
		if($row[1]>0) { $con=" >> "; } else { $con="";}
		$staticId=$row[0];
		return (getCategoryRoot($row[1]).$con.$row[2]);
	}
}

function count_pages()
{    $result = mysql_query("SELECT * FROM tbl_static_pages");  
      $num_rows = mysql_num_rows($result);  
      echo $num_rows;  
}
function count_testimonials()
{    $result = mysql_query("SELECT * FROM tbl_testi");  
      $num_rows = mysql_num_rows($result);  
      echo $num_rows;  
}
function count_openings()
{    $result = mysql_query("SELECT * FROM tbl_career");  
      $num_rows = mysql_num_rows($result);  
      echo $num_rows;  
}
function count_images()
{    $result = mysql_query("SELECT * FROM tbl_media WHERE `title`='Image'");  
      $num_rows = mysql_num_rows($result);  
      echo $num_rows;  
}
function count_videos()
{     $result = mysql_query("SELECT * FROM `tbl_media` WHERE `title`='Video'");  
      $num_rows = mysql_num_rows($result);  
      echo $num_rows;  
}
function count_news()
{    $result = mysql_query("SELECT * FROM `tbl_news` ");  
      $num_rows = mysql_num_rows($result);  
      echo $num_rows;  
}
function verifyData($table,$col,$colval,$col1,$colval1){
	$sqlstr = '';
	if($col!='') 	{ $sqlstr.=" AND `col` = '$colval' 	"; 	}
	if($col1!='') 	{ $sqlstr.=" AND `col1` = '$colval1' "; }
	$query = mysql_query("select * from `$table` where 1 ".$sqlstr." ");
	$num_rows = mysql_num_rows($result);  
    echo $num_rows;
}
function calculateAge($timestamp = 0, $now = 0) {
    # default to current time when $now not given
    if ($now == 0)
        $now = time();
 
    # calculate differences between timestamp and current Y/m/d
    $yearDiff   = date("Y", $now) - date("Y", $timestamp);
    $monthDiff  = date("m", $now) - date("m", $timestamp);
    $dayDiff    = date("d", $now) - date("d", $timestamp);
 
    # check if we already had our birthday
    if ($monthDiff < 0)
        $yearDiff--;
    elseif (($monthDiff == 0) && ($dayDiff < 0))
        $yearDiff--;
 
    # set the result: age in years
    $result = intval($yearDiff);
 
    # deliver the result
    return $result;
}
function getAgeGroupId($age){
 $sql="select `id`,`ageGroupName` from `tbl_age_group` where `isDeleted` = 'no' ";
 $result = mysql_query($sql);
 while($row=mysql_fetch_assoc($result))
	{
	 $ageGroupName = $row['ageGroupName'];
	 $id = $row['id'];
	 $agrgrouparr = explode("-",$ageGroupName);
		 if(($agrgrouparr[0]<=$age)&&($agrgrouparr[1]>=$age)){ 
		 return $id;
 		}	
	} 
}
function alreadyExist($tablename,$col1,$colval1,$col2,$colval2,$col3,$colval3){    
	$sqlstr.= '';
	if($col1!='') 	{ $sqlstr.=" AND `$col1` = '$colval1' "; 	}
	if($col2!='') 	{ $sqlstr.=" AND `$col2` = '$colval2' "; 	}
	if($col3!='') 	{ $sqlstr.=" AND `$col3` = '$colval3' "; 	}
	$result = mysql_query("SELECT * FROM `$tablename` WHERE 1 ".$sqlstr." ");  
    $num_rows = mysql_num_rows($result);  
    return $num_rows;  
}
function chkUniqueEmpNo($empNo,$companyId){
	$rs_tblsite=mysql_query("select `id` from `tbl_company_employee` where `empNo` = '$empNo' AND `isDeleted` = 'no' AND `companyId` = '$companyId' ");
	$no=mysql_num_rows($rs_tblsite);
	return $no;
}
function chkGrade($grade,$companyId){
	$rs_tblsite=mysql_query("select `id` from `tbl_grade` where `grade` = '$grade' AND `isDeleted` = 'no' AND `companyId` = '$companyId' ");
	$row=mysql_fetch_assoc($rs_tblsite);
	return $row['id'];
}
function chkSumInsured($suminsured){
	$suminsured = str_replace(',', '',$suminsured);
	if($rs_tblsite=mysql_query("select `id` from `tbl_sum_insured` where `sumInsured` = '$suminsured' AND `isDeleted` = 'no' ")){
		if($no=mysql_fetch_assoc($rs_tblsite)){
			return $no['id'];
		}
	}
	return 0;
}
function getEmpId($empNo,$companyId){
	$rs_tblsite=mysql_query("select `id` from `tbl_company_employee` where `empNo` = '$empNo' AND `isDeleted` = 'no' AND `companyId` = '$companyId' ");
	$row=mysql_fetch_assoc($rs_tblsite);
	return $row['id'];
}

function getPremium($companyId, $premium, $topupSi = 0)
{
    $sql = "SELECT * FROM tbl_company_employee WHERE companyId = $companyId";
//    New Premium = Old Premium * Multiplier
//    Where Multiplier = No. of Days (Policy End Date – Enrolment Start Date) / No. of Days (Policy Period)

}
/* function made for web enrol end date extension */
function getTotalBatch($companyId){
 $dataArray=array();
 //$sql = "SELECT * from tbl_company_employee where enrolStartDate between '".$enrolStartDateFrom."' AND '".$enrolStartDateTo."' and companyId=".$companyId."";
 $sql = "SELECT DISTINCT(batchno),companyName from tbl_company_employee where companyId=".$companyId."";
 $result = mysql_query($sql);
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
function getBatchDetails($companyId,$batchno){
 $dataArray=array();
 $sql = "SELECT count(*) as totalrow,enrolStartDate,enrolEndDate,companyName from tbl_company_employee where batchno='".$batchno."' and companyId=".$companyId."";
 $result = mysql_query($sql);
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
function getBatchEmployeesDetails($companyId,$batchno){
 $dataArray=array();
 $sql = "SELECT empEmail,empFirstName,empNo,companyId,enrolStartDate,enrolEndDate,companyName from tbl_company_employee where batchno=".$batchno." and companyId=".$companyId." and updatedBy='admin'";
 $result = mysql_query($sql);
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
function getBatchCurrentDetail($companyId){
 $dataArray=array();
 //$sql = "SELECT max(batchno) as batchcount from tbl_company_employee where companyId='".$companyId."'";
 $sql = "SELECT MAX(CONVERT(batchno,SIGNED)) as batchcount from tbl_company_employee where companyId='".$companyId."'";
 
 $result = mysql_query($sql);
 $dataArray=mysqltoarray($sql);
 return $dataArray;
    
}
function getUrlStatusofBatch($companyId,$batchno){
 $dataArray=array();
 $sql = "SELECT urlStatus from tbl_company_employee where batchno='".$batchno."' and companyId=".$companyId."";
 $result = mysql_query($sql);
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
function expiredCorporatesList(){
 $dataArray=array();
 $currentDate = strtotime(date("Y-m-d H:i:s"));
 $sql="select * from tbl_company where policyEndDate < '".$currentDate."' and `isDeleted` = 'no' ";
 $dataArray=mysqltoarray($sql);
 return $dataArray;
}
?>
