<?php
/* * ***************************************************************************
 * COPYRIGHT
 * Copyright 2013 Catabatic Automation Technology Pvt Ltd.
 * All rights reserved
 *
 * DISCLAIMER
 *
 *
 * AUTHOR
 *
 * $Id: reminderMailEmployeeCron.php,v 1.0 2016/05/09 12:51:37 Pooja Choudhary Exp $
 * $Author: Pooja Choudhary $
 *
 * ************************************************************************** */
include('conf/conf.php');
//include('conf/fucts.php');
include('cms/mailTemplate.php');


$cQuery = "select `id`,`companyName`,`reminderFrequency`,`createdOn` from `tbl_company` where `isDeleted` = 'no' AND  `reminderFrequency` != ''";
$compQuery = mysql_query($cQuery);
$array_count = mysql_num_rows($compQuery);
$i=1;
while ($compData = mysql_fetch_array($compQuery))
{
    $today = date('d-m-Y');
    $createdOn = date('d-m-Y', $compData['createdOn']);
    $reminderFrequency = $compData['reminderFrequency'];

    $noOfDays = reminderMail::dateDiff($createdOn, $today);
    //echo '<pre>'; print_r($compData);
//    echo $noOfDays.'---------'.$reminderFrequency.'-----';
    if (($noOfDays % $reminderFrequency) == 0) //  1 == 1 remove after test
    {
         echo $empStatus = reminderMail::executeEmp($compData['id'], $today);
        // echo $i.': '.$empStatus.'</br>';
    }
$i++;}


class reminderMail
{
    static function executeEmp($compId, $time)
    {
        $eQuery = "SELECT id,empEmail,empFirstName,empNo,companyId,enrolStartDate,enrolEndDate,companyName FROM `tbl_company_employee` where companyId = '" . $compId . "' AND '" . strtotime($time) . "' between enrolStartDate and enrolEndDate AND lastLogin='0' AND isDeleted = 'no'";
        $empQuery = mysql_query($eQuery);
        while ($empData = mysql_fetch_array($empQuery))
        { 
            $sEmployee = $empData['empEmail'] . ":" . $empData['empFirstName'] . ":" . $empData['empNo'] . ":" . $compId . ":" . $empData['enrolStartDate'] . ":" . $empData['enrolEndDate'] . ":" . $empData['companyName'];

            $mailStatus = reminderMail::mailEmployeeLink($sEmployee);
            return $mailStatus;
        }
    }
    
/*
 * Get number of days deference between start date and end date.
 * @param string $start, $end
 */
    static function dateDiff($start, $end)
    {
        $start_ts = strtotime($start);
        $end_ts = strtotime($end);
        $diff = abs($end_ts - $start_ts);
        return round($diff / 86400);
    }

    /**
     * Mail Employee. To create mail list of uploaded employees.
     * @param array $emp
     */
    static function mailEmployeeLink($emp)
    {
//        global $appRoot;
        global $template;
        if ($emp != array())
        {            
            $companyId = null;
            //$webRoot = @$webRoot ? $webRoot : 'http://rhicluat.religare.com/religare/webenrol/';
//            $appRoot = 'http://rhicluat.religare.com/religare/webenrol/';
     	    $empDetail = explode(":", $emp);
            $to = $empDetail[0];
            $empNo = $empDetail[2];
            $companyId = $empDetail[3];
            $subject = 'REMINDER: Employee Access';
            $message = 'Employee Message';
            $employeeUrl = base64_encode($empDetail[0] . ":" . $empDetail[1] . ":" . $empDetail[2] .":" . $empDetail[3] .":". $empDetail[4].":".$empDetail[5]);
            $empUrl = APPROOT . "company_" . $employeeUrl;
            //print($empUrl);sleep(100);
            $mailDate = time();

            $formattedTemplate = str_replace("##Name", $empDetail[1], $template);
            $formattedTemplate = str_replace("##companyname", $empDetail[6], $formattedTemplate);
            $formattedTemplate = str_replace("##employeetopupplanlink", $empUrl, $formattedTemplate);
            $formattedTemplate = str_replace("##date", date('M d,Y', $empDetail[5]), $formattedTemplate);
//                 print_r($formattedTemplate); die;
            $headers = "From: enrollment@religare.com \r\n" .
                'MIME-Version: 1.0' . "\r\n" .
                'Content-type: text/html; charset=iso-8859-1';
            if (mail($to, $subject, $formattedTemplate, $headers))
            {// @mail('pooja@catpl.co.in', $subject, $formattedTemplate, $headers);
                
//                $sql = "INSERT INTO `tbl_employee_link` (`empNo`,`mailTo`,`mailSubject`,`mailContent` ,`mailDate`,`mailType`)VALUES ('$empNo', '$to','$subject','$empUrl','$mailDate','1')";
//                mysql_query($sql);
                return 'Success: Mail Sent Successfully';
            }
            else
            {
                return 'Error: Mail not Sent';
            }
        }
    }

}
mail('pooja@catpl.co.in', 'test cron', 'test cron message from webenrol',"From: enrollment@religare.com \r\n"); // only for test 
?>
