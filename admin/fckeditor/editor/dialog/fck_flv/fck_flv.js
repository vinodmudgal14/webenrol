﻿/*
 * FCKeditor - The text editor for Internet - http://www.fckeditor.net
 * Copyright (C) 2003-2008 Frederico Caldeira Knabben
 *
 * == BEGIN LICENSE ==
 *
 * Licensed under the terms of any of the following licenses at your
 * choice:
 *
 *  - GNU General Public License Version 2 or later (the "GPL")
 *    http://www.gnu.org/licenses/gpl.html
 *
 *  - GNU Lesser General Public License Version 2.1 or later (the "LGPL")
 *    http://www.gnu.org/licenses/lgpl.html
 *
 *  - Mozilla Public License Version 1.1 or later (the "MPL")
 *    http://www.mozilla.org/MPL/MPL-1.1.html
 *
 * == END LICENSE ==
 *
 * Scripts related to the Video dialog window (see fck_Video.html).
 */

var dialog		= window.parent ;
var oEditor		= dialog.InnerDialogLoaded() ;
var FCK			= oEditor.FCK ;
var FCKLang		= oEditor.FCKLang ;
var FCKConfig	= oEditor.FCKConfig ;
var FCKTools	= oEditor.FCKTools ;

//#### Dialog Tabs

// Set the dialog tabs.
dialog.AddTab( 'Info', oEditor.FCKLang.DlgInfoTab ) ;

if ( FCKConfig.FLVUpload )
	dialog.AddTab( 'Upload', FCKLang.DlgFLVUpload ) ;

//if ( !FCKConfig.VideoDlgHideAdvanced )
//	dialog.AddTab( 'Advanced', oEditor.FCKLang.DlgAdvancedTag ) ;

// Function called when a dialog tag is selected.
function OnDialogTabChange( tabCode )
{
	ShowE('divInfo', ( tabCode == 'Info') ) ;
	ShowE('divUpload', ( tabCode == 'Upload') ) ;
//	ShowE('divAdvanced', ( tabCode == 'Advanced') ) ;
}

// Get the selected Video embed (if available).
var oFakeImage = dialog.Selection.GetSelectedElement() ;
var oEmbed ;

if ( oFakeImage )
{
	if ( oFakeImage.tagName == 'IMG'&& oFakeImage.getAttribute('_fckFLV') )
		oEmbed = FCK.GetRealElement( oFakeImage ) ;
	else
		oFakeImage = null ;
}

window.onload = function()
{
	// Translate the dialog box texts.
	oEditor.FCKLanguageManager.TranslatePage(document) ;

	// Load the selected element information (if any).
	LoadSelection() ;

	// Show/Hide the "Browse Server" button.
	GetE('tdBrowse').style.display = FCKConfig.FLVBrowser	? '': 'none';

	// Set the actual uploader URL.
	if ( FCKConfig.FLVUpload )
		GetE('frmUpload').action = FCKConfig.FLVUploadURL ;

	dialog.SetAutoSize( true ) ;

	// Activate the "OK" button.
	dialog.SetOkButton( true ) ;

	SelectField( 'txtUrl') ;
}

function LoadSelection()
{
	if ( ! oEmbed ) return ;

	GetE('txtUrl').value    = GetAttribute( oEmbed, 'src', '') ;
	GetE('txtWidth').value  = GetAttribute( oEmbed, 'width', '') ;
	GetE('txtHeight').value = GetAttribute( oEmbed, 'height', '') ;

	// Get Advances Attributes
	GetE('txtAttId').value		= oEmbed.id ;
	GetE('chkAutoPlay').checked	= GetAttribute( oEmbed, 'play', 'true') == 'true';
	GetE('chkLoop').checked		= GetAttribute( oEmbed, 'loop', 'true') == 'true';
	GetE('chkMenu').checked		= GetAttribute( oEmbed, 'menu', 'true') == 'true';
	GetE('cmbScale').value		= GetAttribute( oEmbed, 'scale', '').toLowerCase() ;

	GetE('txtAttTitle').value		= oEmbed.title ;

	if ( oEditor.FCKBrowserInfo.IsIE )
	{
		GetE('txtAttClasses').value = oEmbed.getAttribute('className') || '';
		GetE('txtAttStyle').value = oEmbed.style.cssText ;
	}
	else
	{
		GetE('txtAttClasses').value = oEmbed.getAttribute('class',2) || '';
		GetE('txtAttStyle').value = oEmbed.getAttribute('style',2) || '';
	}

	UpdatePreview() ;
}

//#### The OK button was hit.
function Ok()
{
	if ( GetE('txtUrl').value.length == 0 )
	{
		dialog.SetSelectedTab( 'Info') ;
		GetE('txtUrl').focus() ;

		alert( oEditor.FCKLang.DlgAlertUrl ) ;

		return false ;
	}

	oEditor.FCKUndo.SaveUndoStep() ;
	if ( !oEmbed )
	{
		oEmbed		= FCK.EditorDocument.createElement( 'OBJECT') ;
		oFakeImage  = null ;
	}
	UpdateEmbed( oEmbed ) ;

	if ( !oFakeImage )
	{
		oFakeImage	= oEditor.FCKDocumentProcessor_CreateFakeImage( 'FCK__FLV', oEmbed ) ;
		oFakeImage.setAttribute( '_fckFLV', 'true', 0 ) ;
		oFakeImage	= FCK.InsertElement( oFakeImage ) ;
	}

	oEditor.FCKEmbedAndObjectProcessor.RefreshView( oFakeImage, oEmbed ) ;

	return true ;
}

function UpdateEmbed( e )
{
	SetAttribute( e, 'id', 'GGWindowsMediaPlayer') ;
	SetAttribute( e, 'codeBase', 'http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=5,1,52,701') ;
	SetAttribute( e, 'type', 'application/x-oleobject') ;
	SetAttribute( e, 'standby', 'Loading Microsoft® Windows® Media Player components...') ;
	SetAttribute( e, 'classid', 'CLSID:22D6F312-B0F6-11D0-94AB-0080C74C7E95') ;
	var f		= FCK.EditorDocument.createElement( 'PARAM') ;
	SetAttribute( f, 'NAME', 'filename') ;
	SetAttribute( f, 'Value', GetE('txtUrl').value) ;
	e.appendChild(f);
	var g		= FCK.EditorDocument.createElement( 'PARAM') ;
	SetAttribute( g, 'NAME', 'AutoStart') ;
	SetAttribute( g, 'Value', 'True') ;
	e.appendChild(g);
	var h		= FCK.EditorDocument.createElement( 'PARAM') ;
	SetAttribute( h, 'NAME', 'DisplaySize') ;
	SetAttribute( h, 'Value', '0') ;
	e.appendChild(h);
	var j		= FCK.EditorDocument.createElement( 'PARAM') ;
	SetAttribute( j, 'NAME', 'ShowPositionControls') ;
	SetAttribute( j, 'Value', 'True') ;
	e.appendChild(j);
	var k		= FCK.EditorDocument.createElement( 'PARAM') ;
	SetAttribute( k, 'NAME', 'ShowStatusBar') ;
	SetAttribute( k, 'Value', 'True') ;
	e.appendChild(k);
	var l		= FCK.EditorDocument.createElement( 'PARAM') ;
	SetAttribute( l, 'NAME', 'ShowTracker') ;
	SetAttribute( l, 'Value', 'True') ;
	e.appendChild(l);
	var m		= FCK.EditorDocument.createElement( 'embed') ;
	SetAttribute( m, 'name', 'GGWindowsMediaPlayer') ;
	SetAttribute( m, 'type', 'application/x-mplayer2') ;
	SetAttribute( m, 'pluginspage', 'http://www.microsoft.com/Windows/Downloads/Contents/Products/MediaPlayer/') ;
	SetAttribute( m, 'src', GetE('txtUrl').value) ;
	SetAttribute( m, 'autostart', '1') ;
	SetAttribute( m, 'displaysize', '0') ;
	SetAttribute( m, 'autosize', '0') ;
	SetAttribute( m, 'showcontrols', '1') ;
	SetAttribute( m, 'showtracker', '1') ;
	SetAttribute( m, 'showstatusbar', '1') ;
	e.appendChild(m);
	/*SetAttribute( e, 'src', GetE('txtUrl').value ) ;
	SetAttribute( e, "width" , GetE('txtWidth').value ) ;
	SetAttribute( e, "height", GetE('txtHeight').value ) ;
*/
	// Advances Attributes

	//SetAttribute( e, 'id', GetE('txtAttId').value ) ;
//	SetAttribute( e, 'scale', GetE('cmbScale').value ) ;
//
//	SetAttribute( e, 'play', GetE('chkAutoPlay').checked ? 'true': 'false') ;
//	SetAttribute( e, 'loop', GetE('chkLoop').checked ? 'true': 'false') ;
//	SetAttribute( e, 'menu', GetE('chkMenu').checked ? 'true': 'false') ;

	//SetAttribute( e, 'title', GetE('txtAttTitle').value ) ;

	if ( oEditor.FCKBrowserInfo.IsIE )
	{
		SetAttribute( e, 'className', GetE('txtAttClasses').value ) ;
		e.style.cssText = GetE('txtAttStyle').value ;
	}
	else
	{
		SetAttribute( e, 'class', GetE('txtAttClasses').value ) ;
		SetAttribute( e, 'style', GetE('txtAttStyle').value ) ;
	}
}

var ePreview ;

function SetPreviewElement( previewEl )
{
	ePreview = previewEl ;

	if ( GetE('txtUrl').value.length > 0 )
		UpdatePreview() ;
}

function UpdatePreview()
{
	if ( !ePreview )
		return ;

	while ( ePreview.firstChild )
		ePreview.removeChild( ePreview.firstChild ) ;

	if ( GetE('txtUrl').value.length == 0 )
		ePreview.innerHTML = '&nbsp;';
	else
	{
		var oDoc	= ePreview.ownerDocument || ePreview.document ;
		var e		= oDoc.createElement( 'OBJECT') ;
	SetAttribute( e, 'id', 'GGWindowsMediaPlayer') ;
	SetAttribute( e, 'codeBase', 'http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=5,1,52,701') ;
	SetAttribute( e, 'type', 'application/x-oleobject') ;
	SetAttribute( e, 'standby', 'Loading Microsoft® Windows® Media Player components...') ;
	SetAttribute( e, 'classid', 'CLSID:22D6F312-B0F6-11D0-94AB-0080C74C7E95') ;
	var f		= FCK.EditorDocument.createElement( 'PARAM') ;
	SetAttribute( f, 'NAME', 'filename') ;
	SetAttribute( f, 'Value', GetE('txtUrl').value) ;
	e.appendChild(f);
	var g		= FCK.EditorDocument.createElement( 'PARAM') ;
	SetAttribute( g, 'NAME', 'AutoStart') ;
	SetAttribute( g, 'Value', 'True') ;
	e.appendChild(g);
	var h		= FCK.EditorDocument.createElement( 'PARAM') ;
	SetAttribute( h, 'NAME', 'DisplaySize') ;
	SetAttribute( h, 'Value', '0') ;
	e.appendChild(h);
	var j		= FCK.EditorDocument.createElement( 'PARAM') ;
	SetAttribute( j, 'NAME', 'ShowPositionControls') ;
	SetAttribute( j, 'Value', 'True') ;
	e.appendChild(j);
	var k		= FCK.EditorDocument.createElement( 'PARAM') ;
	SetAttribute( k, 'NAME', 'ShowStatusBar') ;
	SetAttribute( k, 'Value', 'True') ;
	e.appendChild(k);
	var l		= FCK.EditorDocument.createElement( 'PARAM') ;
	SetAttribute( l, 'NAME', 'ShowTracker') ;
	SetAttribute( l, 'Value', 'True') ;
	e.appendChild(l);
	var m		= FCK.EditorDocument.createElement( 'embed') ;
	SetAttribute( m, 'name', 'GGWindowsMediaPlayer') ;
	SetAttribute( m, 'type', 'application/x-mplayer2') ;
	SetAttribute( m, 'pluginspage', 'http://www.microsoft.com/Windows/Downloads/Contents/Products/MediaPlayer/') ;
	SetAttribute( m, 'src', GetE('txtUrl').value) ;
	SetAttribute( m, 'autostart', '1') ;
	SetAttribute( m, 'displaysize', '0') ;
	SetAttribute( m, 'autosize', '0') ;
	SetAttribute( m, 'showcontrols', '1') ;
	SetAttribute( m, 'showtracker', '1') ;
	SetAttribute( m, 'showstatusbar', '1') ;
	e.appendChild(m);
		
		//SetAttribute( e, 'VALUE', GetE('txtUrl').value) ;
//		SetAttribute( e, 'src', GetE('txtUrl').value ) ;
//		SetAttribute( e, 'type', 'application/x-shockwave-Video') ;
//		SetAttribute( e, 'width', '100%') ;
//		SetAttribute( e, 'height', '100%') ;

		ePreview.appendChild( e ) ;
	}
}

// <embed id="ePreview" src="fck_Video/claims.swf" width="100%" height="100%" style="visibility:hidden" type="application/x-shockwave-Video" pluginspage="http://www.macromedia.com/go/getVideoplayer">

function BrowseServer()
{
	OpenFileBrowser( FCKConfig.FLVBrowserURL, FCKConfig.FLVBrowserWindowWidth, FCKConfig.FLVBrowserWindowHeight ) ;
}

function SetUrl( url, width, height )
{
	GetE('txtUrl').value = url ;

	if ( width )
		GetE('txtWidth').value = width ;

	if ( height )
		GetE('txtHeight').value = height ;

	UpdatePreview() ;

	dialog.SetSelectedTab( 'Info') ;
}

function OnUploadCompleted( errorNumber, fileUrl, fileName, customMsg )
{
	// Remove animation
	window.parent.Throbber.Hide() ;
	GetE( 'divUpload').style.display  = '';

	switch ( errorNumber )
	{
		case 0 :	// No errors
			alert( 'Your file has been successfully uploaded') ;
			break ;
		case 1 :	// Custom error
			alert( customMsg ) ;
			return ;
		case 101 :	// Custom warning
			alert( customMsg ) ;
			break ;
		case 201 :
			alert( 'A file with the same name is already available. The uploaded file has been renamed to "'+ fileName + '"') ;
			break ;
		case 202 :
			alert( 'Invalid file type') ;
			return ;
		case 203 :
			alert( "Security error. You probably don't have enough permissions to upload. Please check your server." ) ;
			return ;
		case 500 :
			alert( 'The connector is disabled') ;
			break ;
		default :
			alert( 'Error on file upload. Error number: '+ errorNumber ) ;
			return ;
	}

	SetUrl( fileUrl ) ;
	GetE('frmUpload').reset() ;
}

var oUploadAllowedExtRegex	= new RegExp( FCKConfig.FLVUploadAllowedExtensions, 'i') ;
var oUploadDeniedExtRegex	= new RegExp( FCKConfig.FLVUploadDeniedExtensions, 'i') ;

function CheckUpload()
{
	var sFile = GetE('txtUploadFile').value ;

	if ( sFile.length == 0 )
	{
		alert( 'Please select a file to upload') ;
		return false ;
	}

	if ( ( FCKConfig.FLVUploadAllowedExtensions.length > 0 && !oUploadAllowedExtRegex.test( sFile ) ) ||
		( FCKConfig.FLVUploadDeniedExtensions.length > 0 && oUploadDeniedExtRegex.test( sFile ) ) )
	{
		OnUploadCompleted( 202 ) ;
		return false ;
	}

	// Show animation
	window.parent.Throbber.Show( 100 ) ;
	GetE( 'divUpload').style.display  = 'none';

	return true ;
}
