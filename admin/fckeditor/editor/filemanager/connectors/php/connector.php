<?php
/*
 * FCKeditor - The text editor for Internet - http://www.fckeditor.net
 * Copyright (C) 2003-2008 Frederico Caldeira Knabben
 *
 * == BEGIN LICENSE ==
 *
 * Licensed under the terms of any of the following licenses at your
 * choice:
 *
 *  - GNU General Public License Version 2 or later (the "GPL")
 *    http://www.gnu.org/licenses/gpl.html
 *
 *  - GNU Lesser General Public License Version 2.1 or later (the "LGPL")
 *    http://www.gnu.org/licenses/lgpl.html
 *
 *  - Mozilla Public License Version 1.1 or later (the "MPL")
 *    http://www.mozilla.org/MPL/MPL-1.1.html
 *
 * == END LICENSE ==
 *
 * This is the File Manager Connector for PHP.
 */

ob_start() ;

require('./config.php') ;
require('./util.php') ;
require('./io.php') ;
require('./basexml.php') ;
require('./commands.php') ;
require('./phpcompat.php') ;

if ( !$Config['Enabled'] )
	SendError( 1, 'This connector is disabled. Please check the "editor/filemanager/connectors/php/config.php" file' ) ;

DoResponse() ;
// added by wasim
function sanitize($input_data) {
	$searchArr=array("document","write","alert","%","$",";","+","#","<",">",")","(","'","\'","<img","src=",".ini","<iframe","java:","window.open","http","!",":boot",".exe",".com",".php",".js",".txt",".css","@","having","sleep0");
	$input_data= str_replace("script","",$input_data);
	$input_data= str_replace("iframe","",$input_data);
	$input_data= str_replace($searchArr,"",$input_data);
	$input_data=trim($input_data);
	return htmlentities(stripslashes($input_data), ENT_QUOTES);
}

 
// ended
function DoResponse()
{
    if (!isset($_GET)) {
        global $_GET;
    }
	if ( !isset( sanitize($_GET['Command'] )) || !isset( sanitize($_GET['Type']) ) || !isset( sanitize($_GET['CurrentFolder']) ) )
		return ;

	// Get the main request informaiton.
	$sCommand	= sanitize($_GET['Command']) ;  // sanitize by wasim
	$sResourceType	= sanitize($_GET['Type']) ;   // sanitize by wasim
	$sCurrentFolder	= GetCurrentFolder();

	// Check if it is an allowed command
	if ( ! IsAllowedCommand( $sCommand ) )
		SendError( 1, 'The "' . $sCommand . '" command isn\'t allowed' ) ;

	// Check if it is an allowed type.
	if ( !IsAllowedType( $sResourceType ) )
		SendError( 1, 'Invalid type specified' ) ;

	// File Upload doesn't have to Return XML, so it must be intercepted before anything.
	if ( $sCommand == 'FileUpload' )
	{
		FileUpload( $sResourceType, $sCurrentFolder, $sCommand ) ;
		return ;
	}

	CreateXmlHeader( $sCommand, $sResourceType, $sCurrentFolder ) ;

	// Execute the required command.
	switch ( $sCommand )
	{
		case 'GetFolders' :
			GetFolders( $sResourceType, $sCurrentFolder ) ;
			break ;
		case 'GetFoldersAndFiles' :
			GetFoldersAndFiles( $sResourceType, $sCurrentFolder ) ;
			break ;
		case 'CreateFolder' :
			CreateFolder( $sResourceType, $sCurrentFolder ) ;
			break ;
	}

	CreateXmlFooter() ;

	exit ;
}
?>
