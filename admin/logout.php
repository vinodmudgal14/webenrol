<?php
session_start();
session_destroy();
$_SESSION["Flag"]		=	0;
$_SESSION["id"]			=	"";
$_SESSION["username"]	=	"";
$_SESSION["firstName"]	=	"";
$_SESSION["PHPSESSID"]	=	"";
$_SESSION["color"]		=	"";
setcookie ('PHPSESSID', '', time()-3600, '/', '', 0, 0); // Destroy the cookie.

session_regenerate_id(true);

foreach (@$_COOKIE as $key => $value )
{
    setcookie( $key, '', 1, '/' );
}
header("Location: index.php");
exit;
?>