<?php 
include('../conf/session.php');
include('../conf/conf.php');
include('../conf/fucts.php');
$companyId = sanitize_data(@$_REQUEST['companyId']);
$companyWellness =  companyWellnessDetails(@$companyId);

$wellnessArray = array();
for($i=0;$i<count($companyWellness);$i++){
$wellnessArray[]	=	$companyWellness[$i]['wellness'];
	if($companyWellness[$i]['wellness']=='HRA'){ $hraStartDate =  $companyWellness[$i]['startDate'];	$hraEndDate =  $companyWellness[$i]['endDate'];	}
	if($companyWellness[$i]['wellness']=='Doctor on call'){ $callStartDate =  $companyWellness[$i]['startDate']; $callEndDate =  $companyWellness[$i]['endDate'];	}
	if($companyWellness[$i]['wellness']=='Doctor on chat'){ $chatStartDate =  $companyWellness[$i]['startDate']; $chatEndDate =  $companyWellness[$i]['endDate'];	}
}
//echo '<pre>';
//print_r($companyWellness);
?>
<style>
.middle-heading-bg {
    background: url("../images/green/administration-heading-bg.jpg") repeat-x scroll 0 0 transparent;
    line-height: 36px;
    margin-bottom: 5px;
    padding: 0 11px;
	}
	.middle-heading-bg h1 {
    background: url("../images/green/middle-heading-icon.png") no-repeat scroll left center transparent;
    color: #FFFFFF;
    display: block;
    font-family: Arial,Helvetica,sans-serif;
    font-size: 16px;
    font-weight: normal;
    padding-left: 20px;
}
.middle-data {
    background: none repeat scroll 0 0 #FFFFFF;
    border: 1px solid #E4E4E4;
}
.col-border_event {
    background: none repeat scroll 0 0 #F0F0F0;
    border-bottom: 1px solid #D9D9D9;
    border-right: 1px solid #D9D9D9;
    color: #000000;
    font-size: 12px;
    font-weight: normal;
    padding: 5px 11px;
}
</style>
<script src="<?=_WWWROOT;?>/js/jquery.js"></script>
<script src="<?=_WWWROOT;?>/js/jquery.livequery.js"></script>
<script type="text/javascript" src="<?=_WWWROOT;?>/js/ui.datepicker.js"></script>
<link href="<?=_WWWROOT;?>/css/ui.datepicker.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
$(document).ready(function()
{
	$("#hraStartDate").datepicker({
	minDate: '0',
	yearRange: '2013:2020',
    dateFormat: "dd-mm-yy"
	});	
	$("#hraEndDate").datepicker({
	minDate: '0',
	yearRange: '2013:2020',
    dateFormat: "dd-mm-yy"
	});	
	$("#callStartDate").datepicker({
	minDate: '0',
	yearRange: '2013:2020',
    dateFormat: "dd-mm-yy"
	});	
	$("#callEndDate").datepicker({
	minDate: '0',
	yearRange: '2013:2020',
    dateFormat: "dd-mm-yy"
	});	
	$("#chatStartDate").datepicker({
	minDate: '0',
	yearRange: '2013:2020',
    dateFormat: "dd-mm-yy"
	});	
	$("#chatEndDate").datepicker({
	minDate: '0',
	yearRange: '2013:2020',
    dateFormat: "dd-mm-yy"
	});	
});	
function validate(){
	if ($('#hra').is(':checked')){
		var hraStartDate 	= $('#hraStartDate').val();
		var hraEndDate 		= $('#hraEndDate').val();
		var hrastartarr = hraStartDate.split("-");
		var hraendarr = hraEndDate.split("-");
		var hraStartDateNew = hrastartarr[1]+'/'+hrastartarr[0]+'/'+hrastartarr[2];
		var hraEndDateNew = hraendarr[1]+'/'+hraendarr[0]+'/'+hraendarr[2];
		if(hraStartDate=='') { alert("Please enter HRA start date."); return false;	} 
		if(hraEndDate=='')	 { alert("Please enter HRA end date."); return false;	} else {
		if(hraStartDateNew>hraEndDateNew) { alert("Please enter end date greater than start date."); return false; }
		}
	}	
	if ($('#doctorCall').is(':checked')) {  
		var callStartDate 	= $('#callStartDate').val();
		var callEndDate 	= $('#callEndDate').val();
		var callstartarr = callStartDate.split("-");
		var callendarr = callEndDate.split("-");
		var callStartDateNew = callstartarr[1]+'/'+callstartarr[0]+'/'+callstartarr[2];
		var callEndDateNew = callendarr[1]+'/'+callendarr[0]+'/'+callendarr[2];
		if(callStartDate=='') { alert("Please enter doctor on call start date."); return false;	} 
		if(callEndDate=='')	  { alert("Please enter doctor on call end date."); return false;	}  else {
		if(callStartDateNew>callEndDateNew) { alert("Please enter doctor on call end date greater than start date."); return false; }
		}	
	} 
	if ($('#doctorChat').is(':checked')) {  
		var chatStartDate 	= $('#chatStartDate').val();
		var chatEndDate 	= $('#chatEndDate').val();
		var chatstartarr = callStartDate.split("-");
		var chatendarr = callEndDate.split("-");
		var chatStartDateNew = chatstartarr[1]+'/'+chatstartarr[0]+'/'+chatstartarr[2];
		var chatEndDateNew = chatendarr[1]+'/'+chatendarr[0]+'/'+chatendarr[2];
		if(chatStartDate=='') 	{ alert("Please enter doctor on chat start date."); return false;	} 
		if(chatEndDate=='')	 	{ alert("Please enter doctor on chat end date."); return false;		}  else {
		if(chatStartDateNew>chatEndDateNew) { alert("Please enter doctor on chat end date greater than start date."); return false; }
		}	
	}
}
function check(value){
if(value=='hra') { var id = 'hra'; } 
if(value=='call') { var id = 'doctorCall'; } 
if(value=='chat') { var id = 'doctorChat'; } 

if ($('#'+id).is(':checked')) {
 		$("#"+value+"StartDate").removeAttr('disabled');
		$("#"+value+"EndDate").removeAttr('disabled'); 
} else{
		$("#"+value+"StartDate").attr('disabled','disabled');
		$("#"+value+"EndDate").attr('disabled','disabled');
	}
}
</script>
<div id="middle">
	<div class="middle-heading-bg">
	<h1>Manage Wellness</h1>
	</div> <!--middle heading bg-->
	<div style="border-bottom:none;" class="middle-data">
	<form name="wellnessform" id="wellnessform" action="writeWellness.php" onsubmit="return validate();">
		<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
		<tbody>
		<tr>
		<td align="left" class="col-border_event"><input type="checkbox" name="wellness[]" id="hra" value="HRA" onclick="check('hra')" <?php if(in_array("HRA",$wellnessArray)){ echo 'checked'; }?>/>&nbsp;&nbsp;HRA</td>
		<td align="left" class="col-border_event">Start Date&nbsp;&nbsp;:&nbsp;&nbsp;<input type="text" name="hraStartDate" id="hraStartDate" value="<?php if($hraStartDate>1325356200) { echo date('d-m-Y',$hraStartDate); } else { }?>" <?php if($hraStartDate=='') { echo 'disabled'; } else { }?>/></td>
		<td align="left" class="col-border_event">End Date&nbsp;&nbsp;:&nbsp;&nbsp;<input type="text" name="hraEndDate" id="hraEndDate" value="<?php if($hraEndDate>1325356200) { echo date('d-m-Y',$hraEndDate); } else { }?>" <?php if($hraEndDate=='') { echo 'disabled'; } else { }?>/></td>
		</tr>
		<tr>
		<td align="left" class="col-border_event"><input type="checkbox" name="wellness[]" id="doctorCall" onclick="check('call')" value="Doctor on call" <?php if(in_array("Doctor on call",$wellnessArray)){ echo 'checked'; }?>/>&nbsp;&nbsp;Doctor on Call</td>
		<td align="left" class="col-border_event">Start Date&nbsp;&nbsp;:&nbsp;&nbsp;<input type="text" name="callStartDate" id="callStartDate" value="<?php if($callStartDate>1325356200) {	echo date('d-m-Y',$callStartDate);	}	else { } ?>" <?php if($callStartDate=='') { echo 'disabled'; } else { }?>/></td>
		<td align="left" class="col-border_event">End Date&nbsp;&nbsp;:&nbsp;&nbsp;<input type="text" name="callEndDate" id="callEndDate" value="<?php if($callEndDate>1325356200) {	echo date('d-m-Y',$callEndDate);	}	else { }?>"  <?php if($callEndDate=='') { echo 'disabled'; } else { }?>/></td>
		</tr>
		<tr>
		<td align="left" class="col-border_event"><input type="checkbox" name="wellness[]" id="doctorChat" onclick="check('chat')" value="Doctor on chat" <?php if(in_array("Doctor on chat",$wellnessArray)){ echo 'checked'; }?>/>&nbsp;&nbsp;Doctor on Chat</td>
		<td align="left" class="col-border_event">Start Date&nbsp;&nbsp;:&nbsp;&nbsp;<input type="text" name="chatStartDate" id="chatStartDate" value="<?php if($chatStartDate>1325356200) {	echo date('d-m-Y',$chatStartDate); } else { }?>" <?php if($chatStartDate=='') { echo 'disabled'; } else { }?>/></td>
		<td align="left" class="col-border_event">End Date&nbsp;&nbsp;:&nbsp;&nbsp;<input type="text" name="chatEndDate" id="chatEndDate" value="<?php if($chatEndDate>1325356200) {	echo date('d-m-Y',$chatEndDate);	}	else { }?>" <?php if($chatEndDate=='') { echo 'disabled'; } else { }?>/></td>
		</tr>
		<tr>
		<td align="center" class="col-border_event" colspan="3">
		<input type="hidden" name="companyId" id="companyId" value="<?php echo @$companyId;?>" />		
		<input type="submit" value="Submit"></td>
		</tr>
		</tbody>
		</table>
		</form>
	</div>
</div>