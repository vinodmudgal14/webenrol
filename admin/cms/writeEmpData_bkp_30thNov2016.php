<?php
include('../conf/session.php');
include('../conf/conf.php');
include('../conf/fucts.php');
include('mailTemplate.php');

function getDateDMYFormat($inputDate) {
    $datearr = explode("/", $inputDate);
    $dd = $datearr[1];
    $mm = $datearr[0];
    $yy = $datearr[2];
    return $dd . "/" . $mm . "/" . $yy;
}

/**
 * Mail Employee. To create mail list of uploaded employees.
 * @param array $empList
 */
function mailEmployeeLink($empList) {   
    global $webRoot;
    global $template;
    if ($empList != array()) {
//        $folderPath = "employeeMail";
//        $filename = "employeeMailList.csv";
//        $target_path = $folderPath . DIRECTORY_SEPARATOR . $filename;
        $companyId = null;
        foreach ($empList as $emp) {
            $empDetail = explode(":", $emp);
            $to = $empDetail[0];
            $empNo = $empDetail[2];
            $companyId = $empDetail[3];
            $corporateId = $empDetail[7];
            $subject = 'Employee Access';
            $message = 'Employee Message';
            $employeeUrl = base64_encode($empDetail[0] . ":" . $empDetail[1] . ":" . $empDetail[2] . ":" . $empDetail[3] . ":" . $empDetail[4] . ":" . $empDetail[5]);

            //if ($corporateId == '50105910') {
            if(in_array($corporateId, array('54213260', '53477368'))) {
                $empUrl = $webRoot . "login.php?u=" . $employeeUrl;
            } else {
                $empUrl = $webRoot . "company_" . $employeeUrl;
            }
            $mailDate = time();

            $formattedTemplate = str_replace("##Name", $empDetail[1], $template);
            $formattedTemplate = str_replace("##companyname", $empDetail[6], $formattedTemplate);
            $formattedTemplate = str_replace("##employeetopupplanlink", $empUrl, $formattedTemplate);
            $formattedTemplate = str_replace("##date", date('M d,Y', $empDetail[5]), $formattedTemplate);
            $headers = "From: enrollment@religare.com \r\n" .
                    'MIME-Version: 1.0' . "\r\n" .
                    'Content-type: text/html; charset=iso-8859-1';
            $status = "Success";
            if (isset($_POST['linksendoption']) && $_POST['linksendoption'] == 'manual') {
                $status = "Manual";
            } else {
                if (!mail($to, $subject, $formattedTemplate, $headers)) {
                    $status = "Mail Error";
                }
            }

            $sql = "INSERT INTO `tbl_employee_link` (`empNo`,`mailTo`,`mailSubject`,`mailContent` ,`mailDate`,`mailType`,`status`,`company_name`)VALUES ('$empNo', '$to','$subject','$empUrl','$mailDate','1','$status','$empDetail[6]')";
            mysql_query($sql);
        }
//        $sqlComp        = mysql_query("SELECT `rrmName`, `rrmEmail` from `tbl_company` WHERE `id`=$companyId");    
//        $companyDetail  = mysql_fetch_assoc($sqlComp);
//        $rrmName    = $companyDetail['rrmName'];
//        $rrmEmail    = $companyDetail['rrmEmail'];
//        
//        $empList[] = "". "$rrmEmail : $rrmName : $companyId". "" ;
//        $out = fopen($target_path, 'a');   
//        fputcsv($out, $empList);
//        fclose($out);
    }
}

function mailEmployeeLink_02Sept2015($empList) {
    if ($empList != array()) {
        /*
          $corporateDetail = fetchListByColumnName('CORPORATENAME,CORPORATEUNIQUECODE', 'CRMCORPORATE', "where CORPORATEID='" . @$corporate_id . "'");

          $res = fetchListById("CRMTOPUPPLANEMPLOYEE", "TOPUPPLANEMPLOYEEID", $insert_id);
          $stpenddate = $res[0]['ST_WP_END_DATE'] ? strtotime($res[0]['ST_WP_END_DATE']) : 0;
          $prevenddate = $res[0]['P_WP_END_DATE'] ? strtotime($res[0]['P_WP_END_DATE']) : 0;
          $ppenddate = $res[0]['PP_WP_END_DATE'] ? strtotime($res[0]['PP_WP_END_DATE']) : 0;
          $opdenddate = $res[0]['OPD_END_DATE'] ? strtotime($res[0]['OPD_END_DATE']) : 0;
          $enddatearray = array($stpenddate, $prevenddate, $ppenddate, $opdenddate);

          $resnew = array();
          foreach ($enddatearray as $key => $value)
          {
          if ($value != 0)
          {
          $resnew[] = $value;
          }
          }

          $enddatearray = min($resnew);
          $replacedate = date('d-M-Y', $enddatearray);
         */
        foreach ($empList as $emp) {
            $empDetail = explode("-", $emp);

            $to = $empDetail[1];
            $Mailtemplates = ""; //GetTemplateContentByTemplateName('XOL_EMPLOYEE_TOPUP_LINK');
            $subject = stripslashes('Employee Access');
            $message = stripslashes('Employee Message');
            //$message = str_replace("##Name", @$email, $message);
            $employeeUrl = base64_encode($empDetail[0] . ":" . $empDetail[1] . ":" . $empDetail[2] . ":" . $empDetail[3] . ":" . $empDetail[4] . ":" . $empDetail[5]);
            $empUrl = "/company_" . $employeeUrl;
            //$message = str_replace("##employeetopupplanlink", @$EMPURL, $message);
            //$message = str_replace("##companyname", @$corporateDetail[0]['CORPORATENAME'], $message);
            //$message = str_replace("##date", @$replacedate, $message);
            $headers = 'From:' . stripslashes($empDetail[2]) . ' <' . stripslashes($empDetail[1]) . "> \r\n" .
                    'MIME-Version: 1.0' . "\r\n" .
                    'Content-type: text/html; charset=iso-8859-1';
            //if (mail($to, $subject, $message, $headers))
            {
                $to = addslashes($to);
                $subject = addslashes($subject);
                $message = addslashes($message);
                $mailDate = time();
                $sql = "INSERT INTO `tbl_employee_link` (`mailTo`,`mailSubject`,`mailContent` ,`mailDate`,`mailType`)VALUES ('$to','$subject','$empUrl','$mailDate','1')";
                mysql_query($sql);
            }
        }
    }
}

$prefix = substr(time(), 5, 5);
$companyId = sanitize_data(@$_REQUEST['companyId']);

$rs_tblsite = mysql_query("select `corporateid`,`companyName`,`urlStatus`,`urlExpiryDate`,`offerSumInsured`,`insuranceType`,`policyType`,`dependent` from `tbl_company` where `id` = '$companyId' ");
$array_tblsite = mysql_fetch_array($rs_tblsite);
$corporateId = $array_tblsite['corporateid'];
$companyName = $array_tblsite['companyName'];
$urlStatus = $array_tblsite['urlStatus'];
$urlExpiryDate = $array_tblsite['urlExpiryDate'];
$offerSumInsured = $array_tblsite['offerSumInsured'];
$insuranceType = $array_tblsite['insuranceType'];
$policyType = $array_tblsite['policyType'];
$dependent = $array_tblsite['dependent'];
$insertedEmployee = array();
$final_arr = unserialize(urldecode($_POST["final_arr"]));

if (@$firstColumn == '1') {
    $i = '1';
} else {
    $i = '0';
}
$mailDate = time();
for ($i = $i; $i < sizeof($final_arr); $i++) {
    $empId = '';
    $empNo = $final_arr[$i]['empNo'];
    $empEmail = $final_arr[$i]['empEmail'];
    $empSalutation = $final_arr[$i]['empSalutation'];
    $empFirstName = $final_arr[$i]['empFirstName'];
    $empLastName = $final_arr[$i]['empLastName'];
    $grade = $final_arr[$i]['grade'];
    $gender = $final_arr[$i]['gender'];
    $dob = $final_arr[$i]['dob'];
    $age = $final_arr[$i]['age'];
    $relationshipType = $final_arr[$i]['relationshipType'];
    $sumInsured = $final_arr[$i]['sumInsured'];
    $nomineeName = $final_arr[$i]['nomineeName'];
    $relationNomEmp = $final_arr[$i]['relationNomEmp'];
    $dob = getDateDMYFormat($final_arr[$i]['dob']);
//		$datearr= explode("/",$dob);
//		$dd		= $datearr[0];
//		$mm		= $datearr[1];
//		$yy		= $datearr[2];
//		//2013-03-27
//		$dob	= $yy."-".$mm."-".$dd; 
    $dob = strtotime($dob);
    $sumInsured = str_replace(',', '', $sumInsured);
    $sumInsuredId = chkSumInsured($sumInsured);
    $gradeId = chkGrade($grade, $companyId);
    $enrolStartDate = getDateDMYFormat($final_arr[$i]['enrolStartDate']);
    $enrolStartDate = strtotime($enrolStartDate);
    $enrolEndDate = getDateDMYFormat($final_arr[$i]['enrolEndDate']);
    $enrolEndDate = strtotime($enrolEndDate);

    if (ucfirst($relationshipType) == 'Self') {
        $sql = "INSERT INTO `tbl_company_employee` (`companyId`,`companyName`,`empNo`,`empEmail` ,`empSalutation`,`empFirstName`,`empLastName`,`empGrade`,`gender`,`dob`,`age`,`relationType`,`sumInsuredId`,`sumInsured`,`nomineeName`,`relationNomEmp` ,`urlExpiryDate`,`insuranceType`,`policyType`,`enrolStartDate`, `enrolEndDate`,`createdOn` ,`createdBy` ,`updatedOn` ,`updatedBy`)VALUES ('$companyId','$companyName','$empNo','$empEmail','$empSalutation','$empFirstName','$empLastName','$gradeId','$gender','$dob','$age','$relationshipType','$sumInsuredId','$sumInsured','$nomineeName','$relationNomEmp','$urlExpiryDate','$insuranceType','$policyType', $enrolStartDate, $enrolEndDate, unix_timestamp() ,'admin', unix_timestamp(), 'admin')";

        if (mysql_query($sql)) {
            $insertedEmployee[$i] = $empEmail . ":" . $empFirstName . ":" . $empNo . ":" . $companyId . ":" . $enrolStartDate . ":" . $enrolEndDate . ":" . $companyName . ":" . $corporateId;
        } else {
            $sql = "INSERT INTO `tbl_employee_link` (`empNo`,`mailTo`,`mailSubject`,`mailContent` ,`mailDate`,`mailType`,`status`,`company_name`,`error_detail`)VALUES ('$empNo', '$empEmail','','','$mailDate','1','Insert Error','$companyName','" . mysql_error() . "')";
            mysql_query($sql);
        }
    } else {
        if ($dependent == 'allowed') {
            $empId = getEmpId($empNo, $companyId);
            $sql = "INSERT INTO `tbl_employee_family` (`companyId`,`companyName`,`empId`,`empNo`,`relation` ,`salutation`,`firstName`,`lastName`,`fdob`,`sumInsuredId`,`insuranceType`,`policyType`, `enrolStartDate`, `enrolEndDate`, `createdOn` ,`createdBy` ,`updatedOn` ,`updatedBy`)VALUES ('$companyId','$companyName','$empId','$empNo','$relationshipType','$empSalutation','$empFirstName','$empLastName','$dob','$sumInsured','$insuranceType','$policyType', '$enrolStartDate', '$enrolEndDate', unix_timestamp() ,'admin', unix_timestamp(), 'admin')";
            mysql_query($sql);
        }
    }
}
/* Send mail: mailEmployeeLink */
mailEmployeeLink($insertedEmployee);

$sql1 = "select `id`,`empNo` from `tbl_employee_family` where `isDeleted` = 'no' AND `companyId` = '$companyId' AND `empId`<'1'";
$result1 = mysql_query($sql1);
while ($row1 = mysql_fetch_assoc($result1)) {
    $empfamilyid = $row1['id'];
    $employeeNo = $row1['empNo'];
    $empId = getEmpId($employeeNo, $companyId);
    $updatesql = "UPDATE `tbl_employee_family` SET `empId` = '$empId' WHERE `id` = '$empfamilyid' ";
    mysql_query($updatesql);
}
$sql2 = "select `id`,`sumInsuredId`,`dob`,`empGrade` from `tbl_company_employee` where `isDeleted` = 'no' AND `companyId` = '$companyId' AND `empPremium`<'1'";
$result2 = mysql_query($sql2);
while ($row2 = mysql_fetch_assoc($result2)) {
    $totalPremium = '';
    $empid = $row2['id'];
    $empSiId = $row2['sumInsuredId'];
    $gradeId = $row2['empGrade'];
    $dob = $row2['dob'];
    $gradeId = $row2['empGrade'];
    $empAge = calculateAge($dob);
    $empAgeGroupId = getAgeGroupId($empAge);

    if ($offerSumInsured == 'grade') {
        $getempgradearr = getEmpGradePremium(@$companyId, @$gradeId);
        $gradesiid = $getempgradearr[0]['sumInsuredId'];
        $gradesi = $getempgradearr[0]['siAmount'];

        if ($policyType == 'fixed') {
            $gradepremium = getGradeSIMapData($gradeId, $gradesiid, @$companyId);
        } else {
            $gradepremium = getGradeSAMappingData($empAgeGroupId, $gradesiid, $companyId);
        }
        $employeesiid = $gradesiid;
        $employeepremium = $gradepremium;
    } else {
        if ($policyType == 'fixed') {
            $genpremium = getAgeSIMapData($empSiId, $companyId);
        } else {
            $genpremium = getSumInsuredPremium($companyId, $empSiId, $empAgeGroupId);
        }
        $employeesiid = $empSiId;
        $employeepremium = $genpremium;
    }
    $totalPremium = $employeepremium;
    if ($insuranceType == 'individual') {
        $sql3 = "select `id`,`fdob` from `tbl_employee_family` where `isDeleted` = 'no' AND `companyId` = '$companyId' AND `empId` = '$empid' ";
        $result3 = mysql_query($sql3);
        while ($row3 = mysql_fetch_assoc($result3)) {
            $fid = $row3['id'];
            $fdob = $row3['fdob'];
            $memAge = calculateAge($fdob);
            $memAgeGroupId = getAgeGroupId($memAge);

            if ($offerSumInsured == 'grade') {
                $memsiid = $employeesiid;
                if ($policyType == 'fixed') {
                    $mempremium = $employeepremium;
                } else {
                    $mempremium = getGradeSAMappingData($memAgeGroupId, $memsiid, $companyId);
                }
                $familysiid = $memsiid;
                $familypremium = $mempremium;
            } else {
                $memsiid = $employeesiid;
                if ($policyType == 'fixed') {
                    $mempremium = $employeepremium;
                } else {
                    $mempremium = getSumInsuredPremium($companyId, $memsiid, $memAgeGroupId);
                }
                $familysiid = $memsiid;
                $familypremium = $mempremium;
            }
            $updatesql = "UPDATE `tbl_employee_family` SET `sumInsuredId` = '$familysiid',`sumInsuredPremium` = '$familypremium' WHERE `id` = '$fid' AND `companyId` = '$companyId' ";
            mysql_query($updatesql);
            $totalPremium = $totalPremium + $familypremium;
        }
    }
    $updatesql = "UPDATE `tbl_company_employee` SET `sumInsuredId` = '$employeesiid',`empPremium` = '$employeepremium',`totalPremium` = '$totalPremium' WHERE `id` = '$empid' AND `companyId` = '$companyId' ";
    mysql_query($updatesql);
}
?>
<script>
    document.location = "addCorporate.php?companyId=<?= $companyId; ?>&mode=Edit&empmsg=Employee data saved successfully.#tabs-8"
</script>