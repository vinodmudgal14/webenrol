<?php  
include('../conf/session.php');
include('../conf/conf.php');
include('../conf/fucts.php');

function saveImage($logoImage, $target_path)
{
    $isUploaded     = 0;
    $imageFileType  = pathinfo($logoImage,PATHINFO_EXTENSION);    
    
    if(in_array($imageFileType, array("png", "jpg", "jpeg", "gif")))
    {
        $isUploaded = move_uploaded_file(@$_FILES["logoFile"]["tmp_name"], $target_path.$logoImage);
        chmod($target_path,0777);         
        if($isUploaded)
        {
            deletePreviousImage($target_path, $id);
        }
    }    
    return $isUploaded;
}

function deletePreviousImage($target_path, $id)
{
    $sqlLogoFile = mysql_query("select `logoFile` from `tbl_company` WHERE `id` = '$id'");
    $logoFileArray = mysql_fetch_assoc($sqlLogoFile);
    if(@$logoFileArray['logoFile'])
    {
        unlink($target_path.$logoFileArray['logoFile']);
    }  
}
$prefix=substr(time(),5,5);

$id                 =	sanitize_data(@$_REQUEST['id']);
$corporateid		=	sanitize_data(@$_REQUEST['corporateid']);
$mode				=	sanitize_data(@$_REQUEST['mode']);
$companyId			=	sanitize_data(@$_REQUEST['companyId']);
$companyName		=	sanitize_data(@$_REQUEST['companyName']);
$contactPersonName	=	sanitize_data(@$_REQUEST['contactPersonName']);
$contactPersonEmail	=	sanitize_data_email(@$_REQUEST['contactPersonEmail']);
$contactPersonMobile=	sanitize_data(@$_REQUEST['contactPersonMobile']);
$rrmName			=	sanitize_data(@$_REQUEST['rrmName']);
$rrmEmail			=	sanitize_data_email(@$_REQUEST['rrmEmail']);
$rrmMobile			=	sanitize_data(@$_REQUEST['rrmMobile']);
$address			=	sanitize_data(@$_REQUEST['address']);
$url                =	sanitize_data(@$_REQUEST['url']);
$urlStatus			=	sanitize_data(@$_REQUEST['urlStatus']);
$urlExpiryDate		=	sanitize_data(@$_REQUEST['urlExpiryDate']);
$policyStartDate    =   sanitize_data(@$_REQUEST['policyStartDate']);
$policyEndDate      =   sanitize_data(@$_REQUEST['policyEndDate']);
$isEditPersonalDetails          = sanitize_data(@$_REQUEST['personalDetails']);
$isEditFamilyDetails            = sanitize_data(@$_REQUEST['familyDetails']);
$proRata            =   sanitize_data(@$_REQUEST['proRata']);
$setDefaultTheme    =   sanitize_data(@$_REQUEST['defaultTheme']);
//echo $setDefaultTheme;
$defaultTheme       =   @$setDefaultTheme ? '1' : '0';
$religareLogo       =   sanitize_data(@$_REQUEST['religareLogo']);
$themeColor         =   sanitize_data(@$_REQUEST['themeColor']);
$menuColor          =   sanitize_data(@$_REQUEST['menuColor']);
$menuBgColor        =   sanitize_data(@$_REQUEST['menuBgColor']);
$headingColor       =   sanitize_data(@$_REQUEST['headingColor']);
$headingBgColor     =   sanitize_data(@$_REQUEST['headingBgColor']);
$buttonColor        =   sanitize_data(@$_REQUEST['buttonColor']);
$buttonBgColor      =   sanitize_data(@$_REQUEST['buttonBgColor']);

$reminderFrequency      =   sanitize_data(@$_REQUEST['reminderFrequency']);
        
$logoFile           =   sanitize_data(@$_FILES['logoFile']['name']);

$policyStartDate = strtotime($policyStartDate);
$policyEndDate = strtotime($policyEndDate);
$urlExpiryDate = strtotime($urlExpiryDate);
if($mode=="Edit"){
                    $isUploaded     = 1;
                    $target_path    = "..\..\imagetest".DIRECTORY_SEPARATOR;
                    
                    if(sanitize_data(@$_REQUEST['religareLogo'])==2)
                    {
                        if(@$_FILES["logoFile"]["name"])
                        { 
                            $logoImage      = time()."_".sanitize_data(@$_FILES["logoFile"]["name"]);
                            $isUploaded     = saveImage($logoImage, $target_path, $id);
                            deletePreviousImage($target_path, $id);
                        }
                        else
                        { 
                            $sqlReligareLogo = mysql_query("select `logoFile`, `religareLogo` from `tbl_company` WHERE `id` = '$companyId'");
                            $religareLogoName = mysql_fetch_assoc($sqlReligareLogo);
                            $religareLogo = 1;
                            if(sanitize_data(@$_REQUEST['religareLogo'])==$religareLogoName['religareLogo'])
                            {  
                                $religareLogo = 2;
                                $logoImage = $religareLogoName['logoFile'];
                            }
                        }
                    }
                    else
                    {
                        deletePreviousImage($target_path, $companyId);
                    }
                    if($isUploaded){
                        //$logoImage = $createdTime.$logoImage
			//  $sql = sprintf("UPDATE `tbl_company` SET `corporateid`='%s',`companyName`='%s',`contactPersonName`='%s',`contactPersonEmail`='%s',`contactPersonMobile`='%s',`rrmName`='%s',`rrmEmail`='%s',`rrmMobile`='%s',`address`='%s',`url`='%s',`urlStatus`='%s',`urlExpiryDate`='%s',`policyStartDate`='%s',`policyEndDate`='%s',`reminderFrequency`='%s',`proRata`='%s',`religareLogo`='%s',`defaultTheme`='$defaultTheme',`themeColor`='%s', `menuColor`='%s', `menuBgColor`='%s', `headingColor`='%s', `headingBgColor`='%s', `buttonColor`='%s', `buttonBgColor`='%s',`logoFile`='$logoImage',`updatedOn`=unix_timestamp(),`updatedBy` = '%s' where `id` = '$companyId' LIMIT 1",
                        $sql = sprintf("UPDATE `tbl_company` SET `corporateid`='%s',`companyName`='%s',`contactPersonName`='%s',`contactPersonEmail`='%s',`contactPersonMobile`='%s',`rrmName`='%s',`rrmEmail`='%s',`rrmMobile`='%s',`address`='%s',`url`='%s',`urlStatus`='%s',`urlExpiryDate`='%s',`policyStartDate`='%s',`policyEndDate`='%s',`reminderFrequency`='%s',`personalDetails` = '%s',`familyDetails` = '%s',`proRata`='%s',`religareLogo`='%s',`defaultTheme`='$defaultTheme',`themeColor`='%s', `menuColor`='%s', `menuBgColor`='%s', `headingColor`='%s', `headingBgColor`='%s', `buttonColor`='%s', `buttonBgColor`='%s',`logoFile`='$logoImage',`updatedOn`=unix_timestamp(),`updatedBy` = '%s' where `id` = '$companyId' LIMIT 1",
			 	mysql_real_escape_string(@$corporateid),
				mysql_real_escape_string(@$companyName),
				mysql_real_escape_string(@$contactPersonName),
				mysql_real_escape_string(@$contactPersonEmail),
				mysql_real_escape_string(@$contactPersonMobile),
				mysql_real_escape_string(@$rrmName),
				mysql_real_escape_string(@$rrmEmail),
				mysql_real_escape_string(@$rrmMobile),				
				mysql_real_escape_string(@$address),
				mysql_real_escape_string(@$url),
				mysql_real_escape_string(@$urlStatus),
				mysql_real_escape_string(@$urlExpiryDate),
                                mysql_real_escape_string(@$policyStartDate),
                                mysql_real_escape_string(@$policyEndDate),
                                mysql_real_escape_string(@$reminderFrequency),
                                mysql_real_escape_string(@$isEditPersonalDetails),
                                mysql_real_escape_string(@$isEditFamilyDetails),
                                mysql_real_escape_string(@$proRata),
                                //mysql_real_escape_string(@$defaultTheme),
                                mysql_real_escape_string(@$religareLogo),
                                mysql_real_escape_string(@$themeColor),
                                mysql_real_escape_string(@$menuColor),
                                mysql_real_escape_string(@$menuBgColor),
                                mysql_real_escape_string(@$headingColor),
                                mysql_real_escape_string(@$headingBgColor),
                                mysql_real_escape_string(@$buttonColor),
                                mysql_real_escape_string(@$buttonBgColor),
                                mysql_real_escape_string(@$logoFile),
		 		mysql_real_escape_string(@$_SESSION['username']),
			 	mysql_real_escape_string(@$_SESSION['username']));
			 	$result=mysql_query("$sql");				
                    }
                    $compId = $companyId;
		}	
                else	{
		$sql=mysql_query("select `id` from `tbl_company` WHERE `url` = '$url' AND `isDeleted` = 'no' ");
		$result = mysql_num_rows($sql);
		if($result<1){
		  $query= sprintf("INSERT into `tbl_company` (`corporateid`,`companyName`,`contactPersonName`,`contactPersonEmail`,`contactPersonMobile`,`rrmName`,`rrmEmail`,`rrmMobile`,`address`,`url`,`urlStatus`,`urlExpiryDate`,`policyStartDate`,`policyEndDate`,`reminderFrequency`,`personalDetails`,`familyDetails`,`proRata`,`religareLogo`,`defaultTheme`,`themeColor`,`menuColor`,`menuBgColor`, `headingColor`, `headingBgColor`, `buttonColor`, `buttonBgColor`,`createdOn`,`createdBy`,`updatedOn`,`updatedBy`)
 VALUES
 ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',unix_timestamp(),'%s',unix_timestamp(),'%s')",
 		mysql_real_escape_string(@$corporateid),
		mysql_real_escape_string(@$companyName),
		mysql_real_escape_string(@$contactPersonName),
		mysql_real_escape_string(@$contactPersonEmail),
		mysql_real_escape_string(@$contactPersonMobile),
		mysql_real_escape_string(@$rrmName),
		mysql_real_escape_string(@$rrmEmail),
		mysql_real_escape_string(@$rrmMobile),				
		mysql_real_escape_string(@$address),
		mysql_real_escape_string(@$url),
		mysql_real_escape_string(@$urlStatus),
		mysql_real_escape_string(@$urlExpiryDate),
        mysql_real_escape_string(@$policyStartDate),
        mysql_real_escape_string(@$policyEndDate),
        mysql_real_escape_string(@$reminderFrequency),
                          mysql_real_escape_string(@$isEditPersonalDetails),
                          mysql_real_escape_string(@$isEditFamilyDetails),
        mysql_real_escape_string(@$proRata),
        mysql_real_escape_string(@$religareLogo),
        mysql_real_escape_string(@$defaultTheme),
        mysql_real_escape_string(@$themeColor),
        mysql_real_escape_string(@$menuColor),
        mysql_real_escape_string(@$menuBgColor),
        mysql_real_escape_string(@$headingColor),
        mysql_real_escape_string(@$headingBgColor),  
                          mysql_real_escape_string(@$buttonColor),
                          mysql_real_escape_string(@$buttonBgColor),
		mysql_real_escape_string(@$_SESSION['username']),
		mysql_real_escape_string(@$_SESSION['username']));
   
	     $result=mysql_query("$query");
		 $compId = mysql_insert_id();
			} else {	?>
<script type="text/javascript">
document.location="addCorporate.php&successmsg=Company URL already exist.";
</script>
<?php		}
		}
?>
<script type="text/javascript">
//document.location="addCorporate.php?companyId=<?=$compId;?>&mode=Edit&successmsg=Information saved successfully#tabs-1";
document.location="familyMembersList.php?companyId=<?=$compId;?>&successmsg=Company Information saved successfully";
</script>