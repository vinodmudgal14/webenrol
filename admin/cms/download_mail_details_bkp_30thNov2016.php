<?php
include('../conf/session.php');
include('../conf/conf.php');
include 'PHPExcel.php';
/** PHPExcel_Writer_Excel2007 */
include 'PHPExcel/Writer/Excel5.php';
// Create new PHPExcel object

$sql = "select `empl`.`empNo`,`empl`.`mailTo`,from_unixtime(`empl`.`mailDate`) as mailDate,`empl`.`status`,`empl`.`company_name`,`comp`.`id`,CAST(`empl`.`mailContent` AS CHAR(10000) CHARACTER SET utf8) as EmpLink from `tbl_employee_link` as `empl` left join `tbl_company` as `comp` on `empl`.`company_name`=`comp`.`companyName` WHERE";

if (!empty($_GET["status"])) {
    switch ($_GET["status"]){
        case "1": //uploaded
            break;
        case "2": //Inserted
            $sql.="(`empl`.`status` = 'Success' OR `empl`.`status` = 'Mail Error')";
            break;
        case "4": //Mail Error
            $sql.="`empl`.`status` = 'Mail Error'";
            break;
        case "5": //Not Inserted
            $sql.="`empl`.`status` = 'Insert Error'";
            break;
        case "6": //Manual triggered
            $sql.="`empl`.`status` = 'Manual'";
            break;
        default:
            $sql.="`empl`.`status` = 'Success'";
            break;
    }
} else {
    $sql.="`empl`.`status` = 'Success'";
}

if (!empty($_GET["date_start"]) && !empty($_GET["date_end"])) {
    $sql .= "AND from_unixtime(`empl`.`mailDate`) BETWEEN '" . $_GET["date_start"] . "' AND '" . $_GET["date_end"] . "'";
} else {
    $sql .= " AND from_unixtime(`empl`.`mailDate`) BETWEEN concat_ws('-',year(now()),month(now()),1) AND now()";
}
if (!empty($_GET['mail_to'])) {
    $sql .= " AND `empl`.`mailTo` LIKE '%" . $_GET['mail_to'] . "%'";
}
if (!empty($_GET['company_name'])) {
    $sql .= " AND `empl`.`company_name` LIKE '%" . $_GET['company_name']. "%'";
}
if (!empty($_GET['employee_no'])) {
    $sql .= " AND `empl`.`empNo` LIKE '%" . $_GET['employee_no'] . "%'";
}
if (!empty($_GET['corporate_id'])) {
    $sql .= " AND `comp`.`corporateid` = " . $_GET['corporate_id'] . "";
}
$query = mysql_query($sql);
//$query1 = fbsql_set_lob_mode($query, FBSQL_LOB_HANDLE);
if (!empty($_GET['status']) && $_GET['status']=='6') {
   $total_excelColumns = getNameFromNumber(4);

$objPHPExcel = new PHPExcel();

// Set properties
$objPHPExcel->getProperties()->setTitle("Mail Details");
$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(25);
$objPHPExcel->getActiveSheet()->getRowDimension(2)->setRowHeight(20);
// Set appointment_list height
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
// Set column width
$objPHPExcel->getActiveSheet()->duplicateStyleArray(
        array(
    'font' => array(
        'name' => 'Times New Roman',
        'bold' => true,
        'italic' => false,
        'size' => 12
    ),
    'borders' => array(
        'top' => array('style' => PHPExcel_Style_Border::BORDER_DOUBLE),
        'bottom' => array('style' => PHPExcel_Style_Border::BORDER_DOUBLE),
        'left' => array('style' => PHPExcel_Style_Border::BORDER_DOUBLE),
        'right' => array('style' => PHPExcel_Style_Border::BORDER_DOUBLE)
    ),
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        'wrap' => true
    )
        ), 'A1:' . $total_excelColumns . '1'
);

// Add some data
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->SetCellValue('A1', "SR NO.");
$objPHPExcel->getActiveSheet()->SetCellValue('B1', "COMPANY NAME");
$objPHPExcel->getActiveSheet()->SetCellValue('C1', "EMPLOYEE NAME");
$objPHPExcel->getActiveSheet()->SetCellValue('D1', "EMPLOYEE EMAIL");
$objPHPExcel->getActiveSheet()->SetCellValue('E1', "EMPLOYEE LINK");

$k = 2;
$f = 0;
$i = 1;

while ($mail_details = mysql_fetch_assoc($query)) {
    if (!empty($mail_details)) {
        $objPHPExcel->getActiveSheet()->SetCellValue('A' . $k, $i);
        $objPHPExcel->getActiveSheet()->SetCellValue('B' . $k, $mail_details["company_name"]);
        $objPHPExcel->getActiveSheet()->setCellValue('C' . $k, Employeename($mail_details["empNo"],$mail_details["id"]));
        $objPHPExcel->getActiveSheet()->setCellValue('D' . $k, $mail_details["mailTo"]);
        $objPHPExcel->getActiveSheet()->SetCellValue('E' . $k, $mail_details["EmpLink"]);
        
        $f++;
        $k++;
        $i++;
    }
    else {
        echo "Error saving file!";
    }
}
// Rename sheet

header('Content-Disposition: attachment;filename=Mail_details.xls ');
$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
$objWriter->save('php://output');
} else {
$total_excelColumns = getNameFromNumber(5);

$objPHPExcel = new PHPExcel();

// Set properties
$objPHPExcel->getProperties()->setTitle("Mail Details");
$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(25);
$objPHPExcel->getActiveSheet()->getRowDimension(2)->setRowHeight(20);
// Set appointment_list height
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
// Set column width
$objPHPExcel->getActiveSheet()->duplicateStyleArray(
        array(
    'font' => array(
        'name' => 'Times New Roman',
        'bold' => true,
        'italic' => false,
        'size' => 12
    ),
    'borders' => array(
        'top' => array('style' => PHPExcel_Style_Border::BORDER_DOUBLE),
        'bottom' => array('style' => PHPExcel_Style_Border::BORDER_DOUBLE),
        'left' => array('style' => PHPExcel_Style_Border::BORDER_DOUBLE),
        'right' => array('style' => PHPExcel_Style_Border::BORDER_DOUBLE)
    ),
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        'wrap' => true
    )
        ), 'A1:' . $total_excelColumns . '1'
);

// Add some data
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->SetCellValue('A1', "SR NO.");
$objPHPExcel->getActiveSheet()->SetCellValue('B1', "COMPANY NAME");
$objPHPExcel->getActiveSheet()->SetCellValue('C1', "EMPLOYEE NO.");
$objPHPExcel->getActiveSheet()->SetCellValue('D1', "RECEIVER");
$objPHPExcel->getActiveSheet()->SetCellValue('E1', "MAIL DATE");
$objPHPExcel->getActiveSheet()->SetCellValue('F1', "MAIL STATUS");

$k = 2;
$f = 0;
$i = 1;

while ($mail_details = mysql_fetch_assoc($query)) {
    if (!empty($mail_details)) {
        $objPHPExcel->getActiveSheet()->SetCellValue('A' . $k, $i);
        $objPHPExcel->getActiveSheet()->SetCellValue('B' . $k, $mail_details["company_name"]);
        $objPHPExcel->getActiveSheet()->setCellValue('C' . $k, $mail_details["empNo"]);
        $objPHPExcel->getActiveSheet()->SetCellValue('D' . $k, $mail_details["mailTo"]);
        $objPHPExcel->getActiveSheet()->SetCellValue('E' . $k, $mail_details["mailDate"]);
        $objPHPExcel->getActiveSheet()->SetCellValue('F' . $k, $mail_details["status"]);
        
        $f++;
        $k++;
        $i++;
    }
    else {
        echo "Error saving file!";
    }
}
// Rename sheet

header('Content-Disposition: attachment;filename=Mail_details.xls ');
$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
$objWriter->save('php://output');
}
function getNameFromNumber($num) {
    $numeric = $num % 26;
    $letter = chr(65 + $numeric);
    $num2 = intval($num / 26);
    if ($num2 > 0) {
        return getNameFromNumber($num2 - 1) . $letter;
    } else {
        return $letter;
    }
}
function Employeename($empNo,$companyid){
    $sql = "select `empFirstName`,`empLastName` from `tbl_company_employee` WHERE empNo = '$empNo' and companyid = '$companyid'";
    $query = mysql_query($sql);
    $empDetails = mysql_fetch_row($query);
    $empFirstName = $empDetails['0'];
    $empLastName = $empDetails['1'];
    $empName = $empFirstName.' '.$empLastName;
    return $empName;
}


?>

