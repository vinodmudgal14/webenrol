<?php
include('../inc/hd.php');

$sql = "select * from `tbl_family_member` where `isDeleted` = 'no' ";
$empFamilyDetails = mysqltoarray($sql);

$companyId = @$_REQUEST['companyId'];
$genderArray = array(1 => 'M', 2 => 'F', 3 => 'T');
?>

<html xmlns="http://www.w3.org/1999/xhtml">

    <div id="middle">
        <div class="middle-heading-bg">
            <h1>Manage Family Members</h1>
        </div> <!--middle heading bg-->
        <!--              <div class="middle-data" style="border-bottom:none;">
                        <table align="center" width="100%" cellpadding="0" cellspacing="0" border="0">
                          <tr>
                            <td align="center" class="col-border-1"><a href="addAgeGroup.php">Add Age Group</a></td>
                          </tr>
                        </table>
                        </div> middle data-->

        <div class="middle-data">
            <form name="familymembersform" id="familymembersform" method="post" action="updateFamilyMembers.php">	
                <?php if (@$_REQUEST['successmsg'] != '') { ?>	
                    <div style="padding-bottom:5px; text-align:center; color:red;">
                        <?php echo sanitize_data($_REQUEST['successmsg']); ?></div>
                <?php } ?>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="display" id="example">
                    <thead>
                        <tr>
                            <th width="8%" align="center" class="col-border-1">Sr No.</th>
                            <th width="12%" align="center" class="col-border-1">Gender</th>
                            <th width="80%" align="center" class="col-border-1">Family Members (included in policy)</th>		
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $s = 0;
                        while ($s < 3) {
                            $s++;
                            if ($genderArray[$s] == 'M') {
                                $gender = 'Male';
                            } else if ($genderArray[$s] == 'F') {
                                $gender = 'Female';
                            } else {
                                $gender = 'Trans Gender';
                            }
                            ?>

                            <tr class="gradeX">		
                                <td align="center"  class="col-border-1"><?php echo $s; ?></td>			
                                <td align="left"  class="col-border-1"><?php echo $gender; ?></td>	            
                                <td align="left" class="col-border-1">
                            <?php $n = 0;
                            while ($n < count($empFamilyDetails)) { ?>

                                        <input type="hidden" name="familymembers[]" value="<?= $empFamilyDetails[$n]['id']; ?>" />
                                        <input type="checkbox" name="memberChk_<?= $empFamilyDetails[$n]['id']; ?>_<?= $s; ?>" id="memberChk_<?= $empFamilyDetails[$n]['id']; ?>_<?= $s; ?>" <?php if (chkMembers($companyId, $empFamilyDetails[$n]['id'], $genderArray[$s]) > 0) {
                            echo 'checked';
                        } ?>/>&nbsp;&nbsp;<?= $empFamilyDetails[$n]['member']; ?>
        <?php $n++;
    } ?>                
                                </td>            	
                                <!--<td align="center" class="col-border-1 edit-delete"><a href="addAgeGroup.php?id=<?php //echo $agegrouplist[$s]['id'] ?>&mode=Edit" class="edit" style="float:left;"></a><a onclick="return confirm('Are you really want to delete?');" href="ageGroupList.php?id=<?php //echo $agegrouplist[$s]['id'] ?>&mode=Delete" class="delete" ></a></td>-->		
                            </tr>
    <?php } ?>
                        <input type="hidden" name="companyId" value="<?= $companyId; ?>" />
                    </tbody>	
                    <tr>
                        <td><a href="addCorporate.php?companyId=<?= $companyId; ?>&mode=Edit"><input type="button" value="Back"></td>
                        <td colspan="2" class="col-border_event" align="right">
                            <input type="submit" value="Submit"/>
                        </td>
                    </tr>	
                </table>
            </form>
        </div> <!--middle data-->
    </div><div style="clear:both"></div> <!--middle-->
<?php include('../inc/ft.php'); ?>
</body>
</html>
