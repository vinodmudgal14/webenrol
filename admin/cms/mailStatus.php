<?php
include_once('../inc/hd.php');

$sql = "select `empl`.`empNo`,`empl`.`mailTo`,from_unixtime(`empl`.`mailDate`) as mailDate,`empl`.`status`,`empl`.`company_name` from `tbl_employee_link` as `empl` left join `tbl_company` as `comp` on `empl`.`company_name`=`comp`.`companyName` WHERE";
//$sql = "select `empl`.`empNo`,`empl`.`mailTo`,from_unixtime(`empl`.`mailDate`) as mailDate,`empl`.`status`,`empl`.`company_name`,CAST(`empl`.`mailContent` AS CHAR(10000) CHARACTER SET utf8) as EmpLink from `tbl_employee_link` as `empl` left join `tbl_company` as `comp` on `empl`.`company_name`=`comp`.`companyName` WHERE";
if (!empty($_GET["status"])) {
    switch ($_GET["status"]){
        case "1": //uploaded
            break;
        case "2": //Inserted
            $sql.="(`empl`.`status` = 'Success' OR `empl`.`status` = 'Mail Error')";
            break;
        case "4": //Mail Error
            $sql.="`empl`.`status` = 'Mail Error'";
            break;
        case "5": //Not Inserted
            $sql.="`empl`.`status` = 'Insert Error'";
            break;
        case "6": //Manual
            $sql.="`empl`.`status` = 'Manual'";
            break;
        default:
            $sql.="`empl`.`status` = 'Success'";
            break;
    }
} else {
    $sql.="`empl`.`status` = 'Success'";
}
if (!empty($_GET["date_start"]) && !empty($_GET["date_end"])) {
    $sql .= "AND from_unixtime(empl.mailDate) BETWEEN '" . $_GET["date_start"] . "' AND '" . $_GET["date_end"] . "'";
} else {
    $sql .= " AND from_unixtime(empl.mailDate) BETWEEN concat_ws('-',year(now()),month(now()),1) AND now()";
}
if (!empty($_GET['mail_to'])) {
    $sql .= " AND `empl`.`mailTo` LIKE '%" . $_GET['mail_to'] . "%'";
}
if (!empty($_GET['company_name'])) {
    $sql .= " AND `empl`.`company_name` LIKE '%" . $_GET['company_name']. "%'";
}
if (!empty($_GET['corporate_id'])) {
    $sql .= " AND `comp`.`corporateid` = '". $_GET['corporate_id']. "'";
}
if (!empty($_GET['employee_no'])) {
    $sql .= " AND `empl`.`empNo` LIKE '%" . $_GET['employee_no'] . "%'";
}
$query = mysql_query($sql);
?><script type="text/javascript" src="<?= _WWWROOT; ?>/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<?= _WWWROOT; ?>/js/jquery-ui-1.8.18.custom.min.js"></script>
<script type="text/javascript" language="javascript" src="<?= _WWWROOT; ?>/js/data-jquery.js"></script>
<script type="text/javascript" language="javascript" src="<?= _WWWROOT; ?>/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?= _WWWROOT; ?>/js/dataTables.scrollingPagination1.js"></script>
<link rel="stylesheet" href="<?= _WWWROOT; ?>/css/colorpicker.css" type="text/css" />
<link type="text/css" href="<?= _WWWROOT; ?>/css/ui-lightness/jquery-ui-1.8.18.custom.css" rel="stylesheet" />
<script type="text/javascript" src="<?= _WWWROOT; ?>/js/ui.datepicker.js"></script>
<link href="<?= _WWWROOT; ?>/css/ui.datepicker.css" rel="stylesheet" type="text/css">
<script>
    $(document).ready(function () {
        $('#date_start, #date_end').datepicker({dateFormat: "yy-mm-dd"});
    });
</script>
<style>
    #filter th{float: left;}
</style>

<div id="middle">
    <div class="middle-heading-bg">
        <h1>Mail Details</h1>
    </div> <!--middle heading bg-->
    <div class="middle-data" style="border-bottom:none;">
        <table width="100%" cellpadding="0" cellspacing="0" border="0" id="filter" >
            <tr>
            <form method="GET" action="">
                <td  class="col-border-1" >
                    <table width="50%" style="float: left" >
                        <tr> 
                            <th>
                                Select Status :
                            </th>
                            <td>
                                <select name="status" style='width:62%;'>
                                    <option value="1" <?php if(isset($_GET['status'])) if($_GET['status'] == '1') echo 'selected="selected"'; ?>>Uploaded</option>
                                    <option value="2" <?php if(isset($_GET['status'])) if($_GET['status'] == '2') echo 'selected="selected"'; ?>>Inserted</option>
                                    <option value="3" <?php if(isset($_GET['status'])){ if($_GET['status'] == '3') echo 'selected="selected"'; } else echo 'selected="selected"'; ?>>Mail Sent / Success</option>
                                    <option value="4" <?php if(isset($_GET['status'])) if($_GET['status'] == '4') echo 'selected="selected"'; ?>>Inserted & Mail Not Sent</option>
                                    <option value="5" <?php if(isset($_GET['status'])) if($_GET['status'] == '5') echo 'selected="selected"'; ?>>Not Inserted</option>
                                    <option value="6" <?php if(isset($_GET['status'])) if($_GET['status'] == '6') echo 'selected="selected"'; ?>>Manual Triggered</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Start Date :
                            </th>
                            <td>
                                <input type="text" name="date_start" id="date_start" readonly="true" value="<?= isset($_GET['date_start']) && $_GET['date_start'] != '' ? $_GET['date_start'] : '' ?>"/>
                            </td>
                        <tr>
                        <tr>
                            <th>
                                Company Name :
                            </th>
                            <td>
                                <input type="text" name="company_name" value="<?= isset($_GET['company_name']) && $_GET['company_name'] != '' ? $_GET['company_name'] : '' ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Corporate Id :
                            </th>
                            <td>
                                <input type="text" name="corporate_id" value="<?= isset($_GET['corporate_id']) && $_GET['corporate_id'] != '' ? $_GET['corporate_id'] : '' ?>"/>
                            </td>
                        </tr>
                    </table>

<!--                    <table width="50%" style="float: right" border="1px solid #000">-->
                    <table width="50%" style="float: right" >
                        <tr>
                            <th>
                                Employee No.:
                            </th>
                            <td>
                                <input type="text" name="employee_no" value="<?= isset($_GET['employee_no']) && $_GET['employee_no'] != '' ? $_GET['employee_no'] : '' ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                End Date :
                            </th>
                            <td>
                                <input type="text" name="date_end" id="date_end" readonly="true" value="<?= isset($_GET['date_end']) && $_GET['date_end'] != '' ? $_GET['date_end'] : '' ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Receiver :
                            </th>
                            <td>
                                <input type="text" name="mail_to" value="<?= isset($_GET['mail_to']) && $_GET['mail_to'] != '' ? $_GET['mail_to'] : '' ?>"/> 
                            </td>
                            
                        </tr>
                        <tr>
                            <td>
                               
                            </td>
                            <td>
                                 <input type="submit" value="Search"> 
                                <a href="download_mail_details.php?status=<?php echo isset($_GET['status']) ? $_GET['status'] : ''; ?>&date_start=<?php echo isset($_GET['date_start']) ? $_GET['date_start'] : ''; ?>&company_name=<?php echo isset($_GET['company_name']) ? $_GET['company_name'] : ''; ?>&corporate_id=<?php echo isset($_GET['corporate_id']) ? $_GET['corporate_id'] : ''; ?>&employee_no=<?php echo isset($_GET['employee_no']) ? $_GET['employee_no'] : ''; ?>&date_end=<?php echo isset($_GET['date_end']) ? $_GET['date_end'] : ''; ?>&mail_to=<?php echo isset($_GET['mail_to']) ? $_GET['mail_to'] : ''; ?>">
                                    <img src="../images/xls-icon.png" title="Download Excel" width="30px" style="margin-bottom: -8px;float: right;"></img>
                                </a>
                            </td>
                        </tr>
                    </table>

                </td>
            </form>
            </tr>
        </table>
    </div> 
    <!--middle data-->

    <div class="middle-data">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="display" id="example">
            <thead>
                <tr>
                    <th width="5%" align="center" class="col-border-1">Sr No.</th>
                    <th width="15%" align="left" class="col-border-1">Company Name</th>
                    <th width="15%" align="left" class="col-border-1">Employee No.</th>
                    <th width="15%" align="left" class="col-border-1">Receiver</th>
                    <th width="15%" align="left" class="col-border-1">Mail Date</th>
                    <th width="15%" align="left" class="col-border-1">Mail Status</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 1;
                while ($mail_details = mysql_fetch_assoc($query)) {
                    if (!empty($mail_details)) {
                        ?>
                        <tr>
                            <td width = "5%" align = "center" class = "col-border-1"><?php echo $i; ?></td>
                            <td width = "15%" align = "left" class = "col-border-1"><?php echo $mail_details["company_name"]; ?></td>
                            <td width = "15%" align = "left" class = "col-border-1"><?php echo $mail_details["empNo"]; ?></td>
                            <td width = "15%" align = "left" class = "col-border-1"><?php echo $mail_details["mailTo"]; ?></td>
                            <td width = "15%" align = "left" class = "col-border-1"><?php echo $mail_details['mailDate']; ?></td>
                            <td width = "15%" align = "left" class = "col-border-1"><?php echo $mail_details["status"]; ?></td>
                        </tr>
                        <?php
                        $i++;
                    } else {
                        ?>
                        <tr><td colspan="6"><center>No records found</center></td></tr>
                    <?php
                }
            }
            ?>
            </tbody>
        </table>
    </div> <!--middle data-->
</div><div style = "clear:both"></div> <!--middle-->

<?php include_once('../inc/ft.php'); ?>
