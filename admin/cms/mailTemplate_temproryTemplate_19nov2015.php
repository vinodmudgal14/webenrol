<?php
echo $template = "
                    <META HTTP-EQUIV='Content-Type' CONTENT='text/html; charset=iso-8859-1'>
<html xmlns:v='urn:schemas-microsoft-com:vml' xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:w='urn:schemas-microsoft-com:office:word' xmlns:m='http://schemas.microsoft.com/office/2004/12/omml' xmlns='http://www.w3.org/TR/REC-html40'><head><meta name=Generator content='Microsoft Word 14 (filtered medium)'><style><!--
/* Font Definitions */
@font-face
  {font-family:Wingdings;
  panose-1:5 0 0 0 0 0 0 0 0 0;}
@font-face
  {font-family:Wingdings;
  panose-1:5 0 0 0 0 0 0 0 0 0;}
@font-face
  {font-family:Calibri;
  panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
  {font-family:Tahoma;
  panose-1:2 11 6 4 3 5 4 4 2 4;}
@font-face
  {font-family:'Arial Rounded MT Bold';
  panose-1:2 15 7 4 3 5 4 3 2 4;}
/* Style Definitions */
p.MsoNormal, li.MsoNormal, div.MsoNormal
  {margin-top:0cm;
  margin-right:0cm;
  margin-bottom:10.0pt;
  margin-left:0cm;
  line-height:115%;
  font-size:11.0pt;
  font-family:'Calibri','sans-serif';}
a:link, span.MsoHyperlink
  {mso-style-priority:99;
  color:blue;
  text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
  {mso-style-priority:99;
  color:purple;
  text-decoration:underline;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
  {mso-style-priority:99;
  mso-style-link:'Balloon Text Char';
  margin:0cm;
  margin-bottom:.0001pt;
  font-size:8.0pt;
  font-family:'Tahoma','sans-serif';}
p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
  {mso-style-priority:34;
  margin-top:0cm;
  margin-right:0cm;
  margin-bottom:10.0pt;
  margin-left:36.0pt;
  mso-add-space:auto;
  line-height:115%;
  font-size:11.0pt;
  font-family:'Calibri','sans-serif';}
p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst
  {mso-style-priority:34;
  mso-style-type:export-only;
  margin-top:0cm;
  margin-right:0cm;
  margin-bottom:0cm;
  margin-left:36.0pt;
  margin-bottom:.0001pt;
  mso-add-space:auto;
  line-height:115%;
  font-size:11.0pt;
  font-family:'Calibri','sans-serif';}
p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle
  {mso-style-priority:34;
  mso-style-type:export-only;
  margin-top:0cm;
  margin-right:0cm;
  margin-bottom:0cm;
  margin-left:36.0pt;
  margin-bottom:.0001pt;
  mso-add-space:auto;
  line-height:115%;
  font-size:11.0pt;
  font-family:'Calibri','sans-serif';}
p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast
  {mso-style-priority:34;
  mso-style-type:export-only;
  margin-top:0cm;
  margin-right:0cm;
  margin-bottom:10.0pt;
  margin-left:36.0pt;
  mso-add-space:auto;
  line-height:115%;
  font-size:11.0pt;
  font-family:'Calibri','sans-serif';}
span.BalloonTextChar
  {mso-style-name:'Balloon Text Char';
  mso-style-priority:99;
  mso-style-link:'Balloon Text';
  font-family:'Tahoma','sans-serif';}
span.EmailStyle20
  {mso-style-type:personal;
  font-family:'Calibri','sans-serif';
  color:#1F497D;}
span.EmailStyle21
  {mso-style-type:personal;
  font-family:'Calibri','sans-serif';
  color:#1F497D;}
span.EmailStyle22
  {mso-style-type:personal;
  font-family:'Calibri','sans-serif';
  color:#1F497D;}
span.EmailStyle23
  {mso-style-type:personal;
  font-family:'Calibri','sans-serif';
  color:#1F497D;}
span.EmailStyle24
  {mso-style-type:personal;
  font-family:'Calibri','sans-serif';
  color:#1F497D;}
span.EmailStyle25
  {mso-style-type:personal-reply;
  font-family:'Calibri','sans-serif';
  color:#1F497D;}
.MsoChpDefault
  {mso-style-type:export-only;
  font-size:10.0pt;}
@page WordSection1
  {size:612.0pt 792.0pt;
  margin:72.0pt 72.0pt 72.0pt 72.0pt;}
div.WordSection1
  {page:WordSection1;}
/* List Definitions */
@list l0
  {mso-list-id:1509832205;
  mso-list-type:hybrid;
  mso-list-template-ids:-1241078396 67698703 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l0:level1
  {mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;}
@list l0:level2
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:'Courier New';}
@list l0:level3
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l0:level4
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l0:level5
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:'Courier New';}
@list l0:level6
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l0:level7
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l0:level8
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:'Courier New';}
@list l0:level9
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l1
  {mso-list-id:1693457190;
  mso-list-type:hybrid;
  mso-list-template-ids:755263388 67698699 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l1:level1
  {mso-level-number-format:bullet;
  mso-level-text:\F0D8;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:18.0pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l1:level2
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:54.0pt;
  text-indent:-18.0pt;
  font-family:'Courier New';}
@list l1:level3
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:90.0pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l1:level4
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:126.0pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l1:level5
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:162.0pt;
  text-indent:-18.0pt;
  font-family:'Courier New';}
@list l1:level6
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:198.0pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l1:level7
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:234.0pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l1:level8
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:270.0pt;
  text-indent:-18.0pt;
  font-family:'Courier New';}
@list l1:level9
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:306.0pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l2
  {mso-list-id:2105566545;
  mso-list-type:hybrid;
  mso-list-template-ids:-1241078396 67698703 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l2:level1
  {mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;}
@list l2:level2
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:'Courier New';}
@list l2:level3
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l2:level4
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l2:level5
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:'Courier New';}
@list l2:level6
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l2:level7
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l2:level8
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:'Courier New';}
@list l2:level9
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Wingdings;}
ol
  {margin-bottom:0cm;}
ul
  {margin-bottom:0cm;}
--></style><!--[if gte mso 9]><xml>
<o:shapedefaults v:ext='edit' spidmax='1026' />
</xml><![endif]--><!--[if gte mso 9]><xml>
<o:shapelayout v:ext='edit'>
<o:idmap v:ext='edit' data='1' />
</o:shapelayout></xml><![endif]--></head><body>



Welcome to the worry-free world of Religare Health Insurance!</span></b><o:p></o:p></p><p class=MsoNormal><span style='font-size:10.0pt;line-height:115%'>Dear <span style='color:#1F497D'>##Name</span>,</span><o:p></o:p></p><p class=MsoNormal><span style='font-size:10.0pt;line-height:115%'>We are pleased to inform that you and your family are now covered under new Group Health Insurance Policy (Group CARE), effective from ##date. In order to ease the enrollment process, we have created an exclusive platform for the employees of ##companyname. The platform (link below) will allow you to do online enrollment of your dependent family members. </span><o:p></o:p></p><p class=MsoNormal><span style='font-size:10.0pt;line-height:115%'>URL:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><a href='##employeetopupplanlink' target='_blank'><b><span style='font-size:9.0pt;line-height:115%;font-family:'Arial','sans-serif''>##employeetopupplanlink</span></b></a><o:p></o:p></p><p class=MsoNormal><span style='font-size:10.0pt;line-height:115%'>Please follow below mentioned steps for login and enrollment:</span><o:p></o:p></p><p class=MsoListParagraphCxSpFirst style='text-indent:-18.0pt;mso-list:l0 level1 lfo2'><![if !supportLists]><span style='mso-list:Ignore'>1.<span style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><![endif]><span style='font-size:10.0pt;line-height:115%'>Click on the above URL link.</span><o:p></o:p></p><p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;mso-list:l0 level1 lfo2'><![if !supportLists]><span style='mso-list:Ignore'>2.<span style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><![endif]><span style='font-size:10.0pt;line-height:115%'>Enter your email id along with employee id or date of birth.</span><o:p></o:p></p><p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;mso-list:l0 level1 lfo2'><![if !supportLists]><span style='mso-list:Ignore'>3.<span style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><![endif]><span style='font-size:10.0pt;line-height:115%'>Check your existing details {Name, DOB, Marital status (Grade Section), email id &amp; Contact no.}</span><o:p></o:p></p><p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;mso-list:l0 level1 lfo2'><![if !supportLists]><span style='mso-list:Ignore'>4.<span style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><![endif]><span style='font-size:10.0pt;line-height:115%'>Edit your details in case of any discrepancy.</span><o:p></o:p></p><p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;mso-list:l0 level1 lfo2'><![if !supportLists]><span style='mso-list:Ignore'>5.<span style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><![endif]><span style='font-size:10.0pt;line-height:115%'>Select the sum insured and add details of your dependents as per the below mentioned eligibility criteria.</span><o:p></o:p></p><p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;mso-list:l0 level1 lfo2'><![if !supportLists]><span style='mso-list:Ignore'>6.<span style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><![endif]><span style='font-size:10.0pt;line-height:115%'>Once you complete the details click on the submit button to complete the enrollment</span><o:p></o:p></p><p class=MsoListParagraphCxSpLast style='text-indent:-18.0pt;mso-list:l0 level1 lfo2'><![if !supportLists]><span style='mso-list:Ignore'>7.<span style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><![endif]><span style='font-size:10.0pt;line-height:115%'>In case the enrollment is not complete a reminder mail will be sent to you within 48 working hrs.</span><o:p></o:p></p><p class=MsoNormal><span style='font-size:10.0pt;line-height:115%'>Please refer below eligibility criteria for enrolling the dependents and sum insured.</span><o:p></o:p></p><table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=643 style='width:482.4pt;border-collapse:collapse'><tr style='height:35.0pt'><td width=107 style='width:79.9pt;border:solid white 1.0pt;border-bottom:solid white 3.0pt;background:#95B3D7;padding:3.6pt 7.2pt 3.6pt 7.2pt;height:35.0pt'><p class=MsoNormal style='margin-top:3.35pt;vertical-align:baseline'><b><span style='font-size:9.0pt;line-height:115%;color:white'>Sum Insured</span></b><o:p></o:p></p></td><td width=158 style='width:118.4pt;border-top:solid white 1.0pt;border-left:none;border-bottom:solid white 3.0pt;border-right:solid white 1.0pt;background:#95B3D7;padding:3.6pt 7.2pt 3.6pt 7.2pt;height:35.0pt'><p class=MsoNormal style='margin-top:3.35pt;vertical-align:baseline'><b><span style='font-size:9.0pt;line-height:115%;color:white'>3 Lac (unmarried employee)</span></b><o:p></o:p></p></td><td width=206 style='width:154.65pt;border-top:solid white 1.0pt;border-left:none;border-bottom:solid white 3.0pt;border-right:solid white 1.0pt;background:#95B3D7;padding:3.6pt 7.2pt 3.6pt 7.2pt;height:35.0pt'><p class=MsoNormal style='margin-top:3.35pt;vertical-align:baseline'><b><span style='font-size:9.0pt;line-height:115%;color:white'>4 lac &nbsp;(Married employee with / without parent)</span></b><o:p></o:p></p></td><td width=173 valign=top style='width:129.45pt;border-top:solid white 1.0pt;border-left:none;border-bottom:solid white 3.0pt;border-right:solid white 1.0pt;background:#95B3D7;padding:0cm 0cm 0cm 0cm;height:35.0pt'><p class=MsoNormal style='mso-margin-top-alt:3.35pt;margin-right:0cm;margin-bottom:10.0pt;margin-left:1.05pt;text-indent:-1.05pt;vertical-align:baseline'><b><span style='font-size:9.0pt;line-height:115%;color:white'>6 Lac (married employee with &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;parent)</span></b><o:p></o:p></p></td></tr><tr style='height:79.65pt'><td width=107 style='width:79.9pt;border:solid white 1.0pt;border-top:none;background:#C6D9F1;padding:3.6pt 7.2pt 3.6pt 7.2pt;height:79.65pt'><p class=MsoNormal style='margin-top:3.35pt;vertical-align:baseline'><b><span style='font-size:9.0pt;line-height:115%;color:black'>Family Definition</span></b><o:p></o:p></p></td><td width=158 style='width:118.4pt;border-top:none;border-left:none;border-bottom:solid white 1.0pt;border-right:solid white 1.0pt;background:#C6D9F1;padding:3.6pt 7.2pt 3.6pt 7.2pt;height:79.65pt'><p class=MsoNormal style='mso-margin-top-alt:3.35pt;margin-right:0cm;margin-bottom:0cm;margin-left:18.0pt;margin-bottom:.0001pt;line-height:normal;vertical-align:baseline'><span style='font-size:9.0pt;color:black'>Self only</span><o:p></o:p></p><p class=MsoNormal style='mso-margin-top-alt:3.35pt;margin-right:0cm;margin-bottom:0cm;margin-left:18.0pt;margin-bottom:.0001pt;line-height:normal;vertical-align:baseline'><span style='font-size:9.0pt;color:black'>Self + 1Parents</span><o:p></o:p></p><p class=MsoNormal style='mso-margin-top-alt:3.35pt;margin-right:0cm;margin-bottom:0cm;margin-left:18.0pt;margin-bottom:.0001pt;line-height:normal;vertical-align:baseline'><span style='font-size:9.0pt;color:black'>Self +2 Parents</span><o:p></o:p></p></td><td width=206 style='width:154.65pt;border-top:none;border-left:none;border-bottom:solid white 1.0pt;border-right:solid white 1.0pt;background:#C6D9F1;padding:3.6pt 7.2pt 3.6pt 7.2pt;height:79.65pt'><p class=MsoNormal style='mso-margin-top-alt:3.35pt;margin-right:0cm;margin-bottom:0cm;margin-left:18.0pt;margin-bottom:.0001pt;line-height:normal;vertical-align:baseline'><span style='font-size:9.0pt;color:black'>Self + Spouse</span><o:p></o:p></p><p class=MsoNormal style='mso-margin-top-alt:3.35pt;margin-right:0cm;margin-bottom:0cm;margin-left:18.0pt;margin-bottom:.0001pt;line-height:normal;vertical-align:baseline'><span style='font-size:9.0pt;color:black'>Self + Spouse + Children</span><o:p></o:p></p><p class=MsoNormal style='mso-margin-top-alt:3.35pt;margin-right:0cm;margin-bottom:0cm;margin-left:18.0pt;margin-bottom:.0001pt;line-height:normal;vertical-align:baseline'><span style='font-size:9.0pt;color:black'>Self + Spouse + Children + 1 Parent</span><o:p></o:p></p><p class=MsoNormal style='mso-margin-top-alt:3.35pt;margin-right:0cm;margin-bottom:0cm;margin-left:18.0pt;margin-bottom:.0001pt;line-height:normal;vertical-align:baseline'><span style='font-size:9.0pt;color:black'>Self + Spouse + Children + 2 Parent</span><o:p></o:p></p></td><td width=173 valign=top style='width:129.45pt;border-top:none;border-left:none;border-bottom:solid white 1.0pt;border-right:solid white 1.0pt;background:#C6D9F1;padding:0cm 0cm 0cm 0cm;height:79.65pt'><p class=MsoNormal style='mso-margin-top-alt:3.35pt;margin-right:0cm;margin-bottom:0cm;margin-left:18.0pt;margin-bottom:.0001pt;line-height:normal;vertical-align:baseline'><span style='font-size:9.0pt;color:black'>Self + Spouse + Children + 1 Parent</span><o:p></o:p></p><p class=MsoNormal style='mso-margin-top-alt:3.35pt;margin-right:0cm;margin-bottom:0cm;margin-left:18.0pt;margin-bottom:.0001pt;line-height:normal;vertical-align:baseline'><span style='font-size:9.0pt;color:black'>Self + Spouse + Children + 2 Parent</span><o:p></o:p></p></td></tr></table><p class=MsoNormal style='line-height:normal'><span style='font-size:9.0pt'>&nbsp;</span><o:p></o:p></p><p class=MsoNormal style='line-height:normal'><span style='font-size:9.0pt'>Please also refer the premium contribution table below for adding parents</span><o:p></o:p></p><table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=637 style='width:477.75pt;margin-left:4.65pt;border-collapse:collapse'><tr style='height:35.1pt'><td width=211 style='width:158.0pt;border-top:solid white 1.0pt;border-left:none;border-bottom:solid white 1.5pt;border-right:solid white 1.0pt;background:#95B3D7;padding:0cm 5.4pt 0cm 5.4pt;height:35.1pt'><p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><b><span style='font-size:9.0pt;color:white'>Options</span></b><o:p></o:p></p></td><td width=180 style='width:135.25pt;border-top:solid white 1.0pt;border-left:none;border-bottom:solid white 1.5pt;border-right:solid white 1.0pt;background:#95B3D7;padding:0cm 5.4pt 0cm 5.4pt;height:35.1pt'><p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><b><span style='font-size:9.0pt;color:white'>Sum Insured</span></b><o:p></o:p></p></td><td width=246 style='width:184.5pt;border-top:solid white 1.0pt;border-left:none;border-bottom:solid white 1.5pt;border-right:solid white 1.0pt;background:#95B3D7;padding:0cm 5.4pt 0cm 5.4pt;height:35.1pt'><p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><b><span style='font-size:9.0pt;color:white'>Employees contribution for Optional Parental cover</span></b><o:p></o:p></p></td></tr><tr style='height:15.75pt'><td width=211 nowrap style='width:158.0pt;border:solid #EEECE1 1.0pt;border-top:none;background:#C5D9F1;padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt'><p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><span style='font-size:9.0pt;font-family:'Times New Roman','serif';color:black'>Self</span><o:p></o:p></p></td><td width=180 style='width:135.25pt;border-top:none;border-left:none;border-bottom:solid #EEECE1 1.0pt;border-right:solid #EEECE1 1.0pt;background:#C5D9F1;padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt'><p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><span style='font-size:9.0pt;font-family:'Times New Roman','serif';color:black'>300000</span><o:p></o:p></p></td><td width=246 style='width:184.5pt;border-top:none;border-left:none;border-bottom:solid #EEECE1 1.0pt;border-right:solid #EEECE1 1.0pt;background:#C5D9F1;padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt'><p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><span style='font-size:9.0pt;font-family:'Times New Roman','serif';color:black'>0</span><o:p></o:p></p></td></tr><tr style='height:15.75pt'><td width=211 nowrap style='width:158.0pt;border:solid #EEECE1 1.0pt;border-top:none;background:#C5D9F1;padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt'><p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><span style='font-size:9.0pt;font-family:'Times New Roman','serif';color:black'>Self+1 Parent</span><o:p></o:p></p></td><td width=180 style='width:135.25pt;border-top:none;border-left:none;border-bottom:solid #EEECE1 1.0pt;border-right:solid #EEECE1 1.0pt;background:#C5D9F1;padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt'><p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><span style='font-size:9.0pt;font-family:'Times New Roman','serif';color:black'>300000</span><o:p></o:p></p></td><td width=246 style='width:184.5pt;border-top:none;border-left:none;border-bottom:solid #EEECE1 1.0pt;border-right:solid #EEECE1 1.0pt;background:#C5D9F1;padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt'><p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><span style='font-size:9.0pt;font-family:'Times New Roman','serif';color:black'>26420</span><o:p></o:p></p></td></tr><tr style='height:15.75pt'><td width=211 nowrap style='width:158.0pt;border:solid #EEECE1 1.0pt;border-top:none;background:#C5D9F1;padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt'><p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><span style='font-size:9.0pt;font-family:'Times New Roman','serif';color:black'>Self+2 Parents</span><o:p></o:p></p></td><td width=180 style='width:135.25pt;border-top:none;border-left:none;border-bottom:solid #EEECE1 1.0pt;border-right:solid #EEECE1 1.0pt;background:#C5D9F1;padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt'><p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><span style='font-size:9.0pt;font-family:'Times New Roman','serif';color:black'>300000</span><o:p></o:p></p></td><td width=246 style='width:184.5pt;border-top:none;border-left:none;border-bottom:solid #EEECE1 1.0pt;border-right:solid #EEECE1 1.0pt;background:#C5D9F1;padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt'><p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><span style='font-size:9.0pt;font-family:'Times New Roman','serif';color:black'>28745</span><o:p></o:p></p></td></tr><tr style='height:15.75pt'><td width=211 nowrap style='width:158.0pt;border:solid #EEECE1 1.0pt;border-top:none;background:#C5D9F1;padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt'><p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><span style='font-size:9.0pt;font-family:'Times New Roman','serif';color:black'>Self+Spouse+2Kids</span><o:p></o:p></p></td><td width=180 style='width:135.25pt;border-top:none;border-left:none;border-bottom:solid #EEECE1 1.0pt;border-right:solid #EEECE1 1.0pt;background:#C5D9F1;padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt'><p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><span style='font-size:9.0pt;font-family:'Times New Roman','serif';color:black'>400000</span><o:p></o:p></p></td><td width=246 style='width:184.5pt;border-top:none;border-left:none;border-bottom:solid #EEECE1 1.0pt;border-right:solid #EEECE1 1.0pt;background:#C5D9F1;padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt'><p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><span style='font-size:9.0pt;font-family:'Times New Roman','serif';color:black'>0</span><o:p></o:p></p></td></tr><tr style='height:15.75pt'><td width=211 nowrap style='width:158.0pt;border:solid #EEECE1 1.0pt;border-top:none;background:#C5D9F1;padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt'><p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><span style='font-size:9.0pt;font-family:'Times New Roman','serif';color:black'>Self+Spouse+2Kids+1 Parent</span><o:p></o:p></p></td><td width=180 style='width:135.25pt;border-top:none;border-left:none;border-bottom:solid #EEECE1 1.0pt;border-right:solid #EEECE1 1.0pt;background:#C5D9F1;padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt'><p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><span style='font-size:9.0pt;color:black'>400000</span><o:p></o:p></p></td><td width=246 style='width:184.5pt;border-top:none;border-left:none;border-bottom:solid #EEECE1 1.0pt;border-right:solid #EEECE1 1.0pt;background:#C5D9F1;padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt'><p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><span style='font-size:9.0pt;color:black'>29240</span><o:p></o:p></p></td></tr><tr style='height:15.75pt'><td width=211 nowrap style='width:158.0pt;border:solid #EEECE1 1.0pt;border-top:none;background:#C5D9F1;padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt'><p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><span style='font-size:9.0pt;font-family:'Times New Roman','serif';color:black'>Self+Spouse+2Kids+2 Parents</span><o:p></o:p></p></td><td width=180 style='width:135.25pt;border-top:none;border-left:none;border-bottom:solid #EEECE1 1.0pt;border-right:solid #EEECE1 1.0pt;background:#C5D9F1;padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt'><p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><span style='font-size:9.0pt;color:black'>400000</span><o:p></o:p></p></td><td width=246 style='width:184.5pt;border-top:none;border-left:none;border-bottom:solid #EEECE1 1.0pt;border-right:solid #EEECE1 1.0pt;background:#C5D9F1;padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt'><p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><span style='font-size:9.0pt;color:black'>32147</span><o:p></o:p></p></td></tr><tr style='height:15.75pt'><td width=211 nowrap style='width:158.0pt;border:solid #EEECE1 1.0pt;border-top:none;background:#C5D9F1;padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt'><p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><span style='font-size:9.0pt;font-family:'Times New Roman','serif';color:black'>Self+Family+1 Parent</span><o:p></o:p></p></td><td width=180 style='width:135.25pt;border-top:none;border-left:none;border-bottom:solid #EEECE1 1.0pt;border-right:solid #EEECE1 1.0pt;background:#C5D9F1;padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt'><p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><span style='font-size:9.0pt;color:black'>600000</span><o:p></o:p></p></td><td width=246 style='width:184.5pt;border-top:none;border-left:none;border-bottom:solid #EEECE1 1.0pt;border-right:solid #EEECE1 1.0pt;background:#C5D9F1;padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt'><p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><span style='font-size:9.0pt;color:black'>32672</span><o:p></o:p></p></td></tr><tr style='height:15.75pt'><td width=211 nowrap style='width:158.0pt;border:solid #EEECE1 1.0pt;border-top:none;background:#C5D9F1;padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt'><p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><span style='font-size:9.0pt;font-family:'Times New Roman','serif';color:black'>Self+Family+2 Parent</span><o:p></o:p></p></td><td width=180 style='width:135.25pt;border-top:none;border-left:none;border-bottom:solid #EEECE1 1.0pt;border-right:solid #EEECE1 1.0pt;background:#C5D9F1;padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt'><p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><span style='font-size:9.0pt;color:black'>600000</span><o:p></o:p></p></td><td width=246 style='width:184.5pt;border-top:none;border-left:none;border-bottom:solid #EEECE1 1.0pt;border-right:solid #EEECE1 1.0pt;background:#C5D9F1;padding:0cm 5.4pt 0cm 5.4pt;height:15.75pt'><p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><span style='font-size:9.0pt;color:black'>36161</span><o:p></o:p></p></td></tr></table><p class=MsoNormal><span style='font-size:8.0pt;line-height:115%'>&nbsp;</span><o:p></o:p></p><p class=MsoListParagraphCxSpFirst><span style='font-size:8.0pt;line-height:115%'>&nbsp;</span><o:p></o:p></p><p class=MsoListParagraphCxSpMiddle><span style='font-size:8.0pt;line-height:115%'>*Family means &#8211; spouse and child (maximum 2 child)</span><o:p></o:p></p><p class=MsoListParagraphCxSpLast><span style='font-size:8.0pt;line-height:115%'>** Employee and Immediate family is covered with zero cost impact</span><o:p></o:p></p><p class=MsoNormal><span style='font-size:10.0pt;line-height:115%'>Please note that the enrollment process will be available from 5<sup>th</sup> &nbsp;Oct&#8217;15 till 30<sup>th</sup> Oct&#8217;15 </span><o:p></o:p></p><p class=MsoNormal><span style='font-size:10.0pt;line-height:115%'>For any assistance that you may require, please feel free to send an email to </span><a href='mailto:enrollment@religare.com'><span style='font-size:10.0pt;line-height:115%'>enrollment@religare.com</span></a><span style='font-size:10.0pt;line-height:115%'> or call us at 011-39121143 / 91+9582269216</span>
<hr style='border:none; color:#909090; background-color:#B0B0B0; height: 1px; width: 99%;' />
</body></html>
     ";
?>