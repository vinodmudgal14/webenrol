<?php

$template = '<table width="595" cellspacing="0" cellpadding="0" border="0" align="center">
    <tbody>
        <tr>
            <td style="border:1px solid #dddddd;">
            <table width="595" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                    <tr>
                        <td><span style="font-family: Verdana;"><img src="https://uattractus.religarehealthinsurance.com:9443/religarehrcrm/crmadmin/images/top_img.jpg" alt="" /></span></td>
                    </tr>
                    <tr>
                        <td><span style="font-family: Verdana;">&nbsp;</span></td>
                    </tr>
                    <tr>
                        <td>
                        <table width="595" cellspacing="0" cellpadding="0" border="0" align="center">
                            <tbody>
                                <tr>
                                    <td>
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tbody>
                                            <tr>
                                                <td width="595" valign="top" height="142" align="left" style="font:normal 12px Arial, Helvetica, sans-serif; color:#66656f; line-height:16px; text-align:justify;padding:5px;"><span style="font-family: Verdana;">Dear ##Name,<br />
                                                <br />
                                                Welcome to a world where what matters, above all, is your Health&hellip;.Hamesha!.<br />
                                                <br />
                                                Welcome to the worry free world of Religare Health Insurance (RHI).                                                 <br />
                                                <br />
                                                As RHI, it\'s our mission to provide you access to the highest quality of healthcare and put you back on                                                 the road to a worry-free recovery, without a care about medical bills and other related expenses. As a                                                 member/employee of ##companyname, you are currently insured by our Group Health Insurance                                                 product - Group Care.                                                 <br />
                                                <br />
                                                However, in view of your current &amp; future health needs, you have an option to enhance your coverage                                                 and opt for various value added benefits. Simply  </span><a href="##employeetopupplanlink"><span style="font-family: Verdana;">click here</span></a><span style="font-family: Verdana;"> and enrol for additional coverage.                                                  <br />
                                                <br />
                                                Kindly complete the above enrolment on or before ##date. In case of any query, please  to us at                                                  enrollment@religare.com.                                                  <br />
                                                <br />
                                                Once again, we thank you for this opportunity to serve you, and wish you and your loved ones good                                                 health always!                                                 <br />
                                                <br />
                                                Team Religare Health Insurance</span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td><span style="font-family: Verdana;">&nbsp;</span></td>
                                </tr>
                            </tbody>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td><span style="font-family: Verdana;">&nbsp;</span></td>
                    </tr>
                    <tr>
                        <td>
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <td bgcolor="#d1e392">
                                    <table width="565" cellspacing="0" cellpadding="0" border="0" align="center">
                                        <tbody>
                                            <tr>
                                                <td width="51%" height="62">
                                                <table width="100%" cellspacing="0" cellpadding="0" border="0" style="padding-left:5px;">
                                                    <tbody>
                                                        <tr>
                                                            <td width="294" height="15" style="font:normal 8px Verdana, Helvetica; color:#1f7340;"><span style="font-family: Verdana;">To know more, visit our website</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td height="15" style="font:normal 16px Verdana, Helvetica;"><a href="http://www.religarehealthinsurance.com" style="color:#1f7340; text-decoration:none;" target="_blank"><span style="font-family: Verdana;">www.religarehealthinsurance.com</span></a></td>
                                                        </tr>
                                                        <tr>
                                                            <td height="15">
                                                            <table width="275" cellspacing="0" cellpadding="0" border="0" style="padding-bottom:4px;">
                                                                <tbody>
                                                                    <tr>
                                                                        <td width="82" style="font:normal 7px Verdana, Helvetica; color:#1f7340;"><a href="#" style="color:#1f7340; text-decoration:none;"><span style="font-family: Verdana;"><img border="0" src="https://uattractus.religarehealthinsurance.com:9443/religarehrcrm/crmadmin/images/tick.jpg" alt="" />&nbsp;Quick quote &amp; buy</span></a></td>
                                                                        <td width="67" style="font:normal 7px Verdana, Helvetica; color:#1f7340;"><a href="#" style="color:#1f7340; text-decoration:none;"><span style="font-family: Verdana;"><img border="0" src="https://uattractus.religarehealthinsurance.com:9443/religarehrcrm/crmadmin/images/tick.jpg" alt="" />&nbsp;Online renewals</span></a></td>
                                                                        <td width="82" style="font:normal 7px Verdana, Helvetica; color:#1f7340;"><a href="#" style="color:#1f7340; text-decoration:none;"><span style="font-family: Verdana;"><img border="0" src="https://uattractus.religarehealthinsurance.com:9443/religarehrcrm/crmadmin/images/tick.jpg" alt="" />&nbsp;Customer support</span></a></td>
                                                                        <td width="55" style="font:normal 7px Verdana, Helvetica; color:#1f7340;"><a href="#" style="color:#1f7340; text-decoration:none;"><span style="font-family: Verdana;"><img border="0" src="https://uattractus.religarehealthinsurance.com:9443/religarehrcrm/crmadmin/images/tick.jpg" alt="" />&nbsp;Claim                                             centre</span></a></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                </td>
                                                <td width="49%" style="border-left:1px solid #5faa4a;">
                                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                    <tbody>
                                                        <tr>
                                                            <td height="10" style="font:normal 7px Verdana, Helvetica; color:#1f7340; padding-left:10px;">
                                                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td width="5%"><span style="font-family: Verdana;"><img border="0" src="https://uattractus.religarehealthinsurance.com:9443/religarehrcrm/crmadmin/images/phoneicon.jpg" alt="" /></span></td>
                                                                        <td width="29%" style="font:normal 8px Verdana, Helvetica; color:#1f7340;"><span style="font-family: Verdana;">1800-200-4488</span></td>
                                                                        <td width="6%"><span style="font-family: Verdana;"><img border="0" src="https://uattractus.religarehealthinsurance.com:9443/religarehrcrm/crmadmin/images/mailicon.jpg" alt="" /></span></td>
                                                                        <td width="60%" align="left" style="font:normal 8px Verdana, Helvetica;"><a href="mailto:customerfirst@religarehealthinsurance.com" style=" color:#1f7340; text-decoration:none;"><span style="font-family: Verdana;">customerfirst@religarehealthinsurance.com</span></a></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="10" style="font:normal 7px Verdana, Helvetica; color:#1f7340; padding-left:10px;"><span style="font-family: Verdana;"><br />
                                                            Regd. Office: Religare Health Insurance, D-3, District Centre, Saket, New Delhi-17<br />
                                                            CIN:U66000DL2007PLC161503 IRDA Regn. No.: 148</span></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle" height="25" align="center">
                        <table width="554" cellspacing="0" cellpadding="0" border="0" align="center">
                            <tbody>
                                <tr>
                                    <td width="382" height="20" align="left" style="font:bold 10px Arial, Helvetica, sans-serif; color:#66656f;"><span style="font-family: Verdana;">Health Insurance | Travel Insurance | Critical Illness | Personal Accident</span></td>
                                    <td width="98" align="right" style="font:bold 10px Arial, Helvetica, sans-serif; color:#66656f;"><span style="font-family: Verdana;">Follow Us</span></td>
                                    <td width="21" align="right"><a href="https://www.facebook.com/ReligareHealthInsurance" target="_blank"><span style="font-family: Verdana;"><img border="0" src="https://uattractus.religarehealthinsurance.com:9443/religarehrcrm/crmadmin/images/fb.jpg" alt="" /></span></a></td>
                                    <td width="21" align="right"><a href="https://www.linkedin.com/company/religare-health-insurance" target="_blank"><span style="font-family: Verdana;"><img border="0" src="https://uattractus.religarehealthinsurance.com:9443/religarehrcrm/crmadmin/images/in.jpg" alt="" /></span></a></td>
                                    <td width="32" align="right"><a href="https://www.youtube.com/user/ReligareHealthIns" target="_blank"><span style="font-family: Verdana;"><img border="0" src="https://uattractus.religarehealthinsurance.com:9443/religarehrcrm/crmadmin/images/utube.jpg" alt="" /></span></a></td>
                                </tr>
                            </tbody>
                        </table>
                        </td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
    </tbody>
</table>
<p><span style="font-family: Verdana;"><br/></span></p>';


//function sendMailToEmp()
{
    $file = fopen('C:/xampp/htdocs/religare/webenrol/admin/cms/employeeMail/employeeMailList.csv', 'r');
    $employeeListArray = array();
    $subject = 'Access Url';

    while (!feof($file))
    {
        $employeeListArray[] = fgets($file);
    }

    $companyName = '';
    foreach ($employeeListArray as $employeeList)
    {
        $allEmployee = explode(',', $employeeList);

        foreach ($allEmployee as $employee)
        {
            $empDetail = explode(':', $employee);
            if (count($empDetail) == 7)
            {
                $empName = $empDetail[1];
                $companyName = $empDetail[6];
                $empString = $empDetail[0] . ":" . $empDetail[1] . ":" . $empDetail[2] .":" . $empDetail[3] .":". $empDetail[4].":".$empDetail[5];
                $employeeUrl = base64_encode(substr($empString, 1));
                $empUrl = "http://localhost/religare/webenrol/company_" . $employeeUrl;

                $expiryDate = date("d:m:Y", $empDetail[5]);
                if (strstr($empDetail[0], '@'))
                {
                    $formattedTemplate = str_replace("##Name", $empName, $template);
                    $formattedTemplate = str_replace("##companyname", $companyName, $formattedTemplate);
                    $formattedTemplate = str_replace("##employeetopupplanlink", $empUrl, $formattedTemplate);
                    $formattedTemplate = str_replace("##date", $expiryDate, $formattedTemplate);

                    if (mail($employee, $subject, $formattedTemplate))
                    {
                        echo "Mail Sent: $employeeUrl (Comp: $companyName) \n";
                    }
                    else
                    {
                        echo "Mail Failed: $empName (Comp: $companyName) \n";
                    }
                }
            }
            else if (count($empDetail) == 3)
            {
                $rrmEmail = $empDetail[0];
                $rrmName = $empDetail[1];
                $companyId = $empDetail[2];
                if (mail($rrmEmail, 'Employee Access', $template))
                    echo "Mail Sent(Manager): $rrmName (Comp: $companyName) \n";
            }
        }
    }
    file_put_contents('C:/xampp/htdocs/religare/webenrol/admin/cms/employeeMail/employeeMailList.csv', "");
    fclose($file);
}
?>