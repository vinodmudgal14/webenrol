<?php
include('../conf/session.php');
include('../conf/conf.php');
include('../conf/fucts.php');

$prefix = substr(time(), 5, 5);

$companyId = sanitize_data(@$_REQUEST['companyId']);
$gradebasis = sanitize_data(@$_REQUEST['gradebasis']);
$topupbasis = sanitize_data(@$_REQUEST['topupbasis']);
$gradearr = @$_REQUEST['gradearr'];
$gradesiarr = @$_REQUEST['gradesiarr'];
$agesiarr = @$_REQUEST['agesiarr'];
$topupsiarr = @$_REQUEST['topupsiarr'];
$topupgradearr = @$_REQUEST['topupgradearr'];

if ($gradebasis == 'grade')
{
    for ($p = 0; $p < sizeof($gradearr); $p++)
    {
        $gradeSiSize = (int)sizeof($gradesiarr) / sizeof($gradearr);
        for ($i = 0; $i < $gradeSiSize; $i++)
        {            
            $siid = $gradesiarr[$i];
            $siid = sanitize_data(@$siid);
            $gradeId = sanitize_data($gradearr[$p]);
            $data = "siChk_" . $siid . "_" . $gradeId;
            $mapVal = sanitize_data(@$_REQUEST[$data]);
            if ($mapVal != '')
            {
                $sival = $mapVal;
                $rs_tblsite1 = "SELECT `id` FROM `tbl_grade_si_mapping` WHERE `companyId` = '$companyId' AND `sumInsuredId` = '$siid' AND `gradeId` = '$gradeId' AND `isDeleted` = 'no' LIMIT 1";
                $rs_tblsite = mysql_query($rs_tblsite1);
                $array_tblsite = mysql_fetch_array($rs_tblsite);

                $updatesql = sprintf("UPDATE `tbl_company` SET `offerSumInsured`='%s', `updatedOn`=unix_timestamp(),`updatedBy` = '%s' WHERE `companyId` = '$companyId' LIMIT 1", mysql_real_escape_string('grade'), mysql_real_escape_string(@$_SESSION['username']), mysql_real_escape_string(@$_SESSION['username']));
                $updateresult = mysql_query("$updatesql");
                    
                if ($array_tblsite['id'] > 0)
                {       
                    /*
                    $sql = sprintf("UPDATE `tbl_grade_si_mapping` SET `sumInsuredId` = '%s',`siAmount`='%s',`updatedOn`=unix_timestamp(),`updatedBy` = '%s' WHERE `companyId` = '$companyId' AND `gradeId` = '$gradeId' AND `sumInsuredId` = '$siid'",
					mysql_real_escape_string(@$siid),
					mysql_real_escape_string(@$sival),
					mysql_real_escape_string(@$_SESSION['username']),
					mysql_real_escape_string(@$_SESSION['username']));
					$result=mysql_query("$sql");
                     */
                }
                else
                {   
                    $query= sprintf("INSERT into `tbl_grade_si_mapping` (`companyId`,`gradeId`,`sumInsuredId`,`siAmount`,`gradepremium`,`createdOn`,`createdBy`,`updatedOn`,`updatedBy`) VALUES ('%s','%s','%s','%s','0',unix_timestamp(),'%s',unix_timestamp(),'%s')",
                    mysql_real_escape_string(@$companyId),
                    mysql_real_escape_string(@$gradeId),	
                    mysql_real_escape_string(@$siid),
                    mysql_real_escape_string(@$sival),		 
                    mysql_real_escape_string(@$_SESSION['username']),
                    mysql_real_escape_string(@$_SESSION['username']));	
                    //echo '</br>'.$query;
                    $result=mysql_query("$query");
                }
            }
            else
            {
                $deletesql = "UPDATE `tbl_grade_si_mapping` SET `isDeleted`='yes' WHERE `companyId` = '$companyId' AND `gradeId` = '$gradeId' AND `sumInsuredId` = '$siid' ";
                $result = mysql_query($deletesql);
            }
        }
    }
    /*
    $siChk = array();

    foreach ($_POST as $key => $value)
    {
        $siCheckKey = explode('_', $key);
        if (@$siCheckKey[0] == 'siCheck')
        {
            if (array_key_exists($siCheckKey[1], $siChk))
            {
                $siChk[$siCheckKey[1]] = $siChk[$siCheckKey[1]] . ',' . $value;
            }
            else
            {
                $siChk[$siCheckKey[1]] = $value;
            }
        }
    }

    $sqlSi = mysql_query("SELECT `id` FROM `tbl_sum_insured`");
    $gradeSi = array();
    while ($row = mysql_fetch_array($sqlSi))
    {
        $gradeSi[] = $row['id'];
    }

    for ($i = 0; $i < sizeof($gradearr); $i++)
    {
        $gradeId = $gradearr[$i];
        $siChkArray = explode(',', $siChk[$gradeId]);        
        foreach ($siChkArray as $siidValue)
        {
            $siidValueArray = explode('_', $siidValue);
            $siid = $siidValueArray[0];
            $sival = $siidValueArray[1];
            for ($g = 0; $g < sizeof($gradeSi); $g++)
            {
                if ($siid == $gradeId[$g])
                {
                    $rs_tblsite1 = "SELECT `id` FROM `tbl_grade_si_mapping` WHERE `companyId` = '$companyId' AND `gradeId` = '$gradeId' AND sumInsuredId = `$siid` AND `isDeleted` = 'no' LIMIT 1";

                    $rs_tblsite = mysql_query($rs_tblsite1);
                    $array_tblsite = mysql_fetch_array($rs_tblsite);

                    $updatesql = sprintf("UPDATE `tbl_company` SET `offerSumInsured`='%s', `updatedOn`=unix_timestamp(),`updatedBy` = '%s' WHERE `companyId` = '$companyId' LIMIT 1", mysql_real_escape_string('grade'), mysql_real_escape_string(@$_SESSION['username']), mysql_real_escape_string(@$_SESSION['username']));
                    $updateresult = mysql_query("$updatesql");

                    if ($array_tblsite['id'] > 0)
                    {
                        $sql = sprintf("UPDATE `tbl_grade_si_mapping` SET `sumInsuredId` = '%s',`siAmount`='%s',`updatedOn`=unix_timestamp(),`updatedBy` = '%s' WHERE `companyId` = '$companyId' AND `gradeId` = '$gradeId' ", mysql_real_escape_string(@$siid), mysql_real_escape_string(@$sival), mysql_real_escape_string(@$_SESSION['username']), mysql_real_escape_string(@$_SESSION['username']));
                        $result = mysql_query("$sql");
                    }
                    else
                    {
                        $query = sprintf("INSERT into `tbl_grade_si_mapping` (`companyId`,`gradeId`,`sumInsuredId`,`siAmount`,`createdOn`,`createdBy`,`updatedOn`,`updatedBy`)	VALUES
                        ('%s','%s','%s','%s',unix_timestamp(),'%s',unix_timestamp(),'%s')", mysql_real_escape_string(@$companyId), mysql_real_escape_string(@$gradeId), mysql_real_escape_string(@$siid), mysql_real_escape_string(@$sival), mysql_real_escape_string(@$_SESSION['username']), mysql_real_escape_string(@$_SESSION['username']));
                        $result = mysql_query("$query");
                    }
                }
                else
                {
                    $deletesql = "UPDATE `tbl_grade_si_mapping` SET `isDeleted`='yes' WHERE `companyId` = '$companyId' AND `sumInsuredId` = '$gradeId[$g]' ";
                    $result = mysql_query($deletesql);
                }
            }
        }
    }*/
}
else
{
    for ($i = 0; $i < sizeof($agesiarr); $i++)
    {
        $agesiId = $agesiarr[$i];
        $agesiId = sanitize_data(@$agesiId);
        $data = "ageSIChk_" . $agesiId;
        $mapVal = sanitize_data($_REQUEST[$data]);
        if ($mapVal != '')
        {
            $rs_tblsite1 = "SELECT `id` FROM `tbl_age_si` WHERE `companyId` = '$companyId' AND `sumInsuredId` = '$agesiId' AND `isDeleted` = 'no' LIMIT 1";
            $rs_tblsite = mysql_query($rs_tblsite1);
            $array_tblsite = mysql_fetch_array($rs_tblsite);
            //echo '<br/>';
            $updatesql = sprintf("UPDATE `tbl_company` SET `offerSumInsured`='%s',`updatedOn`=unix_timestamp(),`updatedBy` = '%s' WHERE `companyId` = '$companyId' LIMIT 1", mysql_real_escape_string('age'), mysql_real_escape_string(@$_SESSION['username']), mysql_real_escape_string(@$_SESSION['username']));
            $updateresult = mysql_query("$updatesql");

            if ($array_tblsite['id'] > 0)
            {
                
            }
            else
            {
                $query = sprintf("INSERT into `tbl_age_si`(`companyId`,`sumInsuredId`,`createdOn`,`createdBy`,`updatedOn`,`updatedBy`)	VALUES
					('%s','%s',unix_timestamp(),'%s',unix_timestamp(),'%s')", mysql_real_escape_string(@$companyId), mysql_real_escape_string(@$agesiId), mysql_real_escape_string(@$_SESSION['username']), mysql_real_escape_string(@$_SESSION['username']));
                $result = mysql_query("$query");
            }
        }
        else
        {
            $deletesql = "UPDATE `tbl_age_si` SET `isDeleted`='yes' WHERE `companyId` = '$companyId' AND `sumInsuredId` = '$agesiId' ";
            $result = mysql_query($deletesql);
        }
    }
}
if ($topupbasis == 'yes')
{
    for ($i = 0; $i < sizeof($topupsiarr); $i++)
    {
        $topupsiId = $topupsiarr[$i];
        $topupsiId = sanitize_data(@$topupsiId);
        for ($p = 0; $p < sizeof($topupgradearr); $p++)
        {
            $topupgradeid = sanitize_data($topupgradearr[$p]);
            $data = "topupChk_" . $topupsiId . "_" . $topupgradeid;
            $mapVal = sanitize_data($_REQUEST[$data]);
            if ($mapVal != '')
            {
                $rs_tblsite1 = "SELECT `id` FROM `tbl_topup_si` WHERE `companyId` = '$companyId' AND `sumInsuredId` = '$topupsiId' AND `gradeId` = '$topupgradeid' AND `isDeleted` = 'no' LIMIT 1";
                $rs_tblsite = mysql_query($rs_tblsite1);
                $array_tblsite = mysql_fetch_array($rs_tblsite);

                $updatesql = sprintf("UPDATE `tbl_company` SET `offerTopUp`='%s',`updatedOn`=unix_timestamp(),`updatedBy` = '%s' WHERE `id` = '$companyId' LIMIT 1", mysql_real_escape_string('yes'), mysql_real_escape_string(@$_SESSION['username']), mysql_real_escape_string(@$_SESSION['username']));
                $updateresult = mysql_query("$updatesql");

                if ($array_tblsite['id'] > 0)
                {
                    $sql = sprintf("UPDATE `tbl_topup_si` SET `sumInsuredId` = '%s',`updatedOn`=unix_timestamp(),`updatedBy` = '%s' WHERE `companyId` = '$companyId' LIMIT 1", mysql_real_escape_string(@$topupsiId), mysql_real_escape_string(@$_SESSION['username']), mysql_real_escape_string(@$_SESSION['username']));
                    $result = mysql_query("$sql");
                }
                else
                {
                    $query = sprintf("INSERT into `tbl_topup_si`(`companyId`,`sumInsuredId`,`gradeId`,`createdOn`,`createdBy`,`updatedOn`,`updatedBy`,`siAmount`)	VALUES
			('%s','%s','%s',unix_timestamp(),'%s',unix_timestamp(),'%s','0')", mysql_real_escape_string(@$companyId), mysql_real_escape_string(@$topupsiId), mysql_real_escape_string(@$topupgradeid), mysql_real_escape_string(@$_SESSION['username']), mysql_real_escape_string(@$_SESSION['username']));
                    $result = mysql_query("$query");
                }
            }
            else
            {
                $deletesql = "UPDATE `tbl_topup_si` SET `isDeleted`='yes' WHERE `companyId` = '$companyId' AND `sumInsuredId` = '$topupsiId' AND `gradeId` = '$topupgradeid' ";
                $result = mysql_query($deletesql);
            }
        }
    }
}
else
{
    $updatesql = sprintf("UPDATE `tbl_company` SET `offerTopUp`='%s',`updatedOn`=unix_timestamp(),`updatedBy` = '%s' WHERE `companyId` = '$companyId' LIMIT 1", mysql_real_escape_string('no'), mysql_real_escape_string(@$_SESSION['username']), mysql_real_escape_string(@$_SESSION['username']));
    $updateresult = mysql_query("$updatesql");
}
?>
<script type="text/javascript">
    document.location = "addCorporate.php?companyId=<?= $companyId; ?>&mode=Edit&settingmsg=Settings saved successfully#tabs-3";
</script>