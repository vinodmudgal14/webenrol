<?php
include_once('../inc/hd.php');
$id			=	sanitize_data(isNumeric(@$_REQUEST['id']));
$mode			=	sanitize_data(@$_REQUEST['mode']);
$companyId		=	sanitize_data(isNumeric(@$_REQUEST['companyId']));
$gradeId		=	sanitize_data(@$_REQUEST['gradeId']);
$pagetype		=	sanitize_data(@$_REQUEST['pagetype']);

if(isset($_REQUEST["mode"])=="Edit"){
	$rs_tblsite1 = sprintf("SELECT * FROM `tbl_company` WHERE `id` = '%s' LIMIT 1",
	mysql_real_escape_string($companyId));
	$rs_tblsite=mysql_query($rs_tblsite1);
	$array_tblsite=mysql_fetch_array($rs_tblsite);
}	else	{
	$array_tblsite["linkType"]="Description";
	$array_tblsite["parentId"]='';
}
if(@gradeId!=''){
	$rs_tblsite2 = sprintf("SELECT * FROM `tbl_grade` WHERE `id` = '%s' LIMIT 1",
	mysql_real_escape_string($gradeId));
	$rs_tblsite2=mysql_query($rs_tblsite2);
	$array_tblsite2=mysql_fetch_array($rs_tblsite2);
}
$agegrouplist 						= ageGroupList();
$gradelist 							= gradeList($companyId);
$suminsuredlist 					= sumInsuredList();
$topupsuminsuredlist 				= topupSumInsuredList();
$empList 							= companyEmpList($companyId);
$suminsuredmappinglist 				= sumInsuredMapList($companyId);
$gradesimappinglist 				= gradeSumInsuredMapList($companyId);
//print_r($suminsuredmappinglist);
$topupsuminsuredmappinglist 		= topUpSumInsuredMapList($companyId);
$companydetails 					= companydetails($companyId);
//echo '<pre>';
//print_r($gradesimappinglist);
?>
<script type="text/javascript" src="<?=_WWWROOT;?>/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<?=_WWWROOT;?>/js/jquery-ui-1.8.18.custom.min.js"></script>
<link rel="stylesheet" href="<?=_WWWROOT;?>/css/colorpicker.css" type="text/css" />
<link type="text/css" href="<?=_WWWROOT;?>/css/ui-lightness/jquery-ui-1.8.18.custom.css" rel="stylesheet" />
<script type="text/javascript" src="<?=_WWWROOT;?>/js/ui.datepicker.js"></script>
<link href="<?=_WWWROOT;?>/css/ui.datepicker.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
$(document).ready(function() {
$("#tabs").tabs();
updateLink();
$("#urlExpiryDate").datepicker({
	minDate: -0,
	yearRange: '2012:2015',
    dateFormat: "yy-mm-dd"
	});
});
/*function addGrade(companyId){
	var gradeval = $("#gradeVal").val();
	var gradeId = $("#gradeId").val();
	//alert(gradeval);	
	$.ajax({
		type: "POST",
		url: "writeGrade.php",
		data: "grade="+gradeval+"&companyId="+companyId+"&gradeId="+gradeId,
		success: function(msg){			
			$("#graderesult").html(msg);
			$("#gradeVal").val('');
			$("#gradeId").val('');
			}
	});
}	*/
function updateGradeSI(gradeId,companyId){	
	var sival = $("#suminsured_"+gradeId).val();
	var arr = sival.split("_");
	var siId= arr[0];
	var siamount= arr[1];
	$.ajax({
		type: "POST",
		url: "updateGradeSumInsured.php",
		data: "gradeId="+gradeId+"&companyId="+companyId+"&siId="+siId+"&siamount="+siamount,
		success: function(msg){	
			alert("Sum insured updated successfully");
			if(msg!='undefined'){
			$("#updateGradeResult_"+gradeId).html(msg);	
			$("#updateGradeResult1_"+gradeId).html(msg);
			}	else {
			$("#updateGradeResult1_"+gradeId).html('');
			}
		}
	});
}	
function updateAgeSI(companyId,siId,amount){
if($("#ageSIChk_"+siId).is(":checked")) { var agesichk = 'true'; } else { var agesichk = 'false'; } 
$.ajax({
		type: "POST",
		url: "updateAgeSumInsured.php",
		data: "agesichk="+agesichk+"&companyId="+companyId+"&siId="+siId+"&siamount="+amount,
		success: function(msg){	
			alert("Sum insured updated successfully");		
			}
	});
}
function updateTopUpSI(companyId,siId,amount){
if($("#topupChk_"+siId).is(":checked")) { var topupsichk = 'true'; } else { var topupsichk = 'false'; } 
$.ajax({
		type: "POST",
		url: "updateTopUpSumInsured.php",
		data: "topupsichk="+topupsichk+"&companyId="+companyId+"&siId="+siId+"&siamount="+amount,
		success: function(msg){	
			alert("Sum insured updated successfully");		
			}
	});
}
function gradebasisfunc(value){	
var companyId = $("#companyId").val();
	$.ajax({
		type: "POST",
		url: "updateCompanyGradeStatus.php",
		data: "gradeval="+value+"&companyId="+companyId,
		success: function(msg){	
		}
	});
	if(value=='grade') { $(".gradebasisresult").show();  $(".agebasisresult").hide(); } else { $(".gradebasisresult").hide(); $(".agebasisresult").show(); }
}
function topupbasisfunc(value){
	var companyId = $("#companyId").val();
	$.ajax({
		type: "POST",
		url: "updateCompanyTopUpStatus.php",
		data: "gradeval="+value+"&companyId="+companyId,
		success: function(msg){	
		}
	});
	if(value=='yes') { $(".topupbasisresult").show(); } else { $(".topupbasisresult").hide(); }
}
function updateLimits(){
	var companyId = $("#companyId").val();
	var insuranceType	=  $("input[name='insuranceType']:checked").val();
	//alert(insuranceType);
	var policyType	=  $("input[name='policyType']:checked").val();
	//alert(policyType);
	var dependent	=  $("input[name='dependent']:checked").val();
	//alert(dependent);
	if(insuranceType==''){	alert("Please select insurance type."); return false; 	}
	if(policyType=='')	{	alert("Please select policy type."); return false; 		}
	if(dependent=='')	{	alert("Please select dependent status."); return false; }
	if((insuranceType!='')&&(policyType!='')&&(dependent!='')){
		$.ajax({
			type: "POST",
			url: "updateCompanyLimits.php",
			data: "insuranceType="+insuranceType+"&companyId="+companyId+"&policyType="+policyType+"&dependent="+dependent,
			success: function(msg){	
			alert('Limits updated successfully.');
			}
		});
	}
}
function insurancetypefunc(value){
	if(value=='individual') { $(".floaterresult").hide(); $(".individualresult").show(); } else { $(".individualresult").hide(); $(".floaterresult").show(); }
}
function policytypefunc(value){
	if(value=='fixed') { $(".fixedtype").show(); } else { $(".fixedtype").hide(); }
}

function showtype(){
	var typeval = $("#insuranceType").val();
	if(typeval!='') { $(".premiumtype").show(); } else { $(".premiumtype").hide(); $(".dependent").hide(); }
}
function premiumtype(value){
	//alert(value);
	if(value=='fixed'){ $(".dependent").hide(); $(".fixedpremium").show(); } else if(value=='ageLinked'){ $(".fixedpremium").hide(); $(".dependent").show();  } else { $(".fixedpremium").hide(); $(".dependent").hide(); }
}
function updateSIPremium(){
	var sIData = $("#samapping").serialize();
	//alert(sIData);
	$.ajax({
		type: "POST", 
		data: sIData,
		url: "updateSIPremiumData.php",
		success: function(msg){
		alert("Premium updated successfully");
		}
	});
}
function updateTopUpSIPremium(){
	var topUpSIData = $("#topupsamappingform").serialize();
	//alert(topUpSIData);	
	$.ajax({
		type: "POST", 
		data: topUpSIData,
		url: "updateTopUpSIPremiumData.php",
		success: function(msg){
		alert("Top-Up Premium updated successfully");
		}
	});
}
function showSumInsured(){
	var companyId = $("#companyId").val();
	//alert(companyId);
	$.ajax({
		type: "POST", 
		data: "companyId="+companyId,
		url: "showSumInsuredData.php",
		success: function(msg){		
		$("#showsiresult").html(msg);	
		}
	});
	
}
function showTopUpSumInsured(){
	var companyId = $("#companyId").val();
	//alert(companyId);
	$.ajax({
		type: "POST", 
		data: "companyId="+companyId,
		url: "showTopupSumInsuredData.php",
		success: function(msg){		
		$("#showtopupsiresult").html(msg);	
		}
	});
	
}
function showLimits(){
	var companyId = $("#companyId").val();
	//alert(companyId);
	$.ajax({
		type: "POST", 
		data: "companyId="+companyId,
		url: "showLimits.php",
		success: function(msg){		
		$("#limitsresult").html(msg);	
		}
	});
	
}
function showExternal(){
	$(".externalLink").show();
	$(".description").hide();
}
function updateSettings(){
/*var settingdata = $("#settingform").serialize();
var gradebasis = $("#gradebasis").val();
alert(settingdata);
alert(gradebasis);*/
}
function isEmpty(s)
{
	var re = /\s/g; //Match any white space including space, tab, form-feed, etc. 
	var s = s.replace(re, "");
	if (s.length == 0) 
	{
		return true;
	}
	else 
	{
		return false;
	}
}
function specialcharecter(url)
            {
                var iChars = "!`@#$%^&*()+=-[]\\\';,./{}|\":<>?~_";   
                var data = url;
                for (var i = 0; i < data.length; i++)
                {      
                    if (iChars.indexOf(data.charAt(i)) != -1)
                    {    
                    var status = "yes";                   
                    }  else {
					var status = "no";
					}
                }
				return status;
            }
function hasWhiteSpace(s) {
  return s.indexOf(' ') >= 0;
}
function chkuniqueurl(companyId){
var mode = $("#mode").val();
var url = $("#url").val();
if(isEmpty(url))		{ alert("Please enter url.");	return false; exit(); } 
if(hasWhiteSpace(url))	{ alert("Please remove space in url.");	return false; exit(); }
if(specialcharecter(url)=='yes'){ alert("Please remove special character in url.");	return false; exit(); };
$("#chkuniqueresult").html('<img src="../images/loading.gif"/>');
	$.ajax({
		type: "POST", 
		data: "companyId="+companyId+"&url="+url+"&mode="+mode,
		url: "chkuniqueurl.php",
		success: function(msg){	
		if(msg=='yes') { alert("URL is not unique. Please try again."); } else { alert("URL is unique."); }	
		$("#chkuniqueresult").html("");	
		}
	});
}
function showDesc(){

$(".externalLink").hide();

$(".description").show();

}

function compbasicvalidate(){
		var EmailRegex=/^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,6})$/i;;
		var contactPersonEmail = document.corporate.contactPersonEmail.value;
		var rrmEmail = document.corporate.rrmEmail.value;
		
		if(document.corporate.companyName.value=="")
		{
		alert("Please enter the company name");
		document.corporate.companyName.focus();
		return false;
		}
		if(document.corporate.corporateid.value=="")
		{
		alert("Please enter the corporate-id.");
		document.corporate.corporateid.focus();
		return false;
		}
		if(document.corporate.contactPersonName.value=="")
		{
		alert("Please enter the contact person name");
		document.corporate.contactPersonName.focus();
		return false;
		}
		if(contactPersonEmail!=''){
			if((EmailRegex.test(contactPersonEmail)) != true)
			{
			alert("Please enter valid contact person email-id (abc@xyz.com)");
			document.getElementById('contactPersonEmail').focus();
			return false;
			}		
		}
		if(document.corporate.rrmName.value=="")
		{
		alert("Please enter the religare relationship manager name");
		document.corporate.rrmName.focus();
		return false;
		}
		if(rrmEmail!=''){
			if((EmailRegex.test(rrmEmail)) != true)
			{
			alert("Please enter valid relationship manager email-id (abc@xyz.com)");
			document.getElementById('rrmEmail').focus();
			return false;
			}		
		}
		var url = $("#url").val();
		
		if(isEmpty(url))		{ alert("Please enter url.");	return false; } 
		if(hasWhiteSpace(url))	{ alert("Please remove space in url.");	return false; }
		if(specialcharecter(url)=='yes'){ alert("Please remove special character in url.");	return false; }
        
        var reminderFrequency = $("#reminderFrequency").val();
        
        if(reminderFrequency=="")
		{
		alert("Please enter reminder frequency");
		document.corporate.reminderFrequency.focus();
		return false;
		}
        if (isNaN(reminderFrequency)){
        alert("Only Numbers are allowed in reminder frequency");
		document.corporate.reminderFrequency.focus();
		return false;
        }
		
		if(document.corporate.urlStatus.value=="")
		{
		alert("Please select URL status");
		document.corporate.urlStatus.focus();
		return false;
		}		
		if(document.corporate.urlExpiryDate.value=="")
		{
		alert("Please enter url expiry date");
		document.corporate.urlExpiryDate.focus();
		return false;
		}	
        
	}
	var validURL=true;
	function updateLink(){

		var link=$('#company_url_link');
		var newURL="<?php echo @$companyUrl;?>religare/webenrol/company_"+$('#url').val();
		
		link.attr('href',newURL);
		link.html(newURL);
	}
	function gradevalidate(){
	if(document.gradeform.gradeVal.value=="")
		{
		alert("Please enter grade value");
		document.gradeform.gradeVal.focus();
		return false;
		}		
	}
</script>
<style>
.displaycl{
display:none;
}
</style>
<div id="middle">
  <div class="middle-heading-bg">
    <h1>Set-up Companies > <?=@$array_tblsite['companyName'];?></h1>
  </div>
  <div class="span9 columns" style="width:1000px; margin:0px;">
	<div id="tabs">
			<ul>
			<li><a href="#tabs-1">Basic Info.</a></li>
			<?php if($companyId!=''){?>
			<li><a href="#tabs-2">Grade</a></li>
			<li><a href="#tabs-3">Settings</a></li>						
			<li><a href="#tabs-4">Limits</a></li>
			<li><a href="#tabs-5" onclick="showSumInsured();">Rate as per SI</a></li>
			<li><a href="#tabs-6" onclick="showTopUpSumInsured();">Topup Sum Insured</a></li>
			<li><a href="#tabs-7">Export Employee Data</a></li>						
			<li><a href="#tabs-8">Import Employee</a></li>
			<?php }	?>
		</ul>
		<div id="tabs-1">
		<?php include_once("incBasciCorp.php"); ?>
		</div>
		<?php if($companyId!=''){?>
		<div id="tabs-2">
		<?php include_once("incGrade.php"); ?>
		</div>
		<div id="tabs-3">
		<?php include_once("incSettings.php"); ?>
		</div>
		<div id="tabs-4">
		<span id="limitsresult">
		<?php include_once("incLimits.php"); ?>
		</span>
		</div>
		<div id="tabs-5">
		<div class="tab-pane fade in" style="overflow:auto;">
		<form action="#" method="post" name="samapping" id="samapping">
		<span id="showsiresult">
		<?php include_once("incSumInsured.php"); ?>
		</span>
		</form>
		</div>
		</div>					
		<div id="tabs-6"><?php include_once("incTopupSumInsured.php"); ?></div>		
		<div id="tabs-7" style="text-align:right; padding-right:240px;"><a href="exportEmployeeData.php?companyId=<?=$companyId;?>" target="_blank" style="text-decoration:underline">Export Employee Data</a></div>
		<div id="tabs-8"><?php if(@$pagetype=='reviewimport') { include_once("uploadedEmpData.php"); } elseif(@$pagetype=='excelimport') { include_once("uploadExcelEmpData.php"); } else { include_once("incImportEmployee.php"); } ?></div>
		<?php }	?>
	</div>
  </div>
</div>
<div style="clear:both"></div>
<!--middle-->
<?php
if(@$array_tblsite['offerSumInsured']=='grade') {
?>
<script type="text/javascript">
$(".gradebasisresult").show();  $(".agebasisresult").hide();
</script>
<?php } if(@$array_tblsite['offerSumInsured']=='age') { ?>
<script type="text/javascript">
$(".gradebasisresult").hide();  $(".agebasisresult").show();
</script>
<?php } if(@$array_tblsite['offerTopUp']=='yes') { ?>
<script type="text/javascript">
$(".topupbasisresult").show();
</script>
<?php } ?>
<?php include_once('../inc/ft.php');	?>
</body></html>