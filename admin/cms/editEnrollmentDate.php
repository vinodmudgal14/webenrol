<?php
/* * ***************************************************************************
 * COPYRIGHT
 * Copyright 2016 Qualtech-consultants pvt ltd.
 * All rights reserved
 * DISCLAIMER
 * AUTHOR 
 * $Id: editEnrollmentDate.php,v 1.0 2016/10/04 16:30:20 Sumit $
 * $Author: sumit kumar $
 * Description : Used to extend the enrollment end date
 *
 * ************************************************************************** */

include('../conf/session.php');
include('../conf/conf.php');
include('../conf/fucts.php');
include('mailTemplate.php');
global $webRoot;
global $template;
$companyId = sanitize_data(@$_REQUEST['companyId']);
$batchNo = sanitize_data(@$_REQUEST['batchno']);
$extension = $_REQUEST['ext'];


if (isset($_REQUEST['enrolEndDate'])) {
	/*echo "<script type='text/javascript'>
alert('UnderConstruction !');
window.opener.location.reload(false);
window.close();
</script>";
	*/
    $enrolEndDate = $_REQUEST['enrolEndDate'];
    $enrolEndDate = strtotime($enrolEndDate);
    $batchno = $_REQUEST['batchno'];
    $companyId = $_REQUEST['companyId'];
    $extension = $_REQUEST['extension'];
	$linksendoption = $_REQUEST['linksendtype'];
    $sql = "UPDATE tbl_company_employee SET `enrolEndDate` = '" . $enrolEndDate . "',`extension`='" . $extension . "' WHERE batchno='" . $batchno . "' AND companyId='" . $companyId . "'";
    $result = mysql_query($sql);
    if ($result) {
        $employeeDetails = getBatchEmployeesDetails($companyId, $batchno);
        foreach ($employeeDetails as $empDetail) {
            $employeeUrl = base64_encode($empDetail['empEmail'] . ":" . $empDetail['empFirstName'] . ":" . $empDetail['empNo'] . ":" . $empDetail['companyId'] . ":" . $empDetail['enrolStartDate'] . ":" . $empDetail['enrolEndDate']);
            $empUrl = $webRoot . "company_" . $employeeUrl;
            $to = $empDetail['empEmail'];
            $mailDate = time();
            $subject = 'Employee Access';
            $message = 'Employee Message';
            $formattedTemplate = str_replace("##Name", $empDetail['empFirstName'], $template);
            $formattedTemplate = str_replace("##companyname", $empDetail['companyName'], $formattedTemplate);
            $formattedTemplate = str_replace("##employeetopupplanlink", $empUrl, $formattedTemplate);
            $formattedTemplate = str_replace("##date", date('M d,Y', $empDetail['enrolEndDate']), $formattedTemplate);
            $headers = "From: enrollment@religare.com \r\n" .
                    'MIME-Version: 1.0' . "\r\n" .
                    'Content-type: text/html; charset=iso-8859-1';
            $status = "Success";
            if (isset($linksendoption) && $linksendoption == 'manual') {
                $status = "Manual";
            } else {
                if (!mail($to, $subject, $formattedTemplate, $headers)) {
                    $status = "Mail Error";
                }
            }


            $sql = "INSERT INTO `tbl_employee_link` (`empNo`,`mailTo`,`mailSubject`,`mailContent` ,`mailDate`,`mailType`,`status`,`company_name`)VALUES ('" . $empDetail['empNo'] . "', '" . $empDetail['empEmail'] . "','$subject','$empUrl','$mailDate','1','$status','" . $empDetail['companyName'] . "')";

            mysql_query($sql);
        }
        echo "<script type='text/javascript'>
alert('Records updated successfully');
window.opener.location.reload(false);
window.close();
</script>";
    }
	
}

?>
<style>
    .middle-heading-bg {
        background: url("../images/green/administration-heading-bg.jpg") repeat-x scroll 0 0 transparent;
        line-height: 36px;
        margin-bottom: 5px;
        padding: 0 11px;
    }
    .middle-heading-bg h1 {
        background: url("../images/green/middle-heading-icon.png") no-repeat scroll left center transparent;
        color: #FFFFFF;
        display: block;
        font-family: Arial,Helvetica,sans-serif;
        font-size: 16px;
        font-weight: normal;
        padding-left: 20px;
    }
    .middle-data {
        background: none repeat scroll 0 0 #FFFFFF;
        border: 1px solid #E4E4E4;
    }
    .col-border_event {
        background: none repeat scroll 0 0 #F0F0F0;
        border-bottom: 1px solid #D9D9D9;
        border-right: 1px solid #D9D9D9;
        color: #000000;
        font-size: 12px;
        font-weight: normal;
        padding: 5px 11px;
    }
</style>
<script src="<?= _WWWROOT; ?>/js/jquery.js"></script>
<script src="<?= _WWWROOT; ?>/js/jquery.livequery.js"></script>
<script type="text/javascript" src="<?= _WWWROOT; ?>/js/ui.datepicker.js"></script>
<link href="<?= _WWWROOT; ?>/css/ui.datepicker.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
    $(document).ready(function ()
    {
        $("#enrolEndDate").datepicker({
            minDate: '0',
            yearRange: '2013:2020',
            dateFormat: "dd-mm-yy"
        });


    });
    function validate() {
        var enrolEndDate = $('#enrolEndDate').val();
        if (enrolEndDate == '') {
            alert("Please enter enrollment end date.");
            return false;
        }

    }
    function returntopage() {
        alert("Enrollment date updated successfully");
        window.close();
    }
</script>
<div id="middle">
    <div class="middle-heading-bg">
        <h1>Extend Enrollment Date</h1>
    </div> <!--middle heading bg-->
    <div style="border-bottom:none;" class="middle-data">
        <form name="wellnessform" id="wellnessform" action="" onsubmit="return validate();">
            <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                <tbody>
                    <tr>
                        <td align="left" class="col-border_event">&nbsp;&nbsp;Extend Enrollment Dates</td>
                        <td align="left" class="col-border_event">End Date&nbsp;&nbsp;:&nbsp;&nbsp;<input type="text" name="enrolEndDate" id="enrolEndDate" value="" readonly=""/></td>
                    </tr>
					<tr>
                        <td align="left" class="col-border_event">&nbsp;&nbsp;Link Send Option</td>
                        <td align="left" class="col-border_event"><input type="radio" name="linksendtype" value="manual" > Manual
                        <input type="radio" name="linksendtype" value="autotrigger" checked="checked"> System Trigger</td>
                    </tr>
                    <tr>
                        <td align="center" class="col-border_event" colspan="3">
                            <input type="hidden" name="companyId" id="companyId" value="<?php echo $companyId; ?>" />
                            <input type="hidden" name="batchno" id="employeeNo" value="<?php echo $batchNo; ?>" />
                            <input type="hidden" name="extension" value="<?php echo $extension; ?>" />
                            <input type="submit" value="Submit"></td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
</div>