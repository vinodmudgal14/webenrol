<?php
/* * ***************************************************************************
 * COPYRIGHT
 * Copyright 2016 Qualtech-consultants pvt ltd.
 * All rights reserved
 * DISCLAIMER
 * AUTHOR 
 * $Id: editEnrollment.php,v 1.0 2016/10/04 16:30:20 Sumit $
 * $Author: sumit kumar $
 * Description : Used to display the enrollment details and can edit the dates
 *
 * ************************************************************************** */

include('../inc/hd.php');
$companyId = $_REQUEST['companyId'];
$enrollmentStartDate = $_REQUEST['enrollmentStartDate'];
$enrollmentStartDate = strtotime($enrollmentStartDate);
$enrollmentEndDate = $_REQUEST['enrollmentEndDate'];
$enrollmentEndDate = strtotime($enrollmentEndDate);
$corporateslist = getTotalBatch($companyId);
?>
<style>
</style>
<script type="text/javascript" language="javascript" src="<?= _WWWROOT; ?>/js/data-jquery.js"></script>
<script type="text/javascript" language="javascript" src="<?= _WWWROOT; ?>/js/jquery.dataTables.js"></script>
<!--<script type="text/javascript" src="js/dataTables.scrollingPagination.js"></script>-->
<script type="text/javascript" src="<?= _WWWROOT; ?>/js/dataTables.scrollingPagination1.js"></script>
<script type="text/javascript" src="<?= _WWWROOT; ?>/js/jquery.popupWindow.js"></script>
<script type="text/javascript" src="<?= _WWWROOT; ?>/js/ui.datepicker.js"></script>
<link href="<?= _WWWROOT; ?>/css/ui.datepicker.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
    $(document).ready(function () {
        $('.wellnesspopup').popupWindow({
            height: 400,
            width: 700,
            top: 200,
            left: 350
        });
    });
    $(document).ready(function ()
    {
        $("#enrollmentStartDate").datepicker({
            yearRange: '2013:2020',
            dateFormat: "yy-mm-dd"
        });
        $("#enrollmentEndDate").datepicker({
            /* minDate: '0',*/
            yearRange: '2013:2020',
            dateFormat: "yy-mm-dd"
        });

    });
    function validate() {

        var enrollmentStartDate = $('#enrollmentStartDate').val();
        var enrollmentStartDate = new Date(enrollmentStartDate);
        var enrollmentEndDate = $('#enrollmentEndDate').val();
        var enrollmentEndDate = new Date(enrollmentEndDate);
        if (enrollmentStartDate == '') {
            alert("Please enter Enroll start date from .");
            return false;
        }
        if (enrollmentEndDate == '') {
            alert("Please enter Enroll start date to.");
            return false;
        } else {
            if (enrollmentStartDate > enrollmentEndDate) {
                alert("Please enter end date greater than start date.");
                return false;
            }
        }

    }
</script>

<div id="middle">
    <div class="middle-heading-bg">
        <h1>Manage Employee Enrollment Dates</h1>

    </div>
    <div class="middle-data" style="border-bottom:none;">
        <table align="center" width="100%" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td align="center" class="col-border-1"><?php echo $corporateslist[0]['companyName'];?></td>
            </tr>
        </table>
    </div>


    <div class="middle-data">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="display" id="example">
            <thead>
                <tr>
                    <th width="5%" align="center" class="col-border-1">Sr No.</th>
					<th width="5%" align="center" class="col-border-1">Batch No.</th>
                    <th width="15%" align="left" class="col-border-1">Company Name</th>
                    <th width="15%" align="left" class="col-border-1">Enrollment Start Date</th>
                    <th width="15%" align="left" class="col-border-1">Enrollment End Date</th>
                    <th width="15%" align="left" class="col-border-1">No of Employee's</th>
                    <th width="15%" align="left" class="col-border-1">Total Extension</th>
                    <th width="15%" class="col-border-1">Action</th>
                </tr>
            </thead>
            <tbody>

                <?php
                if (isset($corporateslist) && $corporateslist[0]['batchno'] != '') {
                    $s = 0;

                    while ($s < count($corporateslist)) {
                        $batchDetails = getBatchDetails($companyId, $corporateslist[$s]['batchno']);
                        $totalextension = $batchDetails[0]['extension'];
                        if ($totalextension == '') {
                            $extension = 1;
                        } else {
                            $extension = $totalextension + 1;
                        }
                        ?>

                        <tr class="gradeX">
                            <td align="center"  class="col-border-1"><?php echo $s + 1; ?></td>	
							<td align="center"  class="col-border-1">Batch-<?php echo $corporateslist[$s]['batchno']; ?></td>							
                            <td align="left"  class="col-border-1"><?php echo $batchDetails[0]['companyName']; ?></td>
                            <td align="left"  class="col-border-1"><?php echo date('d M Y', $batchDetails[0]['enrolStartDate']); ?></td>
                            <td align="left"  class="col-border-1"><?php echo date('d M Y', $batchDetails[0]['enrolEndDate']); ?></td>
                            <td align="left"  class="col-border-1"><?php echo $batchDetails[0]['totalrow']; ?></td>
                            <td align="left"  class="col-border-1"><?php echo $totalextension ? $totalextension : 0; ?>&nbsp;Times</td>			
                            <td align="center" class="col-border-1"><a href="changeUrlStatus.php?companyId=<?php echo $companyId;?>&batchno=<?php echo $corporateslist[$s]['batchno'];?>" class="edit wellnesspopup" style="float:left;">Change URL Status</a>&nbsp;|&nbsp;<a href="editEnrollmentDate.php?companyId=<?php echo $companyId ?>&batchno=<?php echo $corporateslist[$s]['batchno'] ?>&ext=<?php echo $extension; ?>" class="edit wellnesspopup" style="float:left;">Extend Enrollment</a></td>		
                        </tr>
                        <?Php
                        $s++;
                    }
                } else {
                    ?>
                    <tr class="gradeX">
                        <td align="center"  class="col-border-1" ></td>
                        <td align="center"  class="col-border-1" ></td>
                        <td align="center"  class="col-border-1" ></td>
                        <td align="center"  class="col-border-1" ><?php echo "No Batch available"; ?></td>
                        <td align="center"  class="col-border-1" ></td>
                        <td align="center"  class="col-border-1" ></td>
                        <td align="center"  class="col-border-1" ></td>
                    </tr>
                <?php } ?>

            </tbody>
            <tfoot>
                <tr>
                    <th align="center" class="col-border-1">Sr.No.</th>
                    <th align="left" class="col-border-1">Company Name</th>
                    <th align="left" class="col-border-1">Enrollment Start Date</th>
                    <th align="left" class="col-border-1">Enrollment End Date</th>	
                    <th align="left" class="col-border-1">No of Employee's</th>	
                    <th align="left" class="col-border-1">Total Extension</th>						
                    <th class="col-border-1">Action</th>
                </tr>

            </tfoot>

        </table>
        <table>
        </table>

    </div> <!--middle data-->
</div><div style="clear:both"></div> <!--middle-->
<?php include('../inc/ft.php'); ?>
</body>
</html>
