<?php  
	include_once("../conf/session.php");
	include_once("../conf/conf.php");
	include_once("../conf/fucts.php");
	set_time_limit(20);
	$query="select * from `tbl_company_employee`  WHERE `isDeleted` = 'no' AND `companyId` = '$companyId' ORDER BY `id` DESC";
	$result=mysql_query($query) or die (mysql_error());
	
	include 'PHPExcel.php';
		/** PHPExcel_Writer_Excel2007 */
	include 'PHPExcel/Writer/Excel5.php';
	// Create new PHPExcel object

$objPHPExcel = new PHPExcel();

// Set properties
$objPHPExcel->getProperties()->setTitle("Employee data");

// Set row height
$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(25);
$objPHPExcel->getActiveSheet()->getRowDimension(2)->setRowHeight(20);

// Set column width
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(20);

$objPHPExcel->getActiveSheet()->duplicateStyleArray(
array(
'font' => array(
'name'         => 'Times New Roman',
'bold'         => true,
'italic'    => false,
'size'        => 12
),
'borders' => array(
'top'        => array('style' => PHPExcel_Style_Border::BORDER_DOUBLE),
'bottom'    => array('style' => PHPExcel_Style_Border::BORDER_DOUBLE),
'left'        => array('style' => PHPExcel_Style_Border::BORDER_DOUBLE),
'right'        => array('style' => PHPExcel_Style_Border::BORDER_DOUBLE)
),
'alignment' => array(
'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
'wrap'       => true
)
),
'A1:U1'
);

// Add some data
$objPHPExcel->setActiveSheetIndex(0);
//$objPHPExcel->getActiveSheet()->SetCellValue('A1',"Company List");
$objPHPExcel->getActiveSheet()->SetCellValue('A1',"CHDRNUM");
$objPHPExcel->getActiveSheet()->SetCellValue('B1',"TXNTY");
$objPHPExcel->getActiveSheet()->SetCellValue('C1',"EMPNO");
$objPHPExcel->getActiveSheet()->SetCellValue('D1',"LSURNAME");
$objPHPExcel->getActiveSheet()->SetCellValue('E1',"LGIVNAME");
$objPHPExcel->getActiveSheet()->SetCellValue('F1',"DPNTTYP");
$objPHPExcel->getActiveSheet()->SetCellValue('G1',"CLTSEX");
$objPHPExcel->getActiveSheet()->SetCellValue('H1',"SALUT");
$objPHPExcel->getActiveSheet()->SetCellValue('I1',"CLTDOB");
$objPHPExcel->getActiveSheet()->SetCellValue('J1',"PLANNO");
$objPHPExcel->getActiveSheet()->SetCellValue('K1',"CLTPCODE");
$objPHPExcel->getActiveSheet()->SetCellValue('L1',"EFFDATE");
$objPHPExcel->getActiveSheet()->SetCellValue('M1',"FMLYRELN");
$objPHPExcel->getActiveSheet()->SetCellValue('N1',"DATATYPE");
$objPHPExcel->getActiveSheet()->SetCellValue('O1',"HEADCNT");
$objPHPExcel->getActiveSheet()->SetCellValue('P1',"REFIND");
$objPHPExcel->getActiveSheet()->SetCellValue('Q1',"Sum Insured");
$objPHPExcel->getActiveSheet()->SetCellValue('R1',"Premium");
$objPHPExcel->getActiveSheet()->SetCellValue('S1',"Topup Sum Insured");
$objPHPExcel->getActiveSheet()->SetCellValue('T1',"Topup Premium");
$objPHPExcel->getActiveSheet()->SetCellValue('U1',"Policy Type");
$objPHPExcel->getActiveSheet()->SetCellValue('V1',"Login Status");

$k=2;
while($row=mysql_fetch_assoc($result)){
//$empsiVal = getName('tbl_sum_insured','id',$row['sumInsuredId'],'sumInsured');
//$emptopupsiVal = getName('tbl_topup_sum_insured','id',$row['empTopupSIId'],'sumInsured');
if($row['lastLogin']>100){ $loginstatus = 'Yes'; } else { $loginstatus = 'No'; }

$objPHPExcel->getActiveSheet()->SetCellValue('A'.$k,'');
$objPHPExcel->getActiveSheet()->SetCellValue('B'.$k,'A');
$objPHPExcel->getActiveSheet()->SetCellValue('C'.$k,$row['empNo']);
$objPHPExcel->getActiveSheet()->SetCellValue('D'.$k,$row['empLastName']);
$objPHPExcel->getActiveSheet()->SetCellValue('E'.$k,$row['empFirstName']);
$objPHPExcel->getActiveSheet()->SetCellValue('F'.$k,'MMBR');
$objPHPExcel->getActiveSheet()->SetCellValue('G'.$k,$row['gender']);
$objPHPExcel->getActiveSheet()->SetCellValue('H'.$k,$row['empSalutation']);
$objPHPExcel->getActiveSheet()->SetCellValue('I'.$k,date('d-M-Y',$row['dob']));
$objPHPExcel->getActiveSheet()->SetCellValue('J'.$k,$row['sumInsured']);
$objPHPExcel->getActiveSheet()->SetCellValue('K'.$k,$row['sumInsured']);
$objPHPExcel->getActiveSheet()->SetCellValue('L'.$k,date('d-M-Y',$row['createdOn']));
$objPHPExcel->getActiveSheet()->SetCellValue('M'.$k,$row['relationType']);
$objPHPExcel->getActiveSheet()->SetCellValue('N'.$k,'M');
$objPHPExcel->getActiveSheet()->SetCellValue('O'.$k,'N');
$objPHPExcel->getActiveSheet()->SetCellValue('P'.$k,'N');
$objPHPExcel->getActiveSheet()->SetCellValue('Q'.$k,$empsiVal);
$objPHPExcel->getActiveSheet()->SetCellValue('R'.$k,$row['empPremium']);
$objPHPExcel->getActiveSheet()->SetCellValue('S'.$k,$emptopupsiVal);
$objPHPExcel->getActiveSheet()->SetCellValue('T'.$k,$row['empTopupPremium']);
$objPHPExcel->getActiveSheet()->SetCellValue('U'.$k,'I');
$objPHPExcel->getActiveSheet()->SetCellValue('V'.$k,$loginstatus);

	$k++;
	$empid = $row['id'];
	$query1="select * from `tbl_employee_family`  WHERE `isDeleted` = 'no' AND `companyId` = '$companyId' AND `empId` = '$empid' ORDER BY `id` DESC";
	$result1=mysql_query($query1) or die (mysql_error());
	while($row1=mysql_fetch_assoc($result1)){
		if($row1['salutation']=='MR') { $gender = 'M'; }   else { $gender = 'F';  }
		//$siVal = getName('tbl_sum_insured','id',$row1['sumInsuredId'],'sumInsured');
		//$topupsiVal = getName('tbl_topup_sum_insured','id',$row1['topSumInsuredId'],'sumInsured');
		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$k,'');
		$objPHPExcel->getActiveSheet()->SetCellValue('B'.$k,'A');
		$objPHPExcel->getActiveSheet()->SetCellValue('C'.$k,$row['empNo']);
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$k,$row1['lastName']);
		$objPHPExcel->getActiveSheet()->SetCellValue('E'.$k,$row1['firstName']);
		$objPHPExcel->getActiveSheet()->SetCellValue('F'.$k,'SPSE');
		$objPHPExcel->getActiveSheet()->SetCellValue('G'.$k,$gender);
		$objPHPExcel->getActiveSheet()->SetCellValue('H'.$k,$row1['salutation']);
		$objPHPExcel->getActiveSheet()->SetCellValue('I'.$k,date('d-M-Y',$row1['fdob']));
		$objPHPExcel->getActiveSheet()->SetCellValue('J'.$k,$siVal);
		$objPHPExcel->getActiveSheet()->SetCellValue('K'.$k,$siVal);
		$objPHPExcel->getActiveSheet()->SetCellValue('L'.$k,date('d-M-Y',$row['createdOn']));
		$objPHPExcel->getActiveSheet()->SetCellValue('M'.$k,$row1['relation']);
		$objPHPExcel->getActiveSheet()->SetCellValue('N'.$k,'M');
		$objPHPExcel->getActiveSheet()->SetCellValue('O'.$k,'N');
		$objPHPExcel->getActiveSheet()->SetCellValue('P'.$k,'N');
		$objPHPExcel->getActiveSheet()->SetCellValue('Q'.$k,$siVal);
		$objPHPExcel->getActiveSheet()->SetCellValue('R'.$k,$row1['sumInsuredPremium']);
		$objPHPExcel->getActiveSheet()->SetCellValue('S'.$k,$topupsiVal);
		$objPHPExcel->getActiveSheet()->SetCellValue('T'.$k,$row1['topSumInsuredPremium']);	
		$objPHPExcel->getActiveSheet()->SetCellValue('U'.$k,'F');
		$objPHPExcel->getActiveSheet()->SetCellValue('V'.$k,'');
		$k++;
	}
//$k++;
}
// Rename sheet
header('Content-Disposition: attachment;filename=employeedata.xls ');
$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
$objWriter->save('php://output');
?>