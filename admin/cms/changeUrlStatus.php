<?php
/* * ***************************************************************************
 * COPYRIGHT
 * Copyright 2016 Qualtech-consultants pvt ltd.
 * All rights reserved
 * DISCLAIMER
 * AUTHOR 
 * $Id: changeUrlStatus.php,v 1.0 2016/12/01 12:30:20 Sumit $
 * $Author: sumit kumar $
 * Description : Used to deactivate the Url status of batch
 *
 * ************************************************************************** */

include('../conf/session.php');
include('../conf/conf.php');
include('../conf/fucts.php');
$companyId = sanitize_data(@$_REQUEST['companyId']);
$batchNo = sanitize_data(@$_REQUEST['batchno']);
$urlStatusofbatch = getUrlStatusofBatch($companyId,$batchNo);
$urlStatus = $urlStatusofbatch[0]['urlStatus'];

///print"<pre>";print_r($urlStatus);die;
if (isset($_REQUEST['urlstatus'])) {
    //print"<pre>";print_r($_REQUEST);die;
    $urlStatus = $_REQUEST['urlstatus'];
    $batchno = $_REQUEST['batchno'];
    $companyId = $_REQUEST['companyId'];
    $sql = "UPDATE tbl_company_employee SET `urlStatus` = '" . $urlStatus . "' WHERE batchno='" . $batchno . "' AND companyId='" . $companyId . "'";
    $result = mysql_query($sql);
    if ($result) {
       echo "<script type='text/javascript'>
alert('Records updated successfully');
window.opener.location.reload(false);
window.close();
</script>";
        }
        
    }

?>
<style>
    .middle-heading-bg {
        background: url("../images/green/administration-heading-bg.jpg") repeat-x scroll 0 0 transparent;
        line-height: 36px;
        margin-bottom: 5px;
        padding: 0 11px;
    }
    .middle-heading-bg h1 {
        background: url("../images/green/middle-heading-icon.png") no-repeat scroll left center transparent;
        color: #FFFFFF;
        display: block;
        font-family: Arial,Helvetica,sans-serif;
        font-size: 16px;
        font-weight: normal;
        padding-left: 20px;
    }
    .middle-data {
        background: none repeat scroll 0 0 #FFFFFF;
        border: 1px solid #E4E4E4;
    }
    .col-border_event {
        background: none repeat scroll 0 0 #F0F0F0;
        border-bottom: 1px solid #D9D9D9;
        border-right: 1px solid #D9D9D9;
        color: #000000;
        font-size: 12px;
        font-weight: normal;
        padding: 5px 11px;
    }
</style>
<script src="<?= _WWWROOT; ?>/js/jquery.js"></script>
<script src="<?= _WWWROOT; ?>/js/jquery.livequery.js"></script>
<script type="text/javascript" src="<?= _WWWROOT; ?>/js/ui.datepicker.js"></script>
<link href="<?= _WWWROOT; ?>/css/ui.datepicker.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
    $(document).ready(function ()
    {
        $("#enrolEndDate").datepicker({
            minDate: '0',
            yearRange: '2013:2020',
            dateFormat: "dd-mm-yy"
        });


    });
    function validate() {
        var enrolEndDate = $('#enrolEndDate').val();
        if (enrolEndDate == '') {
            alert("Please enter enrollment end date.");
            return false;
        }

    }
    function returntopage() {
        alert("Enrollment date updated successfully");
        window.close();
    }
</script>
<div id="middle">
    <div class="middle-heading-bg">
        <h1>Change URL Status of Batch</h1>
    </div> <!--middle heading bg-->
    <div style="border-bottom:none;" class="middle-data">
        <form name="wellnessform" id="wellnessform" action="" >
            <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                <tbody>
                    <tr>
                        <td align="left" class="col-border_event">&nbsp;&nbsp;URL Status</td>
                        <td align="left" class="col-border_event"><input type="radio" name="urlstatus" value="open" <?php if($urlStatus=='open'){echo "checked";};?> > Active
                         <input type="radio" name="urlstatus" value="lock" <?php if($urlStatus=='lock'){echo "checked";};?>> De-Active</td>

                    </tr>

                    <tr>
                        <td align="center" class="col-border_event" colspan="3">
                            <input type="hidden" name="companyId" id="companyId" value="<?php echo $companyId; ?>" />
                            <input type="hidden" name="batchno" id="employeeNo" value="<?php echo $batchNo; ?>" />
                          
                            <input type="submit" value="Submit"></td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
</div>