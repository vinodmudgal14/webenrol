<?php
include('../conf/session.php');
include('../conf/conf.php');
include('../conf/fucts.php');
$prefix = substr(time(), 5, 5);
$companyId = sanitize_data(@$_REQUEST['companyId']);
$companydetails = companydetails($companyId);
/* echo '<pre>';
  print_r($companydetails); */
$offerSumInsured = $companydetails[0]['offerSumInsured'];
$agegrouplist = ageGroupList();
if ($offerSumInsured != 'age')
{
    $gradelist = gradeList($companyId);
    $gradesuminsuredmappinglist = gradeSumInsuredMapList($companyId);
    if ($companydetails[0]['policyType'] == 'fixed')
    {
        ?> 
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="display" id="example">
            <thead>
                <tr>
                    <th width="5%" align="left" class="col-border-1">Grades</th>
                    <?php
                    $m = 0;
                    while ($m < count($gradesuminsuredmappinglist))
                    {
                        ?>
                        <th width="5%" align="center" class="col-border-1"><?php echo $gradesuminsuredmappinglist[$m]['siAmount']; ?></th>
                    <?Php $m++;
                } ?>	
                </tr>
            </thead>
            <tbody>	
                    <?php
                    $s = 0;
                    while ($s < count($gradelist))
                    {
                        ?>
                    <tr class="gradeX">		
                        <td align="left"  class="col-border-1"><?php echo $gradelist[$s]['grade']; ?></td> 
            <?php
            $m = 0;
            while ($m < count($gradesuminsuredmappinglist))
            {
                $chkmap = chkGradeSIMapData($gradelist[$s]['id'], $gradesuminsuredmappinglist[$m]['sumInsuredId'], $companyId);
                ?>
                            <td align="center"  class="col-border-1">
                                <input name="satype[]" type="hidden" size="5"	value="<?= $gradelist[$s]['id']; ?>_<?= $gradesuminsuredmappinglist[$m]['sumInsuredId']; ?>">
                                <input type="text" name="sa_<?= $gradelist[$s]['id']; ?>_<?= $gradesuminsuredmappinglist[$m]['sumInsuredId']; ?>" id="sa_<?= $gradelist[$s]['id']; ?>_<?= $gradesuminsuredmappinglist[$m]['sumInsuredId']; ?>" value="<?= getGradeSIMapData($gradelist[$s]['id'], $gradesuminsuredmappinglist[$m]['sumInsuredId'], $companyId); ?>" style="width:70px;" <?php if ($chkmap < 1)
                { ?>disabled="disabled" <?php } ?>/>			
                            </td>	
                <?Php $m++;
            } ?>		
                    </tr>
            <?Php $s++;
        } ?>	
                <tr>
                    <td class="col-border_event">&nbsp;</td>
                    <td colspan="<?= count($suminsuredlist); ?>" class="col-border_event" align="right">
                        <input type="hidden" name="companyId" id="companyId" value="<?= $companyId; ?>"/>
                        <input type="hidden" name="simapvariable" id="simapvariable" value="grade"/>
                        <input type="hidden" name="policyType" id="policyType" value="<?= $companydetails[0]['policyType']; ?>"/>
                        <input type="hidden" name="insuranceType" id="insuranceType" value="<?= $companydetails[0]['insuranceType']; ?>"/>
                        <input type="button" value="Submit" onclick="updateSIPremium()" /></td>
                </tr>								
            </tbody>						
        </table>
    <?php }
    else
    { ?>
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="display" id="example">
            <thead>
                <tr>
                    <th width="5%" align="center" class="col-border-1">Age Group</th>
        <?php
        $m = 0;
        while ($m < count($gradesuminsuredmappinglist))
        {
            ?>
                        <th width="5%" align="center" class="col-border-1"><?php echo $gradesuminsuredmappinglist[$m]['siAmount']; ?></th>
            <?Php $m++;
        } ?>	
                </tr>
            </thead>
            <tbody>	
                <?php
                $s = 0;
                while ($s < count($agegrouplist))
                {
                    ?>
                    <tr class="gradeX">		
                        <td align="center"  class="col-border-1"><?php echo $agegrouplist[$s]['ageGroupName']; ?></td> 
            <?php
            $m = 0;
            while ($m < count($gradesuminsuredmappinglist))
            {
                ?>
                            <td align="center"  class="col-border-1">
                                <input name="satype[]" type="hidden" size="5"	value="<?= $agegrouplist[$s]['id']; ?>_<?= $gradesuminsuredmappinglist[$m]['sumInsuredId']; ?>">
                                <input type="text" name="sa_<?= $agegrouplist[$s]['id']; ?>_<?= $gradesuminsuredmappinglist[$m]['sumInsuredId']; ?>" id="sa_<?= $agegrouplist[$s]['id']; ?>_<?= $gradesuminsuredmappinglist[$m]['sumInsuredId']; ?>" value="<?= getGradeSAMappingData($agegrouplist[$s]['id'], $gradesuminsuredmappinglist[$m]['sumInsuredId'], $companyId); ?>" style="width:70px;"/>			
                            </td>	
                <?Php $m++;
            } ?>		
                    </tr>
            <?Php $s++;
        } ?>	
                <tr>
                    <td class="col-border_event">&nbsp;</td>
                    <td colspan="<?= count($suminsuredlist); ?>" class="col-border_event" align="right">
                        <input type="hidden" name="companyId" id="companyId" value="<?= $companyId; ?>"/>
                        <input type="hidden" name="simapvariable" id="simapvariable" value="grade"/>
                        <input  type="button" value="Submit" onclick="updateSIPremium()" /></td>
                </tr>		
            </tbody>
        </table>
            <?php
            }
        }
        else
        {
            $suminsuredmappinglist = sumInsuredMapList($companyId);
            if ($companydetails[0]['policyType'] == 'fixed')
            {
                ?>
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="display" id="example">
            <thead>
                <tr>
                    <th width="10%" align="left" class="col-border-1">Sum Insured</th>							
                    <th width="10%" align="center" class="col-border-1">Premium</th>

                </tr>
            </thead>
            <tbody>	
        <?php
        $s = 0;
        while ($s < count($suminsuredmappinglist))
        {
            ?>
                    <tr class="gradeX">		
                        <td align="left"  class="col-border-1"><b><?php echo $suminsuredmappinglist[$s]['sumInsured']; ?></b></td>							
                        <td align="center"  class="col-border-1">
                            <input name="gensitype[]" type="hidden" size="5" value="<?= $suminsuredmappinglist[$s]['id']; ?>">
                            <input type="text" name="gensi_<?= $suminsuredmappinglist[$s]['id']; ?>" id="gensi_<?= $suminsuredmappinglist[$s]['sumInsuredId']; ?>" value="<?= getAgeSIMapData($suminsuredmappinglist[$s]['id'], $companyId); ?>"/>			
                        </td>
                    </tr>	
                        <?Php $s++;
                    } ?>		
                <tr>
                    <td class="col-border_event">&nbsp;</td>
                    <td colspan="<?= count($suminsuredlist); ?>" class="col-border_event" align="right">
                        <input type="hidden" name="companyId" id="companyId" value="<?= $companyId; ?>"/>
                        <input type="hidden" name="simapvariable" id="simapvariable" value="age"/>
                        <input type="hidden" name="policyType" id="policyType" value="<?= $companydetails[0]['policyType']; ?>"/>
                        <input type="hidden" name="insuranceType" id="insuranceType" value="<?= $companydetails[0]['insuranceType']; ?>"/>
                        <input  type="button" value="Submit" onclick="updateSIPremium()" /></td>
                </tr>									
            </tbody>
        </table>
                <?php }
                else
                { ?>
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="display" id="example">
            <thead>
                <tr>
                    <th width="5%" align="center" class="col-border-1">Age Group</th>
                <?php
                $m = 0;
                while ($m < count($suminsuredmappinglist))
                {
                    ?>
                        <th width="5%" align="center" class="col-border-1"><?php echo $suminsuredmappinglist[$m]['sumInsured']; ?></th>
            <?Php $m++;
        } ?>	
                </tr>
            </thead>
            <tbody>	
        <?php
        $s = 0;
        while ($s < count($agegrouplist))
        {
            ?>
                    <tr class="gradeX">		
                        <td align="center"  class="col-border-1"><?php echo $agegrouplist[$s]['ageGroupName']; ?></td> 
            <?php
            $m = 0;
            while ($m < count($suminsuredmappinglist))
            {
                ?>
                            <td align="center"  class="col-border-1">
                                <input name="satype[]" type="hidden" size="5"	value="<?= $agegrouplist[$s]['id']; ?>_<?= $suminsuredmappinglist[$m]['id']; ?>">
                                <input type="text" name="sa_<?= $agegrouplist[$s]['id']; ?>_<?= $suminsuredmappinglist[$m]['id']; ?>" id="sa_<?= $agegrouplist[$s]['id']; ?>_<?= $suminsuredmappinglist[$m]['id']; ?>" value="<?= getSAMappingData($agegrouplist[$s]['id'], $suminsuredmappinglist[$m]['id'], $companyId); ?>" style="width:70px;"/>			
                            </td>	
                <?Php $m++;
            } ?>		
                    </tr>
            <?Php $s++;
        } ?>	
                <tr>
                    <td class="col-border_event">&nbsp;</td>
                    <td colspan="<?= count($suminsuredlist); ?>" class="col-border_event" align="right">
                        <input type="hidden" name="companyId" id="companyId" value="<?= $companyId; ?>"/>
                        <input type="hidden" name="simapvariable" id="simapvariable" value="age"/>
                        <input  type="button" value="Submit" onclick="updateSIPremium()" /></td>
                </tr>		
            </tbody>
        </table>
    <?php }
} ?>