<?php 
include_once('../inc/hd.php');
$id		=	sanitize_data(isNumeric(@$_REQUEST['id']));
$mode	=	sanitize_data(isVarChar(@$_REQUEST['mode']));
if(isset($_REQUEST["mode"])=="Edit"){
	$rs_tblsite1 = sprintf("SELECT * FROM `tbl_topup_sum_insured` WHERE `id` = '%s' LIMIT 1",
	mysql_real_escape_string($id));
	$rs_tblsite=mysql_query($rs_tblsite1);
	$array_tblsite=mysql_fetch_array($rs_tblsite);
}else{
	$array_tblsite["linkType"]="Description";
	$array_tblsite["parentId"]='';
}
?>
<link rel="stylesheet" href="<?=_WWWROOT;?>/css/colorpicker.css" type="text/css" />
<link rel="stylesheet" media="screen" type="text/css" href="<?=_WWWROOT;?>/css/layout.css" />
<script type="text/javascript" src="<?=_WWWROOT;?>/js/colorpicker.js"></script>
<script type="text/javascript" src="<?=_WWWROOT;?>/js/layout.js?ver=1.0.2"></script>
<script>
function validate(){
		if(document.SumInsured.sumInsured.value=="")
		{
		alert("Please enter the Sum Insured");
		document.SumInsured.sumInsured.focus();
		return false;
		}
	}
</script>
<div id="middle">
  <div class="middle-heading-bg">
    <h1>Add / Edit Sum Insured</h1>
  </div>
  <!--middle heading bg-->
  <form action="writeTopupSumInsured.php" method="post" enctype="multipart/form-data" name="SumInsured" id="SumInsured" onSubmit="return validate();">
    <?php if (@$_REQUEST['successmsg'] != '') { ?>	
                    <div style="padding-bottom:5px; text-align:center; color:red;">
                        <?php echo sanitize_data($_REQUEST['successmsg']); ?></div>
     <?php } ?>
      <div class="middle-data">
      <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
        <tbody>         
          <tr>
            <td  align="left" class="col-border_event">Sum Insured</td>
            <td  colspan=2 class="col-border_event"><input style="width:335px;" id="sumInsured" name="sumInsured" type="text" class="login-textfield" value="<?php if(isset($array_tblsite["sumInsured"]))echo $array_tblsite["sumInsured"];?>" AUTOCOMPLETE="OFF"/></td>
          </tr>         
          <tr>
            <input  name="mode" type="hidden" id="mode" value="<?php echo @$mode;?>">
            <input  name="id" type="hidden" id="id" value="<?php echo @$id;?>">
            <td class="col-border_event">&nbsp;</td>
            <td colspan=2 class="col-border_event"><input  type="submit" value="Submit" /></td>
          </tr>
        </tbody>
      </table>
    </div>
    <!--middle data-->
  </form>
</div>
<div style="clear:both"></div>
<!--middle-->
<?php include_once('../inc/ft.php');?>
</body></html>