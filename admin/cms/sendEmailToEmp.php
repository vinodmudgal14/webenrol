<?php 
/*****************************************************************************
* COPYRIGHT
* Copyright 2013 Catabatic Automation Technology Pvt Ltd.
* All rights reserved
*
* DISCLAIMER
*
*
* AUTHOR
*
* $Id: employee_bulk_upload.php,v 1.0 2013/04/16 11:33:29 amit dubey Exp $
* $Author: amit dubey $
*
****************************************************************************/
include_once("conf/session.php");		//including session file 
include_once("conf/conf.php");			//including configuration file
include_once("conf/fucts.php"); 		//including function file
$pageType="Employee";					//page type to focuse menu tab
@$defaultexpanded=0;
$compId=sanitize_data(@$_REQUEST['compId']);	//requested company id 
error_reporting(0);
include("inc/inc.hd.php"); 						//include Header file
  if(isset($_REQUEST["mode"])=="Edit"){  //check request mode to edit and  fetch data from database	
			$array_tblsite=fetchListById('CRMEMPLOYEELOGIN','LOGINID',$id);//function to fetch records
			if(count($array_tblsite) < 1) {
				header('location:dashboard.php'); //locate to dashboard
				}
		}

if(@$_POST['UploadAppointment']!='' && isset($_POST['UploadAppointment'])){ //check condition to insert or update data into/from database
			$prefix=substr(time(),5,5);
$companyId=@$_POST['companyId'];
	if(@$_FILES['appointment_bulk']['name']!=""){
				$ext = pathinfo($_FILES['appointment_bulk']['name'], PATHINFO_EXTENSION);
				if($ext == 'xls') {
				
			$prefix=substr(time(),5,5);
			$appointment_bulk = basename($prefix.toHAndleSpace($_FILES['appointment_bulk']['name'])); 
			$target_path ="appointmentxls/";
			$appointment_bulk =touploadItem('appointment_bulk' ,$target_path, $appointment_bulk);
			
	$newname ='appointmentxls/'.@$appointment_bulk;								//assigning file name to variable
	require_once 'excel_reader2.php';											//include excel file
	$data = new Spreadsheet_Excel_Reader($newname);
	$datacol = $data->sheets[0]["cells"];
	$error =0; 
	 
	$defined_size = "19";
	$j=0;
	$empNoArr = array();
	for($m=2;$m<=count($data->sheets[0]["cells"]);$m++) {
		$empNoArr[]	=  trim($data->sheets[0]["cells"][$m][1]);
	}
	$error='';
	 if(count($data->sheets[0]["cells"]) <= 1 ){
	  $error_empty="file cannot be empty";
	  $error[]=1;
	 }
	for ($row=2;$row<=$data->rowcount();$row++) {
	for ($col=1;$col<=$data->colcount();$col++) {
	 $fieldvalues=trim($data->val($row,$col));
	 if(empty($fieldvalues) && $fieldvalues==''){
	 	 $error_empty="All fields are mandatory";
		 $error[]="All fields are mandatory";
	 }
	 
	}
	}
$employeenoarr = array_count_values($empNoArr);

		for($i=2;$i<=count($data->sheets[0]["cells"]);$i++) {
			$policyNumber			=  trim($data->sheets[0]["cells"][$i][3]);	
			$emailid					=  trim($data->sheets[0]["cells"][$i][4]);
			$password				=  trim($data->sheets[0]["cells"][$i][5]);

			$final_arr[$j]['policyNumber']		= $policyNumber;
			$final_arr[$j]['emailid']				= $emailid;
			$final_arr[$j]['password']			= $password;	
			
		$j++;	
		
	}	

	
	} else { $error_msg="Please Upload only xls format";  }	}  else {
	 $error_msg="Please Upload File";				//error msg prompt to upload file 
	}	
}
if(@$_POST['Confirm']!='' && isset($_POST['Confirm'])){
$final_arr 	= unserialize(urldecode($_POST["final_arr"]));
if(@$firstColumn=='1'){ $i='1'; } else { $i='0'; }
for($i=$i;$i<sizeof($final_arr);$i++)
	{	
	
		$policyNumber 	= $final_arr[$i]['policyNumber'];	
		$emailid		= $final_arr[$i]['emailid'];
		$password 		= $final_arr[$i]['password'];	
		$compTitle		= $_REQUEST['compTitle'];
			SendMailToEmployeeByPolicy($emailid,$password,$compTitle);	
	}

}
?>
<!--Middle Start Here-->
<div class="mid_container">
  <script type="text/javascript" src="js/jquery.pajinate.js"></script>
  <script type="text/javascript">
        	$(document).ready(function(){
				$('#paging_container9').pajinate({
					num_page_links_to_display : 3,
					items_per_page : 10,
                    wrap_around: true,
                    show_first_last: false
				});
			});               

		$(document).ready(function(){
				$('#paging_container11').pajinate();
		});      
		</script>

<div class="innerBody fl">
	<!--Left Nav Start Here-->
<?php include('inc/inc.leftnav.php'); ?>
    <!--Left Nav End Here-->
    
    <!--Right Container Start Here-->
    <div class="rightBody fl">
    <div class="rightBody_head fl"><span>Employee &raquo; </span><a href="download_appointment.php?fileName=Sample_employee.xls" class="download_btn fr">Download Template</a>&nbsp;Bulk Upload</div>
    
    
    <div class="srchBar fl padding_top">
    <form action="" method="post" enctype="multipart/form-data" name="UploadFixedApp">
    <div class="healthformBody fl"><label>Browse File :</label>
    <div class="health_label fl">
      <table width="70%" border="0" cellspacing="0" cellpadding="0" class="fl">
        <tr>
          <td width="57%"><input type="file" id="fileField" name="appointment_bulk"><span style="color:#FF0000;"><?php echo $error_msg?$error_msg:''; ?></span></td>
          <td width="43%"><input type="submit" class="gobtn fl" value="Upload" id="show_button" name="UploadAppointment"></td>
        </tr>
      </table>
      </div>
	  <div class="healthformBody fl"><label>&nbsp;</label>
    <div class="health_label fl">
	[  xls  ]
	</div>
 </div>
    </div>
    </form>
    </div>
    
    
    <!--Listing Start Here-->
	<?php if(@$_POST['UploadAppointment']!='' && isset($_POST['UploadAppointment']) && $error_msg==''){ ?>
	      <div id="paging_container9" class="container">

    <div class="box_768 fl brdr_top" id="form_container"  style="overflow:scroll; height:300px;">
    
    
    
    <h1>Review Upload</h1>
    
    
		 <table cellpadding="10px" width="100%">
 		 <tr>
    <td><strong>Errors</strong></td>
    <td><strong>Policy</strong></td>
    <td><strong>Employee Email</strong></td>
    <td><strong>Password</strong></td>
	</tr>
	<?php if(isset($error_empty) && !empty($error_empty) && ($error_empty=='file cannot be empty')) { 
	?>
	<tr><td align="left" style="color:#D94344;" colspan="8"><?php echo $error_empty; ?></td></tr>
	<?php	
	} else {
	if($k=='1'){ $i='1'; $firstColumn = '1'; } else { $i='0';  $firstColumn = '0';  }
	for($i=$i;$i<sizeof($final_arr);$i++)
	{			
		$policyNumber 	= $final_arr[$i]['policyNumber'];	
		$emailid		= $final_arr[$i]['emailid'];
		$password 		= $final_arr[$i]['password'];
		if( ($i%2) !=0)  { $get_class="gradeX even"; } else { $get_class="gradeX odd";	}
	?>
	    <tr>
    <td style="color:#D94344" class="<?php echo @$get_class; ?>"><?php  echo $i; echo $error[$i]?$error[$i]:'0'; ?></td>
    <td  class="<?php echo @$get_class; ?>"><?=stripslashes($policyNumber);?></td>
    <td class="<?php echo @$get_class; ?>"><?=stripslashes($emailid);?></td>
    <td class="<?php echo @$get_class; ?>"><?=stripslashes($password);?></td>
    </tr>


	<?php } 
	$newarr = urlencode(serialize($final_arr));
	}
	?>	
 		</table>           <!-- pagination   -->
        </form>
    </div>
    <div class="formbuttonBox fl">
    <form action="" method="post" enctype="multipart/form-data">
	<input type="hidden" name="final_arr" id="final_arr" value="<?=$newarr;?>" />
	company title : <input type="text" name="compTitle" value="<?php echo @$_REQUEST['compTitle']; ?>"  />
    <input type="button" class="cancelbtn fr" value="Re-upload" id="button" name="button" onclick="window.location='employee_bulk_upload.php?compId=<?php echo @$compId; ?>'">
   <?php //if($error=='') { ?> <input type="submit"  class="gobtn fr" value="Confirm" id="button" name="Confirm"> <?php //} ?>
	</form>
    </div>
    </div>
	<?php } ?>
    <!--Listing End Here-->
    
    
    
    </div>
    <!--Right Container End Here-->
</div>

  <div class="cl"></div>
</div>
<!--Middle End Here-->
<?php include('inc/inc.ft.php'); ?>