<?php include_once('../inc/hd.php');
 $id=sanitize_data(@$_REQUEST['id']);
 $mode=sanitize_data(@$_REQUEST['mode']);
 
$agegrouplist = ageGroupList();
$suminsuredlist = sumInsuredList();
?>
<link rel="stylesheet" href="<?=_WWWROOT;?>/css/colorpicker.css" type="text/css" />
<link rel="stylesheet" media="screen" type="text/css" href="<?=_WWWROOT;?>/css/layout.css" />
<script type="text/javascript" src="<?=_WWWROOT;?>/js/colorpicker.js"></script>
    <script type="text/javascript" src="<?=_WWWROOT;?>/js/eye.js"></script>
    <script type="text/javascript" src="<?=_WWWROOT;?>/js/layout.js?ver=1.0.2"></script>

<script>
function showExternal(){

$(".externalLink").show();

$(".description").hide();

}

function showDesc(){

$(".externalLink").hide();

$(".description").show();

}

function validate(){

		if(document.location.locationName.value=="")

		{

		alert("Please enter the location");

		document.location.locationName.focus();

		return false;

		}

	}
</script>

<div id="middle">
  <div class="middle-heading-bg">
    <h1>Sum Insured/Age Group Mapping</h1>
  </div>
  <div class="middle-data">
   <table width="100%" border="0" cellpadding="0" cellspacing="0" class="display" id="example">
	<thead>
		<tr>
			<th width="5%" align="center" class="col-border-1">&nbsp;</th>
			<?php
				$m=0;
				while($m<count($suminsuredlist)){
		 	?>
			<th width="10%" align="center" class="col-border-1"><?php echo $suminsuredlist[$m]['sumInsured'];?></th>
			<?Php $m++; }	?>	
		</tr>
	</thead>
	 <form action="writeSAMapping.php" method="post" name="samapping" id="samapping">
	<tbody>
	
		<?php
			$s=0;
			while($s<count($agegrouplist)){
		 ?>
		<tr class="gradeX">		
			<td align="center"  class="col-border-1"><?php echo $agegrouplist[$s]['ageGroupName'];?></td> 
			<?php
				$m=0;
				while($m<count($suminsuredlist)){
		 	?>
			<td align="center"  class="col-border-1">
			<input name="satype[]" type="hidden" size="5"	value="<?=$agegrouplist[$s]['id'];?>_<?=$suminsuredlist[$m]['id'];?>">
			<input type="text" name="sa_<?=$agegrouplist[$s]['id'];?>_<?=$suminsuredlist[$m]['id'];?>" id="sa_<?=$agegrouplist[$s]['id'];?>_<?=$suminsuredlist[$m]['id'];?>" value="<?=getSAMappingData($agegrouplist[$s]['id'],$suminsuredlist[$m]['id'],$companyId);?>"/>			
			</td>	
			<?Php $m++; }	?>		
		</tr>
		<?Php $s++; }	?>	
		<tr>
            <td class="col-border_event">&nbsp;</td>
            <td colspan="<?=count($suminsuredlist);?>" class="col-border_event" align="right">
			<input type="hidden" name="companyId" id="companyId" value="<?=$companyId;?>"/>
			<input  type="submit" value="Submit" /></td>
          </tr>		
	</tbody>	
	</form>
</table>
</div> 
<!--middle data-->
      </div><div style="clear:both"></div> <!--middle-->
  <?php include('../inc/ft.php');?>
</body>
</html>
