<?php  
include('../conf/session.php');
include('../conf/conf.php');
include('../conf/fucts.php');
$prefix=substr(time(),5,5);
$companyId					=	sanitize_data(@$_REQUEST['companyId']);
$companydetails 			=	companydetails($companyId);
$offerTopUp 				=	getName('tbl_company','id',$companyId,'offerTopUp');
if($offerTopUp!='yes') { echo 'Top-up Offer is not included'; } else {
/*echo '<pre>';
print_r($companydetails);*/
$agegrouplist = ageGroupList();
$gradelist = gradeList($companyId);
$topupsuminsuredmappinglist = topUpSumInsuredMapList($companyId);
/*echo '<pre>';
print_r($topupsuminsuredmappinglist);*/
if($companydetails[0]['policyType']=='fixed') { ?> 
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="display" id="example">
					<thead>
						<tr>
							<th width="5%" align="left" class="col-border-1">Grades</th>
							<?php
								$m=0;
								while($m<count($topupsuminsuredmappinglist)){
							?>
							<th width="5%" align="center" class="col-border-1"><?php echo $topupsuminsuredmappinglist[$m]['sumInsured'];?></th>
							<?Php $m++; }	?>	
						</tr>
					</thead>
					<tbody>	
						<?php
							$s=0;
							while($s<count($gradelist)){
						 ?>
						<tr class="gradeX">		
							<td align="left"  class="col-border-1"><?php echo $gradelist[$s]['grade'];?></td> 
							<?php
								$m=0;
								while($m<count($topupsuminsuredmappinglist)){
								$chkmap = chkGradeTopupSIMapData($gradelist[$s]['id'],$topupsuminsuredmappinglist[$m]['id'],$companyId);
							?>
							<td align="center"  class="col-border-1">
							<input name="satype[]" type="hidden" size="5"	value="<?=$gradelist[$s]['id'];?>_<?=$topupsuminsuredmappinglist[$m]['id'];?>">
							<input type="text" name="sa_<?=$gradelist[$s]['id'];?>_<?=$topupsuminsuredmappinglist[$m]['id'];?>" id="sa_<?=$gradelist[$s]['id'];?>_<?=$topupsuminsuredmappinglist[$m]['id'];?>" value="<?=getGradeTopupSIMapData($gradelist[$s]['id'],$topupsuminsuredmappinglist[$m]['id'],$companyId);?>" style="width:70px;" <?php if($chkmap<1){ ?>disabled="disabled" <?php }	?>/>			
							</td>	
							<?Php $m++; }	?>		
						</tr>
						<?Php $s++; }	?>	
						<tr>
						<td class="col-border_event">&nbsp;</td>
						<td colspan="<?=count($suminsuredlist);?>" class="col-border_event" align="right">
						<input type="hidden" name="companyId" id="companyId" value="<?=$companyId;?>"/>
						<input type="hidden" name="simapvariable" id="simapvariable" value="grade"/>
						<input type="hidden" name="policyType" id="policyType" value="<?=$companydetails[0]['policyType'];?>"/>
						<input type="hidden" name="insuranceType" id="insuranceType" value="<?=$companydetails[0]['insuranceType'];?>"/>
						<input type="button" value="Submit" onclick="updateTopUpSIPremium()" /></td>
					  </tr>								
					</tbody>						
					</table>
<?php } else {		?>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="display" id="example">
	<thead>
		<tr>
			<th width="5%" align="center" class="col-border-1">Age Group</th>
			<?php
				$m=0;
				while($m<count($topupsuminsuredmappinglist)){
		 	?>
			<th width="5%" align="center" class="col-border-1"><?php echo $topupsuminsuredmappinglist[$m]['sumInsured'];?></th>
			<?Php $m++; }	?>	
		</tr>
	</thead>
	<tbody>	
		<?php
			$s=0;
			while($s<count($agegrouplist)){
		 ?>
		<tr class="gradeX">		
			<td align="center"  class="col-border-1"><?php echo $agegrouplist[$s]['ageGroupName'];?></td> 
			<?php
				$m=0;
				while($m<count($topupsuminsuredmappinglist)){
		 	?>
			<td align="center"  class="col-border-1">
			<input name="satype[]" type="hidden" size="5"	value="<?=$agegrouplist[$s]['id'];?>_<?=$topupsuminsuredmappinglist[$m]['id'];?>">
			<input type="text" name="sa_<?=$agegrouplist[$s]['id'];?>_<?=$topupsuminsuredmappinglist[$m]['id'];?>" id="sa_<?=$agegrouplist[$s]['id'];?>_<?=$topupsuminsuredmappinglist[$m]['id'];?>" value="<?=getTopUpSAMappingData($agegrouplist[$s]['id'],$topupsuminsuredmappinglist[$m]['id'],$companyId);?>" style="width:70px;"/>			
			</td>	
			<?Php $m++; }	?>		
		</tr>
		<?Php $s++; }	?>	
		<tr>
            <td class="col-border_event">&nbsp;</td>
            <td colspan="<?=count($suminsuredlist);?>" class="col-border_event" align="right">
			<input type="hidden" name="companyId" id="companyId" value="<?=$companyId;?>"/>
			<input type="hidden" name="simapvariable" id="simapvariable" value="grade"/>
			<input  type="button" value="Submit" onclick="updateTopUpSIPremium()" /></td>
          </tr>		
	</tbody>
</table>
<?php }	} ?>